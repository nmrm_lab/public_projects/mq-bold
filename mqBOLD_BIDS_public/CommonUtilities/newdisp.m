function newdisp(vol,skal,fsize)
%newdisp(vol,skal,fsize)
%displays image
%vol: 2D or 3D matrix containing the image(s)
%skal: scaling constant for display, default is 1
%fsize: scaling factor for font size in color bar
%       (if 0 or not specified: no color bar)

if nargin==1
 skal=1;
 fsize=0;
end

if nargin==2
 fsize=0;
end

groesse=size(vol(:));
groesse=groesse(1);
sortfeld=sort(vol(:));
maximum=sortfeld(groesse-10);

dimens=ndims(vol);

if dimens==2
 bild=vol;
end

if dimens==3
 sizemat=size(vol);
 nread=sizemat(2);
 nphase=sizemat(1);
 nimag=sizemat(3);
 numx=ceil(sqrt(nimag));
 numy=ceil(nimag/numx);
 bild=zeros(numy*nphase,numx*nread);
 for i=1:nimag
  col=1+nread*mod((i-1),numx);
  lin=1+nphase*floor((i-1)/numx);
  bild(lin:lin+nphase-1,col:col+nread-1)=vol(:,:,i);
 end
end

h1=imagesc(bild,[0,maximum/skal]);
set(gca,'Visible','off','DataAspectRatio',[1 1 1]);
colormap(gray);
if fsize~=0
 h1=colorbar;
 fontsize=fsize*10;
 set(h1,'FontSize',fontsize);
end

