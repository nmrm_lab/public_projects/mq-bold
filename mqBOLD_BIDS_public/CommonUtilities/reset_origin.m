%% Reset origin
% - Reset origin of given images by given shift matrix
% - Stephan Kaczmarz
% - 14OCT15

%% Required subscripts

% - SPM12


%% Modifications

% - SK16OCT15
%   + Using global Variables VAR
%
% - CP25JUN20
%   adapt for mqBOLD_BIDS


%% Main program
function reset_origin(filepath, filename, shiftOrigin, rescaleVoxel)

    % ---------------------------------------------------------------------
    % INITIALIZE
    % ---------------------------------------------------------------------

    % Print status
    fprintf('Started image origin check\n');


    % ---------------------------------------------------------------------
    % LAOD ORIGIN SHIFT MATRIX
    % ---------------------------------------------------------------------

    % initialize shift/rescale matrix
    M = zeros(4,4);
    M(4,4) = 1;
    % define shifts relative to current origin (analog to spm 'Display')
    if nargin < 3
        M(1,4) = 0;      % negative values move origin right (mm)
        M(2,4) = -15.0;  % negative values move origin forward (mm)
        M(3,4) = -45.0;  % negative values move origin up (mm)
    else
        M(1,4) = shiftOrigin.right; 
        M(2,4) = shiftOrigin.forward; 
        M(3,4) = shiftOrigin.up; 
    end
    % rescale voxel size
    if nargin < 4
        M(1,1) = 1;   % skale voxelsize in LR direction (for 3D data)
        M(2,2) = 1;   % skale voxelsize in AP direction (for 3D data)
        M(3,3) = 1;   % skale voxelsize in HF direction (for 3D data)
    else
        M(1,1) = rescaleVoxel.LR;
        M(2,2) = rescaleVoxel.AP;
        M(3,3) = rescaleVoxel.HF;
    end
	% ------------------------------
	% Load global variables
	% ------------------------------
    % Load defaults
	global defaults;
	spm_defaults;
	flags = defaults.coreg;

	% Flags to pass to routine to create resliced images
	% (spm_reslice)
	resFlags = struct(...
        'interp', flags.write.interp,...	% 1: trilinear interpolation
        'wrap', flags.write.wrap,...     	% wrapping info (ignore...)
        'mask', flags.write.mask,...       	% masking (see spm_reslice)
        'which',1,...                    	% write reslice time series for later use
        'mean',0,...                     	% do not write mean image
        'prefix', '');                  	% overwrite existing file

	% -----------------------------
	% Set images to reset origin
	% -----------------------------

	% PO - (O)ther images, originally realigned to PF and transformed again to PF
	% Other Images
	clear PO;
	PO = spm_select('FPList',filepath,filename);

    % -----------------------------
	% Get coregistration parameters
	% -----------------------------

	% Transform the mean image
	% Spm_get_space(deblank(PG),M);
	% In MM we put the transformations for the 'other' images
	MM = zeros(4,4,size(PO,1));
	for j=1:size(PO,1)
        %get the transformation matrix for every image
        MM(:,:,j) = spm_get_space(deblank(PO(j,:)));
    end
	for j=1:size(PO,1)
        %write the transformation applied to every image
        %filename: deblank(PO(j,:))
        spm_get_space(deblank(PO(j,:)), M*MM(:,:,j));
    end

	% -----------------------------
	% Apply coregistration 
	% -----------------------------
    spm_reslice(PO,resFlags);

    % Print status
	fprintf('    Origin shifting of %s finished\n', ...
                            fullfile(filepath, filename));
end
