%% Overlay functional image on anatomical image
% Save results as tif
% By Stephan Kaczmarz
% 03NOV16
% Using code from Jan Petr, Helmholtz Zentrum Dresden

% Required inputs: img_anatomy, img_mask1, img_mask2, save_path
% Optional inputs: flag_neurorad, img_mask

function Overlay_masks(img_anatomy, img_mask1, img_mask2, save_path, flag_sag2ax, flag_neurorad, img_mask, flag_cutoff)
    
    % Default: Cut off slices on bottom and top
    if nargin < 8
        flag_cutoff = 1;
    end
    
    % Default: Cut off slices on bottom and top
    if nargin < 7
        flag_cutoff = 1;
    end

    % Default: Neurorad view
    if nargin < 6
        flag_neurorad = 1;
    end
    
    % Default: no transformation of geometry
    if nargin < 5
        flag_sag2ax = 0;
    end
    
    % Get mask default: generate by positive mask of anatomy
    if nargin < 7
        imMask = (img_anatomy > 0);
    else
        imMask = img_mask;
    end
    
    % Transform input from sagittal to axial
    if flag_sag2ax
        img_anatomy = permute(img_anatomy, [3 1 2]);
        img_mask1 = permute(img_mask1, [3 1 2]);
        img_mask2 = permute(img_mask2, [3 1 2]);
        imMask = permute(imMask, [3 1 2]);
        img_anatomy = flip(img_anatomy, 1);
        img_mask1 = flip(img_mask1, 1);
        img_mask2 = flip(img_mask2, 1);
        imMask = flip(imMask, 1);
    end    
    
    imMask1 = logical(img_mask1);
    imMask2 = logical(img_mask2);
    
    % Get anatomical and functional image
	imStruct = img_anatomy;
	imRes1 = img_mask1;
	imRes2 = img_mask2;
    
    % Set anatomy above zero
    img_anatomy(img_anatomy<0) = 0;
    
    % Default: Neurorad view
    if nargin < 5
        flag_neurorad = 1;
    end
    
    % Get functional image masked and sorted
    sorted1 = imRes1(imMask1);
    sorted1 = sorted1(~isnan(sorted1));
    sorted1 = sort(sorted1(:),'ascend');
    sorted2 = imRes2(imMask2);
    sorted2 = sorted2(~isnan(sorted2));
    sorted2 = sort(sorted2(:),'ascend');

    % Remove the minimal and maximal value (5%) from results
    valLow1 = 0;
    valHigh1 = 1.1;
    valLow2 = 0;
    valHigh2 = 1.5;
    % Calculate low and high values
    imRes1(imRes1<valLow1) = valLow1;
    imRes1(imRes1>valHigh1) = valHigh1;
    imRes2(imRes2<valLow2) = valLow2;
    imRes2(imRes2>valHigh2) = valHigh2;
    
    % Stretch the anatomical image from min to max to (1..256)
    imStruct = (imStruct - min(imStruct(:)))/(max(imStruct(:)-min(imStruct(:)))); 
    imRes1 = ceil( (imRes1-valLow1)/(valHigh1-valLow1)*256);
    imRes2 = ceil( (imRes2-valLow2)/(valHigh2-valLow2)*256);
    
    % Get number of slices
    nb_slices = size(imRes1, 3);
    % Get cutt off slices on bommom and top
    if flag_cutoff
        if nb_slices < 50
            cutoff_top = 1;
            cutoff_bottom = 1;
        else
            cutoff_top = 35;
            cutoff_bottom = 107;
        end
    else
        cutoff_top = 0;
        cutoff_bottom = 0;
    end
    
    % Get list of slices to display, omitting first and last
    sliceList = 1 + cutoff_top : 1 : (nb_slices-cutoff_bottom);
    % Set number of columns to 6
    nb_clm = floor(sqrt(length(sliceList)*2));
    % Get number of rows depending on nb slices
    nb_rows = ceil((length(sliceList)-2)/nb_clm);
    % Create empty image to be filled
    imDisp = zeros(nb_rows*size(imRes1,2),nb_clm*size(imRes1,1),3);
    % Iterate through slices
    for slice = 1:length(sliceList)
        if flag_neurorad == 1
            sliceRes1 = imRes1(:,end:-1:1,sliceList(slice))';
            sliceRes2 = imRes2(:,end:-1:1,sliceList(slice))';
            sliceMask1 = imMask1(:,end:-1:1,sliceList(slice))';
            sliceMask2 = imMask2(:,end:-1:1,sliceList(slice))';
            sliceStruct = imStruct(:,end:-1:1,sliceList(slice))';
        else
            sliceRes1 = imRes1(end:-1:1,end:-1:1,sliceList(slice))';
            sliceRes2 = imRes2(end:-1:1,end:-1:1,sliceList(slice))';
            sliceMask1 = imMask1(end:-1:1,end:-1:1,sliceList(slice))';
            sliceMask2 = imMask2(end:-1:1,end:-1:1,sliceList(slice))';
            sliceStruct = imStruct(end:-1:1,end:-1:1,sliceList(slice))';
        end;
                
        sliceDisp1 = ind2rgb(sliceRes1, jet(256));
        sliceDisp1 = sliceDisp1.*repmat(sliceMask1,[1 1 3]);
        sliceDisp2 = ind2rgb(sliceRes2, jet(256));
        sliceDisp2 = sliceDisp2.*repmat(sliceMask2,[1 1 3]);
        
        sliceDisp = sliceDisp1*0.8 + sliceDisp2*0.8 + repmat(sliceStruct,[1 1 3])*0.6;
                
        sliceDisp = flip(sliceDisp, 1);
             
        posA = floor((slice-1)/nb_clm);
        posB = mod(slice-1,nb_clm);
        imDisp((1:size(sliceDisp,1)) + posA*size(sliceDisp,1),(1:size(sliceDisp,2)) + posB*size(sliceDisp,2),:) = sliceDisp;
   end;
   % Write image
   imwrite(imDisp, [save_path '.png'],'png');%, 'Compression', 'none', 'Quality', 100);
                   