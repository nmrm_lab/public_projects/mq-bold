%% Overlay functional image on anatomical image
% Save results as tif
% By Stephan Kaczmarz
% 03NOV16
% Using code from Jan Petr, Helmholtz Zentrum Dresden

% Required inputs: img_anatomy, img_functional, save_path
% Optional inputs: flag_neurorad, img_mask

function Overlay_anatomy(img_anatomy, img_functional, save_path, range, file_format, ...
    flag_sag2ax, flag_neurorad, img_mask, flag_cutoff)


    % Default: Cut off slices on bottom and top
    if nargin < 9
        flag_cutoff = 1;
    end
    
    % Get mask default: generate by positive mask of anatomy & functional
    if nargin < 8
        imMask_1 = (img_anatomy > 0);
        imMask_2 = (img_functional > 0);
        imMask = logical(imMask_1 .* imMask_2);
    else
        imMask = img_mask;
    end
    
    % Default: Neurorad view
    if nargin < 7
        flag_neurorad = 1;
    end
    
    % Default: no transformation of geometry
    if nargin < 6
        flag_sag2ax = 0;
    end
    
    if nargin < 5
        file_format = 0; % = Dicom, also fits for nifti
    end
    
    % Default: Automated colormap
    % Set manual range if input variable set and length of range variable is correct
    if nargin >= 4 && length(range)==2
        flag_autocmap = 0;
    else
        flag_autocmap = 1;
    end
    
    
    % Transform input from sagittal to axial
    if flag_sag2ax
        img_anatomy = permute(img_anatomy, [3 1 2]);
        img_functional = permute(img_functional, [3 1 2]);
        imMask = permute(imMask, [3 1 2]);
        img_anatomy = flip(img_anatomy, 1);
        imMask = flip(imMask, 1);
    end
    
    
    % Get anatomical and functional image
	imStruct = img_anatomy;
	imRes = img_functional;
    
    % Get functional image masked and sorted
    sorted = imRes(imMask);
    sorted = sorted(~isnan(sorted));
    sorted = sort(sorted(:),'ascend');

    % Set displayed values
    if flag_autocmap
        % Remove the minimal and maximal value (5%) from results
        valLow = sorted(ceil(0.05*length(sorted)));
        valHigh = sorted(floor(0.95*length(sorted)));
    else
        valLow = range(1);
        valHigh = range(2);
    end
    % Calculate low and high values
    imRes(imRes<valLow) = valLow;
    imRes(imRes>valHigh) = valHigh;
            
    % Stretch the anatomical image from min to max to (1..256)
    imStruct = (imStruct - min(imStruct(:)))/(max(imStruct(:)-min(imStruct(:)))); 
    imRes = ceil( (imRes-valLow)/(valHigh-valLow)*256);
    
    % Get number of slices
    nb_slices = size(imRes, 3);
    % Get cutt off slices on bommom and top
    if flag_cutoff
        if nb_slices < 50
            cutoff_top = 1;
            cutoff_bottom = 1;
        else
            cutoff_top = 35;
            cutoff_bottom = 107;
        end
    else
        cutoff_top = 0;
        cutoff_bottom = 0;
    end
    
    % Get list of slices to display, omitting first and last
    sliceList = 1 + cutoff_top : 1 : (nb_slices-cutoff_bottom);
    % Set number of columns to 6
    nb_clm = floor(sqrt(length(sliceList)*2));
    % Get number of rows depending on nb slices
    nb_rows = ceil((length(sliceList)-2)/nb_clm);
    % Create empty image to be filled
    imDisp = zeros(nb_rows*size(imRes,2),nb_clm*size(imRes,1),3);
    % Iterate through slices
    for slice = 1:length(sliceList)
        if flag_neurorad == 1
            sliceRes = imRes(:,end:-1:1,sliceList(slice))';
            sliceMask = imMask(:,end:-1:1,sliceList(slice))';
            sliceStruct = imStruct(:,end:-1:1,sliceList(slice))';
        else
            sliceRes = imRes(end:-1:1,end:-1:1,sliceList(slice))';
            sliceMask = imMask(end:-1:1,end:-1:1,sliceList(slice))';
            sliceStruct = imStruct(end:-1:1,end:-1:1,sliceList(slice))';
        end
        
        sliceDisp = ind2rgb(sliceRes, jet(256));
        sliceDisp = sliceDisp.*repmat(sliceMask,[1 1 3]);
                
        sliceDisp = sliceDisp*0.4 + repmat(sliceStruct,[1 1 3])*0.6;
                
        if file_format == 1 % flip only necessary for PAR/REC data
            sliceDisp = flip(sliceDisp, 1); %CP: Problem here with old matlab version
        end
        
        posA = floor((slice-1)/nb_clm);
        posB = mod(slice-1,nb_clm);
        imDisp((1:size(sliceDisp,1)) + posA*size(sliceDisp,1),(1:size(sliceDisp,2)) + posB*size(sliceDisp,2),:) = sliceDisp;
    end
   % Check if folder exists
   check_folder(fileparts(save_path));
   
   % Write image
   imwrite(imDisp, [save_path '.png'],'png');%, 'Compression', 'none', 'Quality', 100);