%% Overlay functional image on anatomical image
% Save results as tif
% By Stephan Kaczmarz
% 14DEC16
% Using code from Jan Petr, Helmholtz Zentrum Dresden

% Required inputs: img_anatomy, img_functional, save_path
% Optional inputs: flag_neurorad, img_mask

function Overlay_anatomy_sag2ax(img_anatomy, img_functional, save_path, flag_neurorad, img_mask)


    % Transform input from sagittal to axial
    img_anatomy = permute(img_anatomy, [3 1 2]);
    img_functional = permute(img_functional, [3 1 2]);
    
    % Cut off bottom and top slices
    cut_off_bottom = 24;
    cut_off_top = 94;

    % Get mask default: generate by positive mask of anatomy & functional
    if nargin < 5
        imMask_1 = (img_anatomy > 0);
        imMask_2 = (img_functional > 0);
        imMask = logical(imMask_1 .* imMask_2);
    else
        imMask = img_mask;
    end
    
    % Get anatomical and functional image
	imStruct = img_anatomy;
	imRes = img_functional;
    
    % Default: Neurorad view
    if nargin < 4
        flag_neurorad = 1;
    end
    
    % Get functional image masked and sorted
    sorted = imRes(imMask);
    sorted = sorted(~isnan(sorted));
    sorted = sort(sorted(:),'ascend');

    % Remove the minimal and maximal value (5%) from results
    valLow = sorted(ceil(0.05*length(sorted)));
    valHigh = sorted(floor(0.95*length(sorted)));
    % Calculate low and high values
    imRes(imRes<valLow) = valLow;
    imRes(imRes>valHigh) = valHigh;
            
    % Stretch the anatomical image from min to max to (1..256)
    imStruct = (imStruct - min(imStruct(:)))/(max(imStruct(:)-min(imStruct(:)))); 
    imRes = ceil( (imRes-valLow)/(valHigh-valLow)*256);
    
    % Get number of slices
    nb_slices = size(imRes, 3);
    % Set number of columns to 6
    nb_clm = 12;    
    % Get number of rows depending on nb slices
    nb_rows = ceil((nb_slices-2)/nb_clm);
    % Get list of slices to display, omitting first and last
    sliceList = 2:1:(nb_slices-1);
    % Create empty image to be filled
    imDisp = zeros(nb_rows*size(imRes,2),nb_clm*size(imRes,1),3);
    % Iterate through slices
    for slice = 1+cut_off_bottom : length(sliceList)-cut_off_top
        if flag_neurorad == 1
            sliceRes = imRes(:,end:-1:1,sliceList(slice))';
            sliceMask = imMask(:,end:-1:1,sliceList(slice))';
            sliceStruct = imStruct(:,end:-1:1,sliceList(slice))';
        else
            sliceRes = imRes(end:-1:1,end:-1:1,sliceList(slice))';
            sliceMask = imMask(end:-1:1,end:-1:1,sliceList(slice))';
            sliceStruct = imStruct(end:-1:1,end:-1:1,sliceList(slice))';
        end;
                
        sliceDisp = ind2rgb(sliceRes, jet(256));
        sliceDisp = sliceDisp.*repmat(sliceMask,[1 1 3]);
                
        sliceDisp = sliceDisp*0.4 + repmat(sliceStruct,[1 1 3])*0.6;
                
        sliceDisp = flip(sliceDisp, 1);
             
        posA = floor((slice-1)/nb_clm);
        posB = mod(slice-1,nb_clm);
        imDisp((1:size(sliceDisp,1)) + posA*size(sliceDisp,1),(1:size(sliceDisp,2)) + posB*size(sliceDisp,2),:) = sliceDisp;
   end;
   % Write image
   imwrite(imDisp, [save_path '.jpg'],'jpg', 'Quality', 100);
                   