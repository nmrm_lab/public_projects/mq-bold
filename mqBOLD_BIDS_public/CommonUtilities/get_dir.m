%% Get directories
% - Get directory by GUI selection
% - Stephan Kaczmarz 22OCT15


function dir_output = get_dir(string_display)
    if nargin == 0
        fprintf('get_dir: No Search string found! Using default empty string.')
        string_display = '';
    end
    % Select current dir as default
    wd=pwd;
    % Set flag: indicates if dir was selected
    flag_dir = 0;
    while flag_dir == 0
        % Displayed message
        message = sprintf(string_display);
        % Open Pop-up window to select directory
        dir_output = uigetdir(wd, message);
        if ~isempty(dir_output)
            flag_dir = 1;
        else
            error('No file selected! ');
        end
    end
end