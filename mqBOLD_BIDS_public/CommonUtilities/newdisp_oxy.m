function newdisp_oxy(vol, skal, fsize, title, min, max)
% displays 2D or 3D images 

% vol:      2D or 3D matrix containing the image(s)
% skal:     scaling constant for display, default is 1
% fsize:    scaling factor for font size in color bar
%           (if 0 or not specified: no color bar)
% title:    figure title
% min:      minimum value displayed (if not specified: 0)
% max:      maximum value displayed (if not specified: max val of vol)

if nargin > 3
    suptitle(title);
    
    if nargin==4 && skal==0 % avoid problems with colorbar (AK):
        skal = 1;
    end
end

if nargin==1
    skal = 1;
    fsize= 0;
end

if nargin==2
    fsize= 0;
end

if nargin==3 && skal==0
    % avoid problems with colorbar (AK):
    skal = 1;
end

% avoid problems with colorbar (AK):
vol(isnan(vol)) = 0;
vol(isinf(vol)) = 0;

groesse = size(vol(:));
groesse = groesse(1);
sortfeld= sort(vol(:));
maximum = sortfeld(groesse);

dimens  = ndims(vol);

if dimens==2   
    bild = rot90(vol);
end

if dimens==3
    sizemat = size(vol);
    nread   = sizemat(2);
    nphase  = sizemat(1);
    nimag   = sizemat(3);
    numx    = ceil(sqrt(nimag));
    numy    = ceil(nimag/numx);
    bild    = zeros(numy*nphase,numx*nread);
    
    for i=1:nimag
        col     = 1+nread*mod((i-1),numx);
        lin     = 1+nphase*floor((i-1)/numx);
        bild(lin:lin+nphase-1,col:col+nread-1) = rot90(vol(:,:,i));
        % bild(lin:lin+nphase-1,col:col+nread-1) = (vol(:,:,i)); % AK
    end
    
end

if nargin > 4
    if nargin > 5
        h1 = imagesc(bild, [min, max]);
    else
        h1 = imagesc(bild, [min, maximum/skal]);
    end
else
    h1 = imagesc(bild,[0,maximum/skal]);
end

set(gca,'Visible','off','DataAspectRatio',[1 1 1]);
colormap(gray);

if fsize~=0
    h1         = colorbar;
    fontsize    = fsize*10;
    set(h1,'FontSize',fontsize);
end

% subtitle function allow only one title above whole Figur:
function hout = suptitle(str)
% SUPTITLE Puts a title above all subplots.
%	SUPTITLE('text') adds text to the top of the figure
%	above all subplots (a "super title"). Use this function
%	after all subplot commands.

% Drea Thomas 6/15/95 drea@mathworks.com

% Warning: If the figure or axis units are non-default, this
% will break.

% Parameters used to position the supertitle.

% Amount of the figure window devoted to subplots
plotregion = .97;

% Y position of title in normalized coordinates
titleypos  = .95;

% Fontsize for supertitle
fs         = get(gcf,'defaultaxesfontsize')+5;

% Fudge factor to adjust y spacing between subplots
fudge = 1;

haold    = gca;
figunits = get(gcf,'units');

% Get the (approximate) difference between full height (plot + title
% + xlabel) and bounding rectangle.

if (~strcmp(figunits,'pixels'))
    set(gcf,'units','pixels');
    pos = get(gcf,'position');
    set(gcf,'units',figunits);
else
    pos = get(gcf,'position');
end
ff = (fs-4)*1.27*5/pos(4)*fudge;

% The 5 here reflects about 3 characters of height below
% an axis and 2 above. 1.27 is pixels per point.

% Determine the bounding rectange for all the plots

% h = findobj('Type','axes');

% findobj is a 4.2 thing.. if you don't have 4.2 comment out
% the next line and uncomment the following block.

%h = findobj(gcf,'Type','axes');  % Change suggested by Stacy J. Hills

% If you don't have 4.2, use this code instead
ch = get(gcf,'children');
h  = [];
for i=1:length(ch)
    if strcmp(get(ch(i),'type'),'axes')
        h = [h,ch(i)];
    end
end

max_y=0;
min_y=1;

oldtitle =0;
for i=1:length(h)
    if (~strcmp(get(h(i),'Tag'),'suptitle'))
        pos=get(h(i),'pos');
        if (pos(2) < min_y)
            min_y=pos(2)-ff/5*3;
        end
        if (pos(4)+pos(2) > max_y)
            max_y=pos(4)+pos(2)+ff/5*2;
        end
    else
        oldtitle = h(i);
    end
end

if max_y > plotregion
    scale = (plotregion-min_y)/(max_y-min_y);
    for i=1:length(h)
        pos = get(h(i),'position');
        pos(2) = (pos(2)-min_y)*scale+min_y;
        pos(4) = pos(4)*scale-(1-scale)*ff/5*3;
        set(h(i),'position',pos);
    end
end

np = get(gcf,'nextplot');
set(gcf,'nextplot','add');
if oldtitle ~= 0 % SK16JAN17 to prevent errors with Matlab 2016a
    delete(oldtitle);
end
ha=axes('pos',[0 1 1 1],'visible','off','Tag','suptitle');
ht=text(0.48,titleypos-1,str);set(ht,'horizontalalignment','center','fontsize',fs);
set(gcf,'nextplot',np);
axes(haold);
if nargout
    hout=ht;
end
