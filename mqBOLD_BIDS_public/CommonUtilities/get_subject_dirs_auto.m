% Automatically find unprocessed subjects for autoamted Hypoxia processing
% - Stephan Kaczmarz 15MAR17
% - CP: adapted to BIDS structure

function [StudyPath, nsubs] = get_subject_dirs_auto

    % Load subjects
    % Get dir
    StudyPath.dir_base = ...
        get_dir('Select BIDS study folder to process multiple subjects!');
    StudyPath.dir_input = fullfile(StudyPath.dir_base,'input');
    StudyPath.dir_output = fullfile(StudyPath.dir_base,'output');
    % List of all folders in directory
    liste = dir(StudyPath.dir_input);
    % Name of all foldernames
    folders = {liste.name};
    % Iterate through all foldernames to clean them by neglecting '.' and '..'
    % only keep subject folders that contain 'sub' in their names
    % Additionally filter non directories to neglect files in patient folder
    for i = size(folders,2) : -1 : 1
        tmp = folders(i);
        tmp_str = tmp{1};
        if tmp_str(1) == '.'
            folders(i) = [];
            liste(i) = [];
        elseif isempty(regexpi(tmp_str,'sub'))
       		folders(i) = [];
            liste(i) = [];
        elseif liste(i).isdir == 0
            folders(i) = [];
            liste(i) = [];
        end
    end

    % Create empty cells for subjects to be processed
    StudyPath.input_dir_sb_process = {};
    StudyPath.output_dir_sb_process = {};
    % Iterate through cleaned foldernames 
    % Automatically select only subjects which are not yet analyzed
    % -> exclude processed subjects with existing output folder
    
    % Number of subjects index
    sb_no = 0;
    for sb = 1 : size(folders,2)
        dir_subject_sb = fullfile(StudyPath.dir_input, folders{sb});
        % Check for file that is written after last processing step 
        path_finishedflag = fullfile(StudyPath.dir_output, folders{sb}, ...
                                                  'qBOLD_workspace.mat');
        if ~exist(path_finishedflag, 'file')
            sb_no = sb_no + 1;
            StudyPath.input_dir_sb_process{sb_no} = dir_subject_sb;
            StudyPath.output_dir_sb_process{sb_no} = ...
                fullfile(StudyPath.dir_output, folders{sb});
        end
    end

    % Check if un-processed subjects were found
    try
        nsubs = numel(StudyPath.input_dir_sb_process);
        fprintf('\nFound %i unprocessed participants at directory %s:\n', ...
                                                nsubs, StudyPath.dir_base);
        for sb=1:nsubs
            fprintf('    Sb %i: %s \n', sb, StudyPath.input_dir_sb_process{sb});
        end
    catch
        fprintf('ERROR! No un-processed participants found at directory %s\n', ...
            StudyPath.dir_base);
        return;
    end
    
end