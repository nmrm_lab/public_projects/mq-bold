%% Check if folder exists
% - Creates folder at given dir if it doesn't exist
% - Stephan Kaczmarz 22OCT15

function check_folder(dir)
    if ~exist(dir, 'dir')
            mkdir(dir);
            fprintf('    Creating directory %s\n', dir);
    end
end