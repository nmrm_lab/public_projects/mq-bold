
% fills holes in 2D binary array


function filled_bin_array = fillholes( bin_array)

[x, y] = size(bin_array);
% temp_bin_array is to be filled in regions connected to margins
temp_bin_array = zeros(x,y);



% fill margins


for i = 1:x
    if bin_array(i,1) == 0
        temp_bin_array(i,1) = 1;
    end
    if bin_array(i,y) == 0
        temp_bin_array(i,y) = 1;
    end
end

for j = 1:y
    if bin_array(1,j) == 0 || bin_array(x,j) == 0
        temp_bin_array(1,j) = 1;
    end
    if bin_array(x,j) == 0
        temp_bin_array(x,j) = 1;
    end
end



% fill regions connected to margins


for i = 2:x-1
    for j = 2:y-1
        if bin_array(i,j) == 0 && temp_bin_array(i,j) == 0
            if ( temp_bin_array(i,j-1) == 1 || temp_bin_array(i-1,j) == 1)
       
                temp_bin_array(i,j) = 1;

            end          
        end
    end
end

% eliminate slipstream of neighbored pixels that are connected to margin
% but are not yet evaluated
% j backwards ...
for i = 2:x-1
    for j = 1:y-2
        if bin_array(i,y-j) == 0 && temp_bin_array(i,y-j) == 0
            if ( temp_bin_array(i,y-j+1) == 1 || temp_bin_array(i-1,y-j) == 1)
       
                temp_bin_array(i,y-j) = 1;

            end          
        end
    end
end

% ... i backwards...  
for i = 1:x-2
    for j = 2:y-1
        if bin_array(x-i,j) == 0 && temp_bin_array(x-i,j) == 0
            if ( temp_bin_array(x-i,j-1) == 1 || temp_bin_array(x-i+1,j) == 1)
       
                temp_bin_array(x-i,j) = 1;

            end          
        end
    end
end
   
% ... i and j backwards...  
for i = 1:x-2
    for j = 1:y-2
        if bin_array(x-i,y-j) == 0 && temp_bin_array(x-i,y-j) == 0
            if ( temp_bin_array(x-i,y-j+1) == 1 || temp_bin_array(x-i+1,y-j) == 1)
       
                temp_bin_array(x-i,y-j) = 1;

            end          
        end
    end
end

% ... i and j order switched...
for j = 2:y-1
    for i = 2:x-1
        if bin_array(i,j) == 0 && temp_bin_array(i,j) == 0
            if ( temp_bin_array(i,j-1) == 1 || temp_bin_array(i-1,j) == 1)
       
                temp_bin_array(i,j) = 1;

            end          
        end
    end
end

for j = 1:y-2
    for i = 2:x-1
        if bin_array(i,y-j) == 0 && temp_bin_array(i,y-j) == 0
            if ( temp_bin_array(i,y-j+1) == 1 || temp_bin_array(i-1,y-j) == 1)
       
                temp_bin_array(i,y-j) = 1;

            end          
        end
    end
end
   
for j = 2:y-1
    for i = 1:x-2
        if bin_array(x-i,j) == 0 && temp_bin_array(x-i,j) == 0
            if ( temp_bin_array(x-i,j-1) == 1 || temp_bin_array(x-i+1,j) == 1)
       
                temp_bin_array(x-i,j) = 1;

            end          
        end
    end
end
   
for j = 1:y-2
    for i = 1:x-2
        if bin_array(x-i,y-j) == 0 && temp_bin_array(x-i,y-j) == 0
            if ( temp_bin_array(x-i,y-j+1) == 1 || temp_bin_array(x-i+1,y-j) == 1)
       
                temp_bin_array(x-i,y-j) = 1;

            end          
        end
    end
end

filled_bin_array = 1 - temp_bin_array;

return;

