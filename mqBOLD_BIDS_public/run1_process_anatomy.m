function [SubjectDir, filelist, flag] = run1_process_anatomy(subject_name, ...
                                                      	SubjectDir, flag)
%% Required Subscripts / Scriptsets:

%   - SPM12_modified
%   .........................
%   - using SPM12:
%   .........................
%   - coreg_general.m
%   - segment_SPM12.m

%% Modifications:                                                          
% =========================================================================
%   - CP 14 FEB 2020
%   step 0: copy data (if present) from BIDS
%   step 1: segment data
% =========================================================================

%% Main Program  
% check if anatomical input data exist
if isfolder(fullfile(SubjectDir.input,'anat')) 
    % check if FLAIR data have already been copied and segmented
    SubjectDir.FLAIR = fullfile(SubjectDir.output,'qBOLD','FLAIR');
	if isfile(fullfile(SubjectDir.FLAIR,'cBrMsk.nii')) == 0
        % Create subject folder in BIDS 'output' folder 
        % if it does not already exist
        check_folder(fullfile(SubjectDir.output))
        % Create 'qBOLD' folder within subject folder in BIDS 'output'  
        % if it does not already exist
        check_folder(fullfile(SubjectDir.output, 'qBOLD'))
        % Copy and segment T1w (MPRAGE)
        if isfile(fullfile(SubjectDir.input,'anat',...
                           sprintf('%s_FLAIR.nii.gz',subject_name)))
            % Create 'FLAIR' folder within 'qBOLD' folder within subject folder 
            % in BIDS 'output' folder if it does not already exist   
            check_folder(SubjectDir.FLAIR);
            gunzip(fullfile(SubjectDir.input,'anat', ...
                sprintf('%s_FLAIR.nii.gz',subject_name)));                  
            FLAIR_filename = sprintf('%s_FLAIR.nii',subject_name);
            movefile(fullfile(SubjectDir.input,'anat',FLAIR_filename),...
                              SubjectDir.FLAIR)
            if flag.reset_origin
                reset_origin(SubjectDir.FLAIR,FLAIR_filename); 
            end
            % Segment FLAIR 
            PF = spm_select('ExtFPList',SubjectDir.FLAIR,'.*.nii$');
            PF = PF(1,:);
            segment_SPM12(PF, 6); % is only acceptable with 6 segments!            
        else
            flag.FLAIR = 0;
        end % if isfile(fullfile(SubjectDir.input,'anat',...
            %       sprintf('%s_FLAIR.nii.gz',subject_name)))
    else % define necessary path 
        SubjectDir.FLAIR = fullfile(SubjectDir.output,'qBOLD','FLAIR');
	end %if isfile(fullfile(SubjectDir.output,...,'cBrMsk.nii')) == 0
    
    % generate liste of paths to files that are needed later on
  	filelist.FLAIR_nii.anat = fullfile(SubjectDir.FLAIR,...
                sprintf('%s_FLAIR.nii',subject_name));
	filelist.FLAIR_nii.GM = fullfile(SubjectDir.FLAIR,...
                sprintf('c1%s_FLAIR.nii',subject_name));
	filelist.FLAIR_nii.WM = fullfile(SubjectDir.FLAIR,...
                sprintf('c2%s_FLAIR.nii',subject_name));
	filelist.FLAIR_nii.CSF = fullfile(SubjectDir.FLAIR,...
                sprintf('c3%s_FLAIR.nii',subject_name));
	filelist.FLAIR_nii.BrMsk_CSF = fullfile(SubjectDir.FLAIR,'cBrMsk_CSF.nii');
	filelist.FLAIR_nii.BrMsk = fullfile(SubjectDir.FLAIR,'cBrMsk.nii');
    flag.FLAIR = 1;
    
    % check if T1w data have already been copied and segmented
    SubjectDir.T1w = fullfile(SubjectDir.output, 'qBOLD','T1w');
	if isfile(fullfile(SubjectDir.T1w,'cBrMsk.nii')) == 0    
	% Copy and segment T1w (MPRAGE)
        if isfile(fullfile(SubjectDir.input,'anat',...
                           sprintf('%s_T1w.nii.gz',subject_name)))
            % Create 'T1w' folder within 'qBOLD' folder within subject folder 
            % in BIDS 'output' folder if it does not already exist                      
            check_folder(SubjectDir.T1w);
            gunzip(fullfile(SubjectDir.input,'anat',...
                           sprintf('%s_T1w.nii.gz',subject_name))); 
            T1w_filename = sprintf('%s_T1w.nii',subject_name);           
            movefile(fullfile(SubjectDir.input,'anat', T1w_filename),...
                              SubjectDir.T1w)
            if flag.reset_origin
                reset_origin(SubjectDir.T1w,T1w_filename);  
            end
            % Segment T1w 
            PF = spm_select('ExtFPList',SubjectDir.T1w,'.*.nii$');
            PF = PF(1,:);
            segment_SPM12(PF, 6); % is only acceptable with 6 segments!            
        else
            flag.T1w = 0;
        end % if isfile(fullfile(SubjectDir.input,'anat',...
            %       sprintf('%s_T1w.nii.gz',subject_name))) 
	else % define necessary path 
        SubjectDir.T1w = fullfile(SubjectDir.output, 'qBOLD','T1w');
	end %if isfile(fullfile(SubjectDir.output,...,'cBrMsk.nii')) == 0 

    % generate liste of paths to files that are needed later on
	filelist.T1w_nii.anat = fullfile(SubjectDir.T1w,...
                sprintf('%s_T1w.nii',subject_name));
	filelist.T1w_nii.GM = fullfile(SubjectDir.T1w,...
                sprintf('c1%s_T1w.nii',subject_name));
	filelist.T1w_nii.WM = fullfile(SubjectDir.T1w,...
                sprintf('c2%s_T1w.nii',subject_name));
	filelist.T1w_nii.CSF = fullfile(SubjectDir.T1w,...
                sprintf('c3%s_T1w.nii',subject_name));
	filelist.T1w_nii.BrMsk_CSF = fullfile(SubjectDir.T1w,'cBrMsk_CSF.nii');
	filelist.T1w_nii.BrMsk = fullfile(SubjectDir.T1w,'cBrMsk.nii');
    
    flag.T1w = 1;
else
	flag.FLAIR = 0;
	flag.T1w = 0;
end %if isfolder(fullfile(SubjectDir.input,'anat'))
end
