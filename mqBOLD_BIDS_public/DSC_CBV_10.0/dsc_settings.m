function DSC_pars = dsc_settings()
 % Are DSC data available for CBV calculation?
 % NOTE: all processing (except rCBV and MR-rOEF calculation) 
 %       works without DSC data
%--------------------------------------------------------------------------
% NOTE: Proper leakage correction in patients with contrat enhancing 
%   lesions, e.g. brain tumors actually requires T1w MPR + contrast agent
% - for data from healthy subjects & patients with intact blood brain 
%   barrier (no contrast agent leakage), the program also works with 
%   native T1w mages
%-------------------------------------------------------------------------- 
 
DSC_pars.presetvalues = 1; % 1: automatic processing with predefined input
                           %    parameters(recommended)
                           
DSC_pars.absolutCBV = 0;   % 0: no absolute CBV calculation (recommended) 

DSC_pars.choice_align = 'Yes'; 	% DSC: No  = without motion correction
                                %      Yes = with motion correction
                                %            (recommended)

DSC_pars.choice_stc   = 'Yes'; 	% DSC: Slice Time Correction 
                                % is performed in function timing_reorder.m
                                % Assumed scan order for
                                %   - Siemens scanner [2 4 6 ... 1 3 5 ...]
                                %   - Philips scanner [1 3 5 ... 2 4 6 ...]
DSC_pars.choice_thres = 'No';  	% for segmented brain, FLAIR and MPR_KM; 
                                % No = automatic choice of mask 
                                % Yes = manual probability thresholds for masks
                                
DSC_pars.choice_wbm   = '3 (conjunction)'; % Choice of brain masks  
                % mask '1 (left)': whole brain mask generated from FLAIR
                % mask '2 (right)': whole brain mask generated from MPRAGE
                % mask '3 (conjunction)': conjunction of masks 1&2
                %         (recommended)
                                
DSC_pars.choice_fit   = 2; 
                % Choice of calculation & leakage Correction method: 
               	% 1: Weisskoff/Boxerman 2006; first pass integration
               	% 2: Weisskoff/Boxerman 2006; full integration
               	%    (recommended choice)
            	% 3: Bjornerud et al. 2011; standard SVD (former method 3c)
               	% 4: Bjornerud et al. 2011; Tikhonov SVD (former method 3d)
% NOTE: Detailed information on the implementation of DSC processing 
%       can befound in:
% Kluge A, Lukas M, Toth V, Pyka T, Zimmer C, Preibisch C. Analysis 
%   of three leakage-correction methods for DSC-based measurement 
%   of relative cerebral blood volume with respect to heterogeneity 
%   in human gliomas. Magn Reson Imaging. 2016 May;34(4):410-21. 
%   doi: 10.1016/j.mri.2015.12.015. Epub 2015 Dec 17.
% Hedderich D, Kluge A, Pyka T, Zimmer C, Kirschke JS, Wiestler B, 
%   Preibisch C. Consistency of normalized cerebral blood volume values 
%   in glioblastoma using different leakage correction algorithms on 
%   dynamic susceptibility contrast magnetic resonance imaging data 
%   without and with preload. J Neuroradiol. 2019 Feb;46(1):44-51. 
%   doi: 10.1016/j.neurad.2018.04.006. Epub 2018 May 21.
end