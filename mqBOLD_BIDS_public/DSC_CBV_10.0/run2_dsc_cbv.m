function [SubjectDir, filelist, flag] = run3_dsc_cbv(SubjectDir, filelist, ...
                                                        DSC_pars, flag)

%% CBV calculation from DSC (Anne Kluge)
%
%   - includes leakage Correction Methodes
%   - includes slice time correction
%
%   - works for PAR/REC & DICOM & NII
%   - latest changes by SK in JUL16 only set up for PAR/REC
%
%   - Can be run in 
%       + Batch Mode: Process all unprocessed subjects within  directory
%       + Single Subject: Run one subject
%
%   - Settings can be set automatically or manually during processing
%       + Batch Mode: is always automatically
%       + Single Subject: can be choosen automatically or manually
%
%   - DSC folder has to be in Patient folder:
%   - with subfolders:
%                       - 'Perfusion' (containing Perfusion data)
%                       - 'MPR_KM'    (containing MPRAGE_KM data)
%                       - 'FLAIR'     (containing FLAIR data)
%
%% Required Subscripts:
%
%   - SPM 12_modified4
%
%   - dsc_cbv_final_DICOM.m
%   - dsc_cbv_final_PARREC.m
%
%   - dcm2analyze.m
%   - REC2niiv5.1
%
%% Modifications:
%   - *AK_27MAY2015*
%       + automatic creation of DSC subfolders and copy original DICOM or
%         PAR/REC into it.
%       + Check if MPRAGE is available in PAR/REC data
%   - *AK_23NOV2015*
%       + possibility to batch script:
%                       flag_batch = 0 (no batch)
%                       flag_batch = 1 (batch script)
%   - AK2016
%       + Sliec time correction
%   - *SK11JUL16*
%       + Improve coreg stabilization MPR 2 FLAIR 2 DSC
%       + Develop automatic directory processing
%       + Add separate slice timing correction
%       + Extended parameter saving
%       + Debug PAR/REC processing
%       + Save images for quality check in 'DSC/Processing'
%       + Add further comments to sourcecode
%       + Outsource data conversion
%       + Add automated PAR/REC vs DICOM detection
%   - *SK20JUL16*
%       + Added option: use predefined values also for single folder & GUI
%       + Debudding of singel patient processing
%       + Make dsc_cbv_final an independent program to prevent rOEF GUI problems
%       + Therefore, try to load rOEF_GUI_workspace.mat
%   - *SK16AUG16*
%       + Debug DICOM file handling
%       + Add detection of DICOM '.IMG' files
%       + Debug prebolus data handling
%   - *SK13JAN17*
%       + No more PB/MB
%       + Clean up code 
%   - *SK25JAN17*
%       + Debug batch mode
%       + dsc_cbv_calc as a function

% ========================================================================
%   - *YR12APR17*
%       +Final testing of the Nifty inputs 
%           To test a completely new set of Nifty data Please first make
%           sure that folders and subfolders are completely correct and 
%           make sure the main folder of the patient names ?DCM_Testdata?. 
%           There are three .nii files: 
%               1. FLAIR.nii > which need to rename to o_FLAIR.nii and in the FLAIR folder
%               2. PWI.nii > in the Perfusion folder
%               3. cT1.nii > which need to rename to o_T1.nii and in the MPR_KM folder
%       + Clean up code 
%   - *YR15FEB17*
%       + Nifti as Input
% =========================================================================

% =========================================================================
% - CP 03JUL17
%   automatic processing of one data set
%      - path to patient directory needs to supplied as input parameter
%      - Perfusion, MPR & FLAIR data need to be containd in DSC subfolders
% =========================================================================

% =========================================================================
%   - CP 07JUL2017
%       - overall clean up
%       - reorganisation of leakage correction subfunctions
%       - set multiplication factor Tc in SVD method = 4 for decreased
%         noise sensitivity
%       - automatic processing tested for Dicom, PAR/REC & Nifti
% =========================================================================

% =========================================================================
%   - CP 15JUN2018
%       - further clean up
% =========================================================================
    
% =========================================================================
%   - CP 19JUL2019
%       - adapt to Frankfurt data
% =========================================================================
% =========================================================================
%   - *CP02JAN20*
%     + adapt to *.nii.gz files from PACS (BW)
% =========================================================================

% =========================================================================
%   - *CP10MAR20*
%     + adapt to multi-condition qBOLD evaluation, importing BIDS data
%     + further cleanup
% =========================================================================

% --------------------------------------------------------------------------
% Set parameters for automatic processing mode
% --------------------------------------------------------------------------
if nargin < 1
	disp(' ')
	disp(' Requires data path (SubjectDir) as input!')
    % generated in 'run2_sortDataMultipleConditions.m'
    % being called by 'RUN_mqBOLD_auto.m'
	disp(' ')
end
if nargin < 2
	disp(' ')
	disp(' Requires paths to data (filelist) as input!')
    % generated in 'run2_sortDataMultipleConditions.m'
    % being called by 'RUN_mqBOLD_auto.m'    
	disp(' ')
end
if nargin < 3
    % Parameters are set in 'dsc_settings.m'
    % being called by 'RUN_mqBOLD_auto.m'     
	DSC_pars.presetvalues = 1 ; % 1: Use preset values
                                % 0: Manual choice of processing options
    DSC_pars.absolutCBV = 0;    % 0: no absolute CBV values (recommended) 
                                %   calculate WM normalized CBV (nCBV) only                     
                                % 1: attempt to calculate absolute CBV                              
    
	DSC_pars.choice_align = 'Yes'; 	% DSC: No = without / Yes = with MoCo
	DSC_pars.choice_stc   = 'Yes'; 	% DSC: Slice Time Correction       
	DSC_pars.choice_thres = 'No';  	% for segmented brain, FLAIR and MPR_KM; 
                                    % No = automatic choice of mask 
                                    % Yes = manual probability thresholds 
                                    %       for masks
	DSC_pars.choice_wbm   = '3';    % Choice of masks
                                    % mask 1: whole brain mask from FLAIR
                                    % mask 2: whole brain mask from MPRAGE
                                    % mask 3: conjunction of masks 1&2
	DSC_pars.choice_fit   = 2;     	% Choice of calculation 
                                    %          & leakage Correction method: 
                                    % 1: Weisskoff/Boxerman 2006; first pass 
                                    %                            integration
                                    % 2: Weisskoff/Boxerman 2006; full 
                                    %                            integration
                                    % 3: Bjornerud et al. 2011; standard SVD 
                                    % 4: Bjornerud et al. 2011; Tikhonov SVD 
                                    % NOTE: to choose more the one method, 
                                    % use parenthesis, e.g. [1 2 3 4]
end

% Save parameter settings
save(fullfile(SubjectDir.DSC.toplevel, 'DSC_pars.mat'), 'DSC_pars')

% Preparation & Load Images:
try % Catch error messages

	% ---------------------------------------------------------------------
	% Initialize:
	% ---------------------------------------------------------------------

	% Warning Settings
	warning ('off', 'all');
	warning ('on',  'MATLAB:singularMatrix');
	warning ('on',  'MATLAB:Rankdeficient');
    
    %----------------------------------------------------------------------
    % Check if processing was already performed
    %----------------------------------------------------------------------
    flag.DSCfinished = 0;
    SubjectDir.DSC.CBV = fullfile(SubjectDir.DSC.toplevel, 'CBV');
    switch DSC_pars.choice_fit
        case 1
            tmp = fullfile(SubjectDir.DSC.CBV, 'nCBV_corr_auc_fp.nii');
             if exist(tmp, 'file') == 2
                flag.DSCfinished = 1;
                filelist.DSC.CBV_auc_fp = tmp;
            end           
        case 2
           	tmp = fullfile(SubjectDir.DSC.CBV, 'nCBV_corr_auc_full.nii');
            if exist(tmp, 'file') == 2
                flag.DSCfinished = 1;
                filelist.DSC.CBV_auc_full = tmp;
            end
        case 3        
            tmp = fullfile(SubjectDir.DSC.CBV, 'nCBV_corr_sSVD.nii');
            if exist(tmp, 'file') == 2
                flag.DSCfinished = 1;
                filelist.DSC.CBV_sSVD = tmp;
            end
        case 4
            tmp = fullfile(SubjectDir.DSC.CBV, 'CBV_corr_TiSVD.nii');
            if exist(tmp, 'file') == 2
                flag.DSCfinished = 1;
                filelist.DSC.CBV_TiSVD = tmp;
            end
    end            
    
    if flag.DSCfinished == 0 % final CBV maps do not yet exist
        % -----------------------------------------------------------------
        %  Check if Programm was already started AND at which point it was
        %  aborted
        %   - load workspace and start at this point again
        % -----------------------------------------------------------------

        % Get list of previosly saved workspaces from aborted analysis 
        % of this subject
        DSC_pars.list_mat = dir(fullfile(SubjectDir.DSC.toplevel, '0*.mat'));

        % Check if processing was already performed
        if size(DSC_pars.list_mat,1) > 0 && ...
                strcmp(DSC_pars.list_mat(end).name,'05_workspace.mat')
        % if processing was only partially performed, it will be continued 
        % if processing was not performed yet, it will be started
        else            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Start MAIN Analysis Program                                 %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
            [SubjectDir, filelist, flag] = dsc_cbv_calc(SubjectDir, filelist, ...
                                                        DSC_pars, flag);
        end

        % Display status
        fprintf('Finished processing of DSC data: %s.\n', ...
            SubjectDir.DSC.toplevel);  
        
    else % CBV maps exist already -> print status message
        fprintf('Finished processing of DSC data: %s.\n', ...
            SubjectDir.DSC.toplevel);
    end %flag.DSCfinished == 0 % final CBV maps do not yet exist

% Catch error messages
catch exception
	disp(' ')
	disp(' DSC evaluation DID NOT WORK')
	disp(getReport(exception, 'extended', 'hyperlinks', 'on'))
	disp(' ')
	save('DSC_processing_DID NOT WORK.txt')
end % try batch_job
end
