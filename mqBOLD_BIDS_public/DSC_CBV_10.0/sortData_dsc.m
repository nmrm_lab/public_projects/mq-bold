%% Automatic sorting of data (per subject) 
%  for automated qBOLD processing
% - Stephan Kaczmarz 15MAR17
% - CP 28AUG 2018
% - CP 15DEC 2019
%   - Detecting corresponding files and conditions from BIDS convention
%   - Copying files to respective output directories
%--------------------------------------------------------------------------

function [SubjectDir, filelist, DSC_pars, flag] = sortData_dsc(subject_name, ...
        	conditions, SubjectDir, filelist, DSC_pars, flag, sb)

%determine number of conditions
nconditions = size(conditions,2);   
for ncd = 1:nconditions
    SubjectDir.condition(ncd).toplevel = fullfile(SubjectDir.output,...
                                    'qBOLD', cell2mat(conditions(ncd)));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% Check for DSC data; If DSC data exist: unzip and copy
% Also: Consider multiple conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------------------------------------------------------
% check for non-condition specific DSC
%--------------------------------------------------------------------------
dsc_niigz_filename = sprintf('%s_acq-perf_dsc.nii.gz',subject_name);
n_no_dsc = 0; % count number of conditions without DSC
if isfile(fullfile(SubjectDir.input,'dsc',dsc_niigz_filename))
    dsc_nii_filename = ...
        sprintf('%s_acq-perf_dsc.nii',subject_name);    
    json_filename = ...
        sprintf('%s_acq-perf_dsc.json',subject_name);
    % unzip data
    gunzip(fullfile(SubjectDir.input,'dsc',dsc_niigz_filename));
    % Create 'DSC' and 'DSC/Perfusion' folder within each subjects  
    % BIDS 'output' folder (if it does not already exist)           
	SubjectDir.DSC.toplevel = ...
    	fullfile(SubjectDir.output,'qBOLD','DSC');
        check_folder(SubjectDir.DSC.toplevel)         
	SubjectDir.DSC.Perfusion = fullfile(SubjectDir.output,'qBOLD',...
        'DSC','Perfusion');
	check_folder(SubjectDir.DSC.Perfusion);
	movefile(fullfile(SubjectDir.input,'dsc',dsc_nii_filename), ...
    	SubjectDir.DSC.Perfusion);
	copyfile(fullfile(SubjectDir.input,'dsc',json_filename),...
        SubjectDir.DSC.Perfusion);
	filelist.DSC.perfusion.nii = fullfile(SubjectDir.DSC.Perfusion, ...
        dsc_nii_filename); 
    % copy anatomy from toplevel folder to DSC
    [SubjectDir, filelist] = copy_MPR_FLAIR_2_DSC_dir(SubjectDir, filelist,sb);
    flag.DSC = 'singleDSC';
    % ---------------------------------------------------------------------
    % Split 4D to 3D nifti and extract parameters from json file
    % ---------------------------------------------------------------------               
    DSC_pars = Split4Dnifti(SubjectDir, filelist, DSC_pars, flag);	
else % search for condition specific DSC
    n_no_dsc = n_no_dsc + 1; % no non-condition specific DSC
    for ct=1:nconditions
        dsc_niigz_filename = ...
            sprintf('%s_%s_acq-perf_dsc.nii.gz',subject_name, conditions(ct));
        dsc_nii_filename = ...
            sprintf('%s_%s_acq-perf_dsc.nii',subject_name, conditions(ct));    
        json_filename = ...
            sprintf('%s_%s_acq-perf_dsc.json',subject_name, conditions(ct));
        if isfile(fullfile(SubjectDir.input,'dsc',dsc_niigz_filename))
            % unzip data
            gunzip(fullfile(SubjectDir.input,'dsc',dsc_niigz_filename));
            % Create 'DSC' and 'DSC/Perfusion' folder within each subjects 
            % condition folders  
            % in BIDS 'output' folder (if it does not already exist)           
            SubjectDir.condition(ct).DSC.toplevel = ...
                fullfile(SubjectDir.output,'qBOLD',cell2mat(conditions(ct)),...
                'DSC');
            check_folder(SubjectDir.condition(ct).DSC.toplevel)         
            SubjectDir.condition(ct).DSC.Perfusion = ...
                fullfile(SubjectDir.output,'qBOLD',cell2mat(conditions(ct)),...
                'DSC','Perfusion');
            check_folder(SubjectDir.condition(ct).DSC.Perfusion);
            movefile(fullfile(SubjectDir.input,'dsc',dsc_nii_filename), ...
                SubjectDir.condition(ct).DSC.Perfusion)
            copyfile(fullfile(SubjectDir.input,'dsc',json_filename),...
                SubjectDir.condition(ct).DSC.Perfusion);
            filelist.condition(ct).DSC.perfusion.nii = ...
                fullfile(SubjectDir.condition(ct).DSC.Perfusion, ...
                dsc_nii_filename);
            % copy anatomy from toplevel folder to condition specific DSC
            [SubjectDir, filelist] = ...
                copy_MPR_FLAIR_2_conditionDSC_dir(SubjectDir, filelist,sb,ct);
            flag.condition(ct).DSC = 1;
            flag.DSC = 'conditionDSC';
            % -------------------------------------------------------------
            % Split 4D to 3D nifti and extract parameters from json file
            % -------------------------------------------------------------
            DSC_pars = Split4Dnifti(SubjectDir.condition(ct),...
                            filelist.condition(ct), DSC_pars, flag);            
        else
            flag.condition(ct).DSC = 0;
            n_no_dsc = n_no_dsc + 1;
        end % if isfile(fullfile(SubjectDir.input,'dsc',dsc_niigz_filename))
    end % for ct=1:nconditions
end % isfile(fullfile(SubjectDir.input,'dsc',dsc_niigz_filename))
if n_no_dsc == (nconditions + 1)
    fprintf('No DSC data detected for subject %s\n', SubjectDir.input);
    flag.DSC = 0; % no DSC data found
end
    
end