%% Normalize CBV to defined Region
%   - Region general in WM
%   - roi: use either a predifined rectangular roi OR
%          an defined normalization mask (= NAWM)

function [CBVout] = normalize_CBV(CBVin, roi)

if nargin == 1
    [aver, ~]               = newroi_dsc( CBVin , 4);
end

if nargin == 2
    if numel(roi) == 4
        [aver, ~]	= newroi_dsc( CBVin , 4, roi);
    else
        aver	 	= median(CBVin(roi==1));    
    end
end

CBVout                  = CBVin * 0.025/aver * 100;     % in Prozent
CBVout(isnan(CBVout))   = 0.0;
CBVout(isinf(CBVout))   = 0.0;
CBVout( CBVout > 100)   = 100;
CBVout( CBVout < 0.0)   = 0.0;

end