function [aif_svd,aif_svd_real,aif_cluster,PVc,PWc,idx,TTPc]= ...
            svd_aif(dscdata, dR2s, DSC_pars, Index_E, Index_A, maskWB,... 
            mask_tumor, mask_pAUC, mask_pRough, mask_rPSR, s1, s2, fignr)
                       
nx = DSC_pars.nx;
ny = DSC_pars.ny;
nframes = DSC_pars.nframes;                 

%% SVD AIF
%

% initialize counter:
ss = 1;

% Set mask:
for k = 1:nframes
    
    % dR2s or Signal:
    % - dR2s:   contains more components and is more different between
    %           subjects
    % - Signal: contains 2 components and is largely comparable between
    %           subjects
    
    sig_1 = dR2s(:,:,:,k); % dR2s?
    sig_2(:,:,:,k) = sig_1 .* maskWB .* (1-mask_tumor) .* mask_pAUC .* mask_pRough .* mask_rPSR;  
end

data_reshape = zeros(size(dscdata));

for s = s1:s2
    
    % reshape to 2D dataset:
    slice2d     = reshape(sig_2(:,:,s,:), [(nx*ny) nframes]);
    
    % Standard SVD:
    [U, D, V]   = svd(slice2d);
    
    % restrict D:
    D(D < 0.05)  = 0;
    
    % weight * basisfunctions:
    curve2d     = D * V';
    
    % to localize arterial pixel:
    U_loc                   = U(:,1:nframes);
    
    U_loc_reshape(:,:,:,ss) = reshape(U_loc, [nx ny nframes]);
    
    U_1 = U_loc(:,1);
    U_2 = U_loc(:,2);
    U_3 = U_loc(:,3);
    
    % check curve direction:
    figure(fignr);
    subplot(2,2,1), plot(curve2d(1,:));hold all;
    subplot(2,2,2), plot(curve2d(2,:));hold all;
    subplot(2,2,3), plot(curve2d(3,:));hold all;
    subplot(2,2,4), plot(curve2d(4,:));hold all;
    
    % for simulation...(1. Kurve anderes rum)
    if max(abs(curve2d(1,:))) == max((curve2d(1,:)))
        curve2d (1,:)           = -curve2d(1,:);
        U_loc_reshape(:,:,1,ss) = -U_loc_reshape(:,:,1,ss);
        U_1                     = -U_1;
    end
    
    [val_1a, max_1]  = min(curve2d(1,:));  
    [val_2a, min_2]  = max(curve2d(2,:));
    [val_2b, max_2]  = min(curve2d(2,:));
    
    try
        while ( (curve2d(2,min_2-2) > (val_2a - 0.05*val_2a)) || ...
                (curve2d(2,min_2+2) > (val_2a - 0.05*val_2a)) ) &&  ...
               (curve2d(2,min_2) ~= 0)
            if min_2 > start
                [val_2a, min_2]  = max(curve2d(2,1:min_2-1));
            else
                offset = min_2+1;
                [val_2a, min_2]   = max(curve2d(2,offset:end));
                min_2 = offset + min_2;
            end
        end
    catch
    end
    try
        while ( (curve2d(2,max_2-2) < (val_2b + 0.05*val_2b)) || ...
                (curve2d(2,max_2+2) < (val_2b + 0.05*val_2b)) ) ...
                && (curve2d(2,max_2) ~= 0)
            if max_2 > start
                [val_2b, max_2]   = min(curve2d(2,1:max_2-1));
            else
                offset = max_2+1;
                [val_2b, max_2]   = min(curve2d(2,offset:end));
                max_2 = offset + max_2;
            end
        end
    catch
    end
    
    [val_3a, min_3]  = max(curve2d(3,:));
    [val_3b, max_3]  = min(curve2d(3,:));
    
    try
        while ( (curve2d(3,min_3-2) > (val_3a - 0.05*val_3a)) || ...
                (curve2d(3,min_3+2) > (val_3a - 0.05*val_3a)) ) ...
                && (curve2d(3,min_3) ~= 0)
            if min_3 > start
                [val_3a, min_3]  = max(curve2d(3,1:min_3-1));
            else
                offset = min_3+1;
                [val_3a, min_3]   = max(curve2d(3,offset:end));
                min_3 = offset + min_3;
            end
        end
    catch
    end
    try
        while ( (curve2d(3,max_3-2) < (val_3b + 0.05*val_3b)) || ...
                (curve2d(3,min_3+2) < (val_3b + 0.05*val_3b)) ) ...
                && (curve2d(3,max_3) ~= 0)
            if max_3 > start
                [val_3b, max_3]   = min(curve2d(3,1:max_3-1));
            else
                offset = max_3+1;
                [val_3b, max_3]   = min(curve2d(3,offset:end));
                max_3 = offset + max_3;
            end
            
        end
    catch
    end
    
	% dR2s als Basis
	p_min(ss) = quantile(U_1(U_1~=0), 0.25); % to get negative values
    
    p_max3(ss) = 0;
    p_max2(ss) = 0;
    
    % calculate aif in each slice, combining the first basis functions:
    
    % check for doublepeak (determines very good AIF properties):
    if ( (abs(val_2a./val_2b) >= 0.4) && (abs(val_2a./val_2b) <= 2.0) )
        
        p_max3(ss) = 0;
        
        if ( max_2 <= max_1 )
            
            % need NEGATIV as 1. Curve:
            p_max2(ss) = quantile(U_2(U_2~=0), 0.05);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_2(U_2 < p_max2(ss)))) .* curve2d(2, :));
            
        elseif ( min_2 <= max_1 )
            
            % need POSITIV:
            p_max2(ss) = quantile(U_2(U_2~=0), 0.95);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_2(U_2 > p_max2(ss)))) .* curve2d(2, :));
            
            U_loc_reshape(:,:,2,ss) = -U_loc_reshape(:,:,2,ss);
            p_max2(ss)              = -p_max2(ss);
            
        elseif ( min_2 > max_1 ) && ( max_2 > max_1 )
            
            if ( max_2 > min_2 )
                
                % need NEGATIV as 1. Curve:
                p_max2(ss) = quantile(U_2(U_2~=0), 0.05);
                
                aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                    (mean(mean(U_2(U_2 < p_max2(ss)))) .* curve2d(2, :));
                
            else  % ( max_2 < min_2 )
                
                % need POSITIV:
                p_max2(ss) = quantile(U_2(U_2~=0), 0.95);
                
                aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                    (mean(mean(U_2(U_2 > p_max2(ss)))) .* curve2d(2, :));
                
                U_loc_reshape(:,:,2,ss) = -U_loc_reshape(:,:,2,ss);
                p_max2(ss)              = -p_max2(ss);
                
            end
            
        end
        
        if ( (abs(val_3a./val_3b) >= 0.4) && (abs(val_3a./val_3b) <= 2.0) )
            
            if ( max_3 <= max_1 )
                
                % need NEGATIV as 1. Curve:
                p_max3(ss) = quantile(U_3(U_3~=0), 0.05);
                
                aif_svd(:,ss) = aif_svd(:,ss)' + ...
                    (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
                
            elseif ( min_3 <= max_1 )
                
                % need POSITIV:
                p_max3(ss) = quantile(U_3(U_3~=0), 0.95);
                
                aif_svd(:,ss) = aif_svd(:,ss)' + ...
                    (mean(mean(U_3(U_3 > p_max3(ss)))) .* curve2d(3, :));
                
                U_loc_reshape(:,:,3,ss) = -U_loc_reshape(:,:,3,ss);
                p_max3(ss)              = -p_max3(ss);
                
            elseif ( min_3 > max_1 ) && ( max_3 > max_1 )
                
                if ( max_3 > min_3 )
                    
                    % need NEGATIV as 1. Curve:
                    p_max3(ss) = quantile(U_3(U_3~=0), 0.05);
                    
                    aif_svd(:,ss) = aif_svd(:,ss)' + ...
                        (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
                    
                else  % ( max_3 < min_3 )
                    
                    % need POSITIV:
                    p_max3(ss) = quantile(U_3(U_3~=0), 0.95);
                    
                    aif_svd(:,ss) = aif_svd(:,ss)' + ...
                        (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
                    
                    U_loc_reshape(:,:,3,ss) = -U_loc_reshape(:,:,3,ss);
                    p_max3(ss)              = -p_max3(ss);
                    
                end
                
            end
        end
        
    elseif ( (abs(val_3a./val_3b) >= 0.5) && (abs(val_3a./val_3b) <= 2.0) )
        
        p_max2(ss) = 0;
        
        if ( max_3 <= max_1 )
            
            % need NEGATIV as 1. Curve:
            p_max3(ss) = quantile(U_3(U_3~=0), 0.05);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
            
        elseif ( min_3 <= max_1 )
            
            % need POSITIV:
            p_max3(ss) = quantile(U_3(U_3~=0), 0.95);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_3(U_3 > p_max3(ss)))) .* curve2d(3, :));
            
            U_loc_reshape(:,:,3,ss) = -U_loc_reshape(:,:,3,ss);
            p_max3(ss)              = -p_max3(ss);
            
        elseif ( min_3 > max_1 ) && ( max_3 > max_1 )
            
            if ( max_3 > min_3 )
                
                % need NEGATIV as 1. Curve:
                p_max3(ss) = quantile(U_3(U_3~=0), 0.05);
                
                aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                    (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
                
            else  % ( max_3 < min_3 )
                
                % need POSITIV:
                p_max3(ss) = quantile(U_3(U_3~=0), 0.95);
                
                aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                    (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
                
                U_loc_reshape(:,:,3,ss) = -U_loc_reshape(:,:,3,ss);
                p_max3(ss)              = -p_max3(ss);
                
            end
            
        end
        
    else % beide zusammen packen
        
        if ( max_2 <= max_1 ) && ( max_3 <= max_1 )
            
            % need NEGATIV as 1. Curve:
            p_max2(ss) = quantile(U_2(U_2~=0), 0.05);
            p_max3(ss) = quantile(U_3(U_3~=0), 0.05);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_2(U_2 < p_max2(ss)))) .* curve2d(2, :)) + ...
                (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
            
        elseif ( min_2 <= max_1 ) && ( min_3 <= max_1 )
            
            % need POSITIV:
            p_max2(ss) = quantile(U_2(U_2~=0), 0.95);
            p_max3(ss) = quantile(U_3(U_3~=0), 0.95);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_2(U_2 > p_max2(ss)))) .* curve2d(2, :)) + ...
                (mean(mean(U_3(U_3 > p_max3(ss)))) .* curve2d(3, :));
            
            U_loc_reshape(:,:,2:3,ss) = -U_loc_reshape(:,:,2:3,ss);
            p_max2(ss)              = -p_max2(ss);
            p_max3(ss)              = -p_max3(ss);
            
        elseif ( min_2 <= max_1 ) && ( max_3 <= max_1 )
            
            p_max2(ss) = quantile(U_2(U_2~=0), 0.95);
            p_max3(ss) = quantile(U_3(U_3~=0), 0.05);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_2(U_2 > p_max2(ss)))) .* curve2d(2, :)) + ...
                (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
            
            U_loc_reshape(:,:,2,ss) = -U_loc_reshape(:,:,2,ss);
            p_max2(ss)              = -p_max2(ss);
            
        elseif ( max_2 <= max_1 ) && ( min_3 <= max_1 )
            
            p_max2(ss) = quantile(U_2(U_2~=0), 0.05);
            p_max3(ss) = quantile(U_3(U_3~=0), 0.95);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_2(U_2 < p_max2(ss)))) .* curve2d(2, :)) + ...
                (mean(mean(U_3(U_3 > p_max3(ss)))) .* curve2d(3, :));
            
            U_loc_reshape(:,:,3,ss) = -U_loc_reshape(:,:,3,ss);
            p_max3(ss)              = -p_max3(ss);
            
        elseif ( max_2 <= max_1 )
            
            p_max3(ss) = 0;
            
            % need NEGATIV as 1. Curve:
            p_max2(ss) = quantile(U_2(U_2~=0), 0.05);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_2(U_2 < p_max2(ss)))) .* curve2d(2, :));
            
        elseif ( min_2 < max_1 )
            
            p_max3(ss) = 0;
            
            % need POSITIV:
            p_max2(ss) = quantile(U_2(U_2~=0), 0.95);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_2(U_2 > p_max2(ss)))) .* curve2d(2, :));
            
            U_loc_reshape(:,:,2,ss) = -U_loc_reshape(:,:,2,ss);
            p_max2(ss)              = -p_max2(ss);
            
        elseif ( max_3 <= max_1 )
            
            p_max2(ss) = 0;
            
            % need NEGATIV as 1. Curve:
            p_max3(ss) = quantile(U_3(U_3~=0), 0.05);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
            
        elseif ( min_3 <= max_1 )
            
            p_max2(ss) = 0;
            
            % need POSITIV:
            p_max3(ss) = quantile(U_3(U_3~=0), 0.95);
            
            aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                (mean(mean(U_3(U_3 > p_max3(ss)))) .* curve2d(3, :));
            
            U_loc_reshape(:,:,3,ss) = -U_loc_reshape(:,:,3,ss);
            p_max3(ss)              = -p_max3(ss);
            
        else
            
            p_max2(ss) = 0;
            p_max3(ss) = 0;
            
            if ( max_2 > min_2 )
                
                % need NEGATIV as 1. Curve:
                p_max2(ss) = quantile(U_2(U_2~=0), 0.05);
                
                aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                    (mean(mean(U_2(U_2 < p_max2(ss)))) .* curve2d(2, :));
                
            else  % ( max_2 < min_2 )
                
                % need POSITIV:
                p_max2(ss) = quantile(U_2(U_2~=0), 0.95);
                
                aif_svd(:,ss) = (mean(mean(U_1(U_1 < p_min(ss)))) .* curve2d(1, :)) + ...
                    (mean(mean(U_2(U_2 > p_max2(ss)))) .* curve2d(2, :));
                
                U_loc_reshape(:,:,2,ss) = -U_loc_reshape(:,:,2,ss);
                p_max2(ss)              = -p_max2(ss);
                
            end
            
            if ( max_3 > min_3 )
                
                % need NEGATIV as 1. Curve:
                p_max3(ss) = quantile(U_3(U_3~=0), 0.05);
                
                aif_svd(:,ss) = aif_svd(:,ss)' + ...
                    (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
                
            else  % ( max_3 < min_3 )
                
                % need POSITIV:
                p_max3(ss) = quantile(U_3(U_3~=0), 0.95);
                
                aif_svd(:,ss) = aif_svd(:,ss)' + ...
                    (mean(mean(U_3(U_3 < p_max3(ss)))) .* curve2d(3, :));
                
                U_loc_reshape(:,:,3,ss) = -U_loc_reshape(:,:,3,ss);
                p_max3(ss)              = -p_max3(ss);
                
            end
            
        end
    end
    
    data = U * D * V';
    
    data_reshape(:,:,s,:) = reshape(data, [nx ny nframes]);
    
    curve3d (:,:,ss) = curve2d;
    ss = ss+1;
end

mask_svd    = zeros([nx ny 5]);
maskn       = zeros(size(maskWB));

for k = 1:5
    
    fsvd = figure(fignr);
    
    % Welche Maske:
    if ( p_max3(k) == 0 ) 
        mask_svd(:,:,k) = ...
            (U_loc_reshape(:,:,1,k) < p_min(k) ) & ...
            (U_loc_reshape(:,:,2,k) < p_max2(k));
    elseif ( p_max2(k) == 0 )
        mask_svd(:,:,k) = ...
            (U_loc_reshape(:,:,1,k) < p_min(k) ) & ...
            (U_loc_reshape(:,:,3,k) < p_max3(k));
        
    else % ( p_max2(k) == 0 ) && ( p_max3(k) == 0 ) oder ( p_max2(k) ~= 0 ) && ( p_max3(k) ~= 0 )
        mask_svd(:,:,k) = ...
            (U_loc_reshape(:,:,1,k) < p_min(k) ) & (...
            (U_loc_reshape(:,:,2,k) < p_max2(k)) & ...
            (U_loc_reshape(:,:,3,k) < p_max3(k)) );
    end
    
    mask_svd(:,:,k)     = mask_svd(:,:,k) .* maskWB(:,:,s1+(k-1));
    
    if sum(sum(mask_svd(:,:,k))) == 0
        
        if ( p_max3(k) == 0 ) 
            mask_svd(:,:,k) = ...
                (U_loc_reshape(:,:,1,k) < 0 ) & ...
                (U_loc_reshape(:,:,2,k) < p_max2(k));
            %             (U_loc_reshape(:,:,1,k) < 0 ) & ...
        elseif ( p_max2(k) == 0 )
            mask_svd(:,:,k) = ...
                (U_loc_reshape(:,:,1,k) < 0 ) & ...
                (U_loc_reshape(:,:,3,k) < p_max3(k));
            
        else % ( p_max2(k) == 0 ) && ( p_max3(k) == 0 ) oder ( p_max2(k) ~= 0 ) && ( p_max3(k) ~= 0 )
            mask_svd(:,:,k) = ...
                (U_loc_reshape(:,:,1,k) < 0 ) & (...
                (U_loc_reshape(:,:,2,k) < p_max2(k)) | ...
                (U_loc_reshape(:,:,3,k) < p_max3(k)) );
        end
        
    end
    
    maskn(:,:,s1+(k-1)) = mask_svd(:,:,k);
    
    for n = 1:nframes
        datan           = data_reshape(:,:,s1+(k-1),n); %dscdata(:,:,:,n);
        datan2           = dscdata(:,:,s1+(k-1),n); %dscdata(:,:,:,n);
        aif_svd_1(n,k)     = mean(datan2(maskn(:,:,s1+(k-1)) == 1));
        aif_svd_real(n,k)    = mean(datan(maskn(:,:,s1+(k-1)) == 1));
    end
    
    % -> find minimum of individual curves
    % -> curves are normalized for comparison
    % USE SVD Curves or REAL curves?
    % -> REAL Signal otherwise possible negative values!!!
    
    [PVc(k), TTPc(k)] = max( aif_svd_real(:,k), [], 1);
    PWc(k)            = median(Index_E(maskn(:,:,s1+(k-1))==1) ...
                        - Index_A(maskn(:,:,s1+(k-1))==1));
    InAc(k)           = median(Index_A(maskn(:,:,s1+(k-1))==1));
    
    idx_plot_orig(:,:,s1+(k-1)) = k .* mask_svd(:,:,k);
end
clear maskn
fignr = fignr + 1;

% *************************************************************************
% (3) Find best curve
% *************************************************************************

PVc(isnan(PVc))     = 1;
PWc(isnan(PWc))     = 0;
TTPc(isnan(TTPc))   = 0;

QM = ( PVc./mean(PVc) ) ...
    ./ ( ( (TTPc./ mean(TTPc)) ...
    .*  (PWc ./ mean(PWc)) ).^2 ...
    );

QM(isnan(QM))     = 0;

[~, aif_cluster]    = max(QM);

idx_plot(idx_plot_orig == aif_cluster) = 1;
idx                 = sum(idx_plot(:));

aif_svd = aif_svd_real;

end
