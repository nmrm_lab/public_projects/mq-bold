%% Smooting TTP
% - Toolbox for batch processing ASL perfusion based fMRI data.
% - All rights reserved.
% - Ze Wang @ TRC, CFN, Upenn 2004
% - Started in SPM2
% - Using smooth TTP implemented by SK 13JUL16

%% Required subscripts

% - SPM12


%% Modifications


%% Main program

function smooth_TTP(path_TTP, path_sTTP)

    % ------------------------------------------------------------------------
    % INITIALIZE
    % ------------------------------------------------------------------------

    % Define Smoothing Kernel
    fwhm_smooth = [5];

    % print status
    fprintf('Started smooting the TTP image.\n');

    % ------------------------------------------------------------------------
    % SMOOTH
    % ------------------------------------------------------------------------

    %now call spm_smooth with kernel defined at PAR
    spm_smooth(path_TTP, path_sTTP, fwhm_smooth);

    % print status
    fprintf('Finished smooting the TTP image.\n');
    
end