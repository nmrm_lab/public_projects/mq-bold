function calc_CBV_TikhonovSVD(SubjectDir, DSC_pars, S0, S0_sd, C_t, C_a_fit, ...
                         start, ende, NAWM, maskWB, V_save, kH, roh, fignr)
nx = DSC_pars.nx;
ny = DSC_pars.ny;
nslices = DSC_pars.nslices;
nframes = DSC_pars.nframes;
time_dyn = DSC_pars.time_dyn;
save_path = SubjectDir.DSC.CBV;
                        
%%	Model Independent Deconvolution, based on matrix equations 
%   (Ostergaard 1996)
%   Deconvolution Method: Singular Value Decomposion
%   Reduce Noise effects: Cut-off value for diagonal matrix SNR dependent 
%   (= simplest Regularization)
%   --> siehe (Ostergaard 1996, Knutsson 2010)
%   Solution of equation: AIF * (CBF*ResidueFunction) 
%                         = Concentration-Time-Curve
%   --> A * c = b

%   - *AK_24JUN2015*
%       + CBF in ml/100g/min (old: ml/100g/sec)

disp('========================================================================');
disp('Start Leakage Correction: Methods 3');
disp('========================================================================');
tic
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Declare Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt          = time_dyn(2); % temporal resolution of Scan [ms]

b_m3        = zeros(nx,ny,nslices, nframes); % Matrix for dR2* / Ct
c_m3a       = b_m3;                          % Matrix for H(t)
resf_m3c    = c_m3a;                         % Matrix for Residue Function
resf_m3d    = c_m3a;
f_m3c       = zeros(nx,ny,nslices);   % Matrix for f (proportional to CBF)
f_m3d       = f_m3c;
ind_m3c     = zeros(nx,ny,nslices,1); % Index of Tc in Time Curve
ind_m3d     = ind_m3c;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dispose Cut-off value on Diagonal Matrix D:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% SNR dependent according to Knutsson 2004:
SNR             = S0./S0_sd;
SNR(isnan(SNR)) = 0.0;
SNR(isinf(SNR)) = 0.0;
SNR             = mean(SNR(maskWB==1));

if (SNR <= 70) && (SNR > 0)
    cut_off = 0.2;
    cut_thi = cut_off/1000;
else
    cut_off = 0.1;
    cut_thi = cut_off/1000;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate AIF Data Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Standard nach Ostergaard / Bjornerud:
% including relaxivity:
for n = 1:nframes
    A1(n:nframes, n) = C_a_fit(1:(nframes-(n-1))); % [mM]
end
A1 = dt .* A1; % [ms * mM]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Singular Value Decomposion:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Standard SVD:
[U3, D3, V3]    = svd ( A1);     % AIF = U*D*V';
[U03, D03, V03]    = csvd ( A1); % AIF = U*D*V';

% Restrict Oscillations:
% - Je hoeher cutt-off value desto flacher die Oscillations
% - allerdings auch 1., die CBF definiert!

D3 ( D3 < ( max(D3(:)) * cut_off ))  = 0.0; % cut-off Value
D03( D03< ( max(D03(:))* cut_thi  )) = 0.0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reshape for PARFOR:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

maskWB_parfor = reshape(maskWB,[(nx*ny*nslices) 1]); % correction applied within 
                                                 % brain mask
C_t_parfor  = reshape(C_t, [(nx*ny*nslices) nframes]); % [mM]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reshape for PARFOR:

b_parfor        = reshape(b_m3,  [(nx*ny*nslices) nframes]);
b_m3c_parfor    = b_parfor;
c_m3c_parfor    = reshape(c_m3a, [(nx*ny*nslices) nframes]);
c_m3d_parfor    = c_m3c_parfor;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start Calculation of H(t):
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('... Start Correction of CBV - step 2/6...');

parfor ll = 1:(nx*ny*nslices)
    if maskWB_parfor(ll,1) == 1
        
        % *********************************************************
        % Method 3c: Bjornerud 2011
        % *********************************************************
        % - with relaxivity corrected input
        b_m3c_parfor(ll,:) = C_t_parfor(ll,:); % [mM]
        % [1/ms] -> should cancel out (AIF in [ms * mM])
        c_m3c_parfor(ll,:) = V3 * pinv(D3) * (U3' * b_m3c_parfor(ll,:)');      
        
        % *********************************************************
        % Method 3d: Bjornerud 2011
        % *********************************************************
        % - Tikhonov Regularisation - L-curve criterion: Scripts by Hansen
        % - with relaxivity corrected input
        [lambda3, ~, ~, ~] = l_curve(U03,D03,b_m3c_parfor(ll,:)','Tikh');
        [x_l3,~,~]         = tikhonov(U03,D03,V03,b_m3c_parfor(ll,:)',...
                                                                lambda3);
        c_m3d_parfor(ll,:) = x_l3; % [1/ms] -> should cancel out 
                                   %           (AIF in [ms * mM])     
   end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reshape back for PARFOR:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c_m3d = reshape(c_m3d_parfor, [nx ny nslices nframes]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate f - proportional constant to CBF:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[f_m3d(:,:,:), Tmax_m3d(:,:,:)] = max(c_m3d(:,:,:,1:end-1),[],4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate Residue Function R(t):
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for l = 1:nframes
    resf_m3d(:,:,:,l) = c_m3d(:,:,:,l) ./ f_m3d(:,:,:);  
end

resf_m3d(isnan(resf_m3d)) = 0.0; resf_m3d(isinf(resf_m3d)) = 0.0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reshape back for PARFOR:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

resf_m3d_parfor = reshape(resf_m3d, [(nx*ny*nslices) nframes]);

Ka_mean_m3d_parfor = zeros([nx *ny* nslices, 1]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pixelwise Fit of R(t) to get Tc (Capillary transit time):
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% *********************************************************
% Method 3: Bjornerud 2011
% *********************************************************

disp('... Start Correction of CBV - step 3/6...');

% Options for R(t) fit:
opts        = optimoptions(@lsqcurvefit,	...
    'Algorithm',    'trust-region-reflective',  ...
    'Display',      'off',                  ...
    'TolFun',       1.0000e-08,             ...
    'TolX' ,        1.0000e-08,             ...
    'MaxFunEvals',  400,...
    'ScaleProblem', 'Jacobian');

figure(fignr);
ll = 1;

time_init = time_dyn(1:6); 
%SK08JUL16: Added line in order to set dimensions correctly 
%for 'least_squares_varpro_gss_cg.m'
time_init = reshape(time_init, 1, 6); 
time_max  = max(time_dyn'*dt);
fit_lor   = @( x_, ga_, p_) (1 ./ ( 1 + ( (pi() .* x_) ./ (2 .* ga_) ) .^2 ));

[ ~, ~, coeff_m3d_parfor, res_m3d_parf] = ...
                        least_squares_varpro_gss_cg( time_init, ...
                        squeeze(resf_m3d_parfor(:,1:6)), [] , ...
                        fit_lor, 0, time_max, [], 1e-8);

for ff = 1:nframes
    coeff_m3d_parfor(maskWB_parfor==0,ff) = time_dyn(ende); 
end

parfor ll = 1:(nx*ny*nslices)
    
    if maskWB_parfor(ll) == 1
        [~,ind_m3d(ll) ] = min(abs(time_dyn-coeff_m3d_parfor(ll)));
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate Ka / K2:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('... Start Correction of CBV - step 4/6...');
t1 = start;
for ll = 1:(nx*ny*nslices)
    if maskWB_parfor(ll) == 1
        
        % vereinfacht mit mean statt exp-fit:
        % - mean ab 4*Tc (if possible), da assumption only valid for t >> Tc
        % - mean bis start AIF (t1), da ab da curve gegen Null geht
        factor = 4; 
        Ka_mean_m3d_parfor(ll) = ...
            mean(c_m3d_parfor(ll,(factor*ind_m3d(ll)):(end-t1)),2); % [1/ms]
        
        % Check if mean ab 4*Tc is possible (Tc to large), 
        % otherwise would result be 0:       
        if (factor*ind_m3d(ll))>=(nframes-t1)
            Ka_mean_m3d_parfor(ll) = ...
                mean(c_m3d_parfor(ll,(4*ind_m3d(ll)):(end-t1)),2);
            if (4*ind_m3d(ll))>=(nframes-t1)
                Ka_mean_m3d_parfor(ll) = ...
                    mean(c_m3d_parfor(ll,(2*ind_m3d(ll)):(end-t1)),2);
            end
        end
        
              
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reshape back for PARFOR:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% - negative, to equalize with other K2 values

K2_m3d = reshape(-Ka_mean_m3d_parfor, [nx ny nslices]);

% isnan / isinf bereinigen:
K2_m3d(isnan(K2_m3d)) = 0.0; K2_m3d(isinf(K2_m3d)) = 0.0;

close (fignr); % only important for GUI Programm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CBV, CBF, MTT Calculation:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('... Start Calculation of CBV - step 6/6...');

% uncorrected CBV:
CBV_unc_svd_m3d = (kH / roh) .* trapz(time_dyn, c_m3d(:,:,:,:), 4);

% corrected CBV:
CBV_corr_svd_m3d	= CBV_unc_svd_m3d ...
    + (K2_m3d  .* dt .* (nframes-ind_m3d) .* kH./ roh);

% CBF = Cerebral Blood Flow:
CBF_m3d = (kH / roh) .* f_m3d * 1000;       % 1/(100g/ml) * 1/sec
CBF_m3d = CBF_m3d .* 60;                    % umrechnung in ml/100g/min

% MTT = Mean Transit Time:
max_MTT = time_dyn(end) ./ 1000 ./ 60; %in min

MTT_unc_m3d                         = CBV_unc_svd_m3d ./ CBF_m3d;
MTT_unc_m3d(MTT_unc_m3d < 0)        = 0.0;
MTT_unc_m3d(MTT_unc_m3d > max_MTT)  = max_MTT;

MTT_m3d                     = CBV_corr_svd_m3d ./ CBF_m3d;
MTT_m3d(MTT_m3d < 0)        = 0.0;
MTT_m3d(MTT_m3d > max_MTT)  = max_MTT;

    %% Save MTT in sec:
    V_save.fname = fullfile(SubjectDir.DSC.MAPS, 'MTT_unc_TiSVD.nii');
    spm_write_vol(V_save, MTT_unc_m3d.*60);
    V_save.fname = fullfile(SubjectDir.DSC.MAPS, 'MTT_TiSVD.nii');
    spm_write_vol(V_save, MTT_m3d.*60);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Save K2 Values:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

V_save.fname = fullfile(SubjectDir.DSC.MAPS, 'K2_TiSVD.nii');
spm_write_vol(V_save, K2_m3d);

toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save calculated SVD-based CBV, MTT and CBF parameter maps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
    %% Save uncorrected, normalized CBV:
    % - normalize, restrict SVD CBV:
    nCBV_unc_svd_m3d = normalize_CBV(CBV_unc_svd_m3d, NAWM);
    V_save.fname = fullfile( save_path, 'nCBV_unc_TiSVD.nii');
    spm_write_vol(V_save, nCBV_unc_svd_m3d);
    
    %% Save corrected, normalized CBV:
    % - normalize, restrict SVD CBV:
    nCBV_corr_svd_m3d = normalize_CBV(CBV_corr_svd_m3d, NAWM);
    V_save.fname = fullfile(save_path, 'nCBV_corr_TiSVD.nii');
    spm_write_vol(V_save, nCBV_corr_svd_m3d);
    
    if DSC_pars.absolutCBV == 1
        %% Save uncorrected: 
        V_save.fname = fullfile( save_path, 'CBV_unc_TiSVD.nii');
        spm_write_vol(V_save, CBV_unc_svd_m3d);
    
        %% Save corrected,restricted CBV:
        CBV_corr_svd_m3d = restrict_CBV(  CBV_corr_svd_m3d);
        V_save.fname = fullfile( save_path, 'CBV_corr_TiSVD.nii');
        spm_write_vol(V_save, CBV_corr_svd_m3d);

        %% Save unnormalized CBF:
        V_save.fname = fullfile(SubjectDir.DSC.MAPS, 'CBF_TiSVD.nii');
        spm_write_vol(V_save, CBF_m3d);
    end
    
    %% Save normalized CBF:
    nCBF_m3d = normalize_CBF(CBF_m3d, NAWM);
    V_save.fname = fullfile(SubjectDir.DSC.MAPS, 'nCBF_TiSVD.nii');
    spm_write_vol(V_save, nCBF_m3d);
end