%% Normalize CBF to defined Region
%   - Input:
%       + CBFin: 3D CBF Volume to be normalized
%       + roi: 3D ROI of NAWM
%   - Output:
%       + CBFout: Normalized 3D CBF Volume
%   - roi: defined normalization mask (= NAWM)
%   - CBF Literature value follows Ostergaard et al. MRM 36 (1996) 726-736.

function [CBFout] = normalize_CBF(CBFin, roi)

% Define CBF in NAWM for normalization
CBF_NAWM_mean_set = 22;

% Get 3D volume
if nargin == 1
    [aver, ~]               = newroi_dsc( CBFin , 4);
end

if nargin == 2
    if numel(roi) == 4
        [aver, ~]	= newroi_dsc( CBFin , 4, roi);
    else
        aver	 	= median(CBFin(roi==1));    
    end
end

% Get old mean of CBF in NAWM
CBF_NAWM_mean_in = mean(CBFin(roi));
% Get normalization factor
norm_factor = CBF_NAWM_mean_set/CBF_NAWM_mean_in;
% Apply normalization factor
CBFout = CBFin * norm_factor;

CBFout(isnan(CBFout))   = 0.0;
CBFout(isinf(CBFout))   = 0.0;
CBFout( CBFout < 0.0)   = 0.0;

end