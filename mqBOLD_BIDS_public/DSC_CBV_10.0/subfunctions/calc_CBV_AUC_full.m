function calc_CBV_AUC_full(SubjectDir, DSC_pars, dR2s, dR2s_mean, dR2s_a, ...
                        C_t, C_a, rMPR, NAWM, maskWB, opts, V_save, r_t, kH,...
                         roh, fignr)

nx = DSC_pars.nx;
ny = DSC_pars.ny;
nslices = DSC_pars.nslices;
nframes = DSC_pars.nframes;
time_dyn = DSC_pars.time_dyn;
save_path = SubjectDir.DSC.CBV;

%%*************************************************************************
% CBV CALCULATION - uncorrected - (2/2)
%%*************************************************************************

% -------------------------------------------------------------------------
% Absolutes CBV - uncorrected:
%
% = Integral (Trapezoidal Integration) of concentration time curves (4D)
%   divided by Integral over AIF Concentration:
%
% = including correction factors
% -------------------------------------------------------------------------

% uncorrected absolute CBV (full integration):
CBV_unc_auc_full = ((kH / roh) .* (trapz(C_t(:,:,:,: ), 4) ...
                    ./ trapz(abs (C_a(:,:)), 2))) .* maskWB; % mit abs C_a
CBV_unc_auc_full = restrict_CBV( CBV_unc_auc_full);

if DSC_pars.absolutCBV == 1
    V_save.fname = fullfile( save_path, 'CBV_unc_auc_full.nii');
    spm_write_vol( V_save, CBV_unc_auc_full);  
end

% Normalize uncorrected CBV
nCBV_unc_auc_full   = normalize_CBV( CBV_unc_auc_full, NAWM);
V_save.fname        = fullfile( save_path, 'nCBV_unc_auc_full.nii');
spm_write_vol( V_save, nCBV_unc_auc_full);
    
% Save image to check coreg: MPR & CBV
path_CBVimg=fullfile(SubjectDir.DSC.toplevel, 'Processings/CBV');
 %range_width = 4;
 range = [-5 5]; %random numbers
 Overlay_anatomy(rMPR, nCBV_unc_auc_full, path_CBVimg, range);
        
%% Batch for Boxerman/Weisskoff 2006 Leakage-Correction

% ********** auc **********
% - Boxerman/Weisskoff 2006
% - K2 positive and negative
% - with dR2*

% CP 07JUl17:   - join AUC_CBV_mod.m and AUCmodels_mod
%               - separate into 
%                   - first pass integration: calc_CBV_AUC_fp
%                   - full integration: calc_CBV_AUC_full
disp('=========================================================================');
disp('Start Leakage Correction: Boxerman/Weisskoff full integration (Methods 2)');
disp('=========================================================================');
tic
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Declare Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% gobals for model fitting:
%global dR2s_mean;

% K1:
K1_auc = zeros( nx, ny, nslices);

% K2:
K2_auc = zeros( nx, ny, nslices);

% Residuals from fits:
res_1a = zeros( nx, ny, nslices, nframes);

% fitoptions: Start values for coeff [K1 K2]
startval  = [1 , 0];                     
save('savedR2s_mean.mat','dR2s_mean');
% reference curve (mean of nonenhancing Pixel) in 1/ms
curve_r = squeeze(dR2s_mean);          


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fit over all Voxel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear mod;

for s = 1:nslices % Schichten
    
    series(:,:,:) = squeeze( dR2s(:,:,s,:) );
    
    for k = 1:ny
        
        for j = 1:nx
            
            if maskWB(j,k,s) == 1   % whole brain mask
                
                value = squeeze( series(j,k,:));
                        
                % *********************************************************
                % Method 1a: Boxerman/Weisskoff 2006
                % *********************************************************
                
                % se help to 'System of Linear Equation' one of the examples 
                % to 'Overdetermined Systems'
                % size musst be: nframes 2
                Matrix  = [curve_r;  -cumtrapz(time_dyn, curve_r)]';  
                Coeff_1a= Matrix \ value;
                
                % If matrix division does not work:
                if Coeff_1a(1) < 0
                    Coeff_1a  = lsqcurvefit( @model_Boxerman, startval, ...
                                time_dyn, value', [0 -Inf], [Inf Inf], opts);
                    Coeff_1a  = Coeff_1a';
                end
                
                K1_auc(j,k,s) = Coeff_1a(1);
                K2_auc(j,k,s) = Coeff_1a(2);
                % residuals = yfit - ydata
                res_1a(j,k,s,:) = (Matrix*Coeff_1a) - value; 
                
                % output examples of fitted curves
                if mod(k, 64) == 0 && mod(j, 64) == 0
                    figure(fignr);
                    clf;
                    
                    plot( time_dyn, value, '.', 'Color', 'red');    % meas
                    hold all;
                    
                    plot( time_dyn, Matrix*Coeff_1a, '--');         % auc
                    
                    xlabel('time (ms)');   
                    ylabel('dR2s in 1/ms');
                    legend('meas','auc');
                    hold off;
                    
                end
                
            end % if
        end % nx
    end % ny
end % nslices

% Restrict values:
K1_auc(isnan(K1_auc)) = 0.0; K1_auc(isinf(K1_auc)) = 0.0;

K2_auc = K2_auc .* (1000*60); % convert to 1/min...
K2_auc(isnan(K2_auc)) = 0.0; K2_auc(isinf(K2_auc)) = 0.0;

% Save K2 Values:
V_save.fname = fullfile( SubjectDir.DSC.K2, 'K2_auc.nii');
spm_write_vol(V_save, K2_auc);

%% batch_AUC_CBV: Calculate corrected dR2s and CBVs
% - after batch_AUCmodels
% - includes save maps

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Methods 1,2:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K2_auc = K2_auc ./ (1000*60);

% calculate corrected dR2s:
dR2s_corr_auc = zeros(size(dR2s));

for f = 1:nframes
    dR2s_corr_auc(:,:,:,f) = dR2s(:,:,:,f) + ...
        (K2_auc .* trapz(time_dyn(1:f), dR2s_mean(1:f), 2));
end

% calculate corrected dR2s of blood pixels:
% - probably its not only blood, therefore correction could be meaningful
dR2s_a_corr_auc = dR2s_a;
%%*************************************************************************
% CBV CALCULATION - corrected
%%*************************************************************************

%%--------------------------------------------------------------------------
% rCBV corrected nach Boxerman/Weisskoff 2006:
% = Trapezoidal Integration over time (Spacing = time_dyn(2))
%%--------------------------------------------------------------------------

%%--------------------------------------------------------------------------
% Relatives CBV - corrected:
%%--------------------------------------------------------------------------

% full integration:
rCBV_corr_auc_full = trapz(dR2s_corr_auc(:,:,:,: ), 4) .* time_dyn(2) .* maskWB;

%%--------------------------------------------------------------------------
% Absolutes CBV - corrected:
%%--------------------------------------------------------------------------

% Conversion to Concentrations:
C_t_auc = dR2s_corr_auc ./ r_t;

C_a_auc = dR2s_a_corr_auc' ./ r_t;

% Calculate absolute CBV:
dt = time_dyn(2) - time_dyn(1);

% full integration:
CBV_corr_auc_full_auc = ((kH / roh) .* (trapz(C_t_auc(:,:,:,:), 4)...
                        ./ trapz(abs (C_a_auc(:,:)), 2)) ) .* maskWB;

% Normalize corrected CBV:

nCBV_corr_auc_full = normalize_CBV(rCBV_corr_auc_full, NAWM);

%%*************************************************************************
% SAVE ALL CBV
%%*************************************************************************

%% Save normalized, restricted CBV (full integration):

V_save.fname = fullfile( save_path, 'nCBV_corr_auc_full.nii');
spm_write_vol( V_save, nCBV_corr_auc_full);

if DSC_pars.absolutCBV == 1
    
    %% Save absolut CBV (full integration):

    CBV_corr_auc_full_auc = restrict_CBV( CBV_corr_auc_full_auc);
    V_save.fname          = fullfile( save_path, 'CBV_corr_auc_full.nii');
    spm_write_vol( V_save, CBV_corr_auc_full_auc);

    %% Save K1:

    K1_auc       = restrict_CBV( K1_auc);
    V_save.fname = fullfile( save_path, 'K1_auc.nii');
    spm_write_vol( V_save, K1_auc);
end
toc
end