function [aifslice, dR2s_a] = select_aif_manual(SubjectDir,dscdata,S0,start,...
            Index_A,Index_TTP, Peak_height, Peak_width, rMPR, maskWB, DSC_pars,...
            fignr)
%%--------------------------------------------------------------------------
%% Arterial Input Function (AIF)
%%--------------------------------------------------------------------------
scrsz = get(0,'ScreenSize');  % Screensize

nframes = DSC_pars.nframes;
TE = DSC_pars.TE;

% Threshold for vessels in MPRAGE - only for orientation:
threshold_vessel = max(max(rMPR(:,:,1)));% 1st try about 1700 
prob_vessel      = ( rMPR > threshold_vessel/2);

% Variablen Definition:
input_1   = 1;

difference= S0 - Peak_height;
aif       = zeros( nframes, 1);
aifvoxels = zeros( 1, 2);

fg_aif = figure('Position',[10 1 scrsz(3)/4 scrsz(4)]);
fignr = fignr+1;

while input_1 == 1
    
    % slice selection:
    prompt      = {'Slice for AIF Selection'};
    def         = {num2str(1)};
    answer      = inputdlg(prompt,'Integration Range',1,def);
    aifslice    = answer{1};
    aifslice    = str2double(aifslice);
    
    input_1     = 0;
    
    %Highlight Pixels with artery like signal properties
    % criteria of Pixel:
    arterial0 = Index_TTP   <= mean(Index_TTP(maskWB==1 & Index_TTP > 0)) & (Index_TTP > 0);  % early Peak
    arterial1 = Index_A     <= start+2;                                                       % early Signaldrop
    arterial2 = difference  >= mean(difference(maskWB==1));                                   % high Peak
    arterial3 = Peak_width  <= mean(Peak_width(Peak_width > 0 & maskWB==1));                  % small Peakwidth
    
    % Generate Output Image for AIF selection:
    if exist('rMPR', 'var')  % if for MPRAGE is path in GUI selected *AK_27MAY2015*
        arterial = difference .* (3*prob_vessel + arterial0 + 3*arterial1 + arterial2 + arterial3); % empirisch
    else
        arterial = difference .* ( arterial0 + 3*arterial1 + arterial2 + arterial3); % empirisch - no rMPR
    end
    
    arterial(isnan(arterial)) = 0.0;
    arterial(isinf(arterial)) = 0.0;
    
    figure(fg_aif);
    subplot(2,1,1), newdisp_oxy(fliplr(arterial(:,:,aifslice)), 0, 0, ...
        ['Image, slice# ', num2str(aifslice), ' (AIF voxel selection)'] );
    
    choice = questdlg('Accept slice for AIF?', 'AIF Slice', 'Yes', 'No','Yes');
    
    switch choice
        case 'Yes'
            
            zoom on;
            input('\n\nZoom the area of interest to create a roi and press enter\n')
            zoom off;
            
            title('Please select blood voxels in figure, then press ENTER');
            
            [xmax, ymax]    = getpts(fg_aif);
            xmax            = round(xmax);
            ymax            = round(ymax);
            
            for i = 1:numel(xmax) % allow more aifvoxels *AK_27MAY2015*
                
                aifvoxels(i,:)  = [xmax(i), ymax(i)];
                aif  = aif + squeeze( dscdata(xmax(i), ymax(i), aifslice, :));
                
            end;
            
            aif  = aif ./ numel(xmax);  % allow more aifvoxels *AK_27MAY2015*
            
            figure(fg_aif);
            subplot(2,1,2), plot( aif);
            hold all
            
            title(['AIF of slice# ', num2str(aifslice), ' (manual selection)']);
            xlabel('dyn scan number');
            ylabel('MR signal (a.u.)');
            
            choice = questdlg('Accept AIF?', 'AIF', 'Yes', 'No','Yes');
            
            switch choice
                case 'No'
                    input_1 = 1;
            end % switch
            
        case 'No'
            
            input_1 = 1;
            
    end % switch
    
    
end % while

hold off;
%close(fignr-1);
close(fg_aif);

fignr = fignr + 1;

clear arterial0; clear arterial1; clear arterial2; clear arterial3;
clear xmax;      clear ymax;

% Calculate dR2s curve of AIF:
S0_a   = mean (aif(1:start,:), 1);
dR2s_a = -(1/TE) * log(aif ./ S0_a);        % same calculation as for tissue

dR2s_a(dR2s_a < 0.0) = 0.0;                 % values < 0 impossible! - noise


    % Save AIF results as tif SK11JUL16
    fh      = figure('MenuBar','None');
    plot(dR2s_a);
    title('Resulting AIF');
    xlabel('Dynamic #');
    xlabel('Signal(dR2s)');
    outfile=fullfile(SubjectDir.DSC.toplevel, 'Processings', ...
        '04_AIF_results.tif');
    check_folder(fileparts(outfile));
    saveas(gcf,outfile);
           
    uiwait(gcf,20); % wait until figure is closed or 20 sec are gone           
    close all

% PVE criterion:
% - Calamante 2013, Bleeker 2011
% - post-bolus steady state conventration / AUC = const. over brain
% - except if PVE occure

end