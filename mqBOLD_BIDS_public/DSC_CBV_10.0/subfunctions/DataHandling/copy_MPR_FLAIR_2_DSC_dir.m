%% Copy processed MPR & FLAIR data to DSC folder 
% - Original program from Christine Preibisch.
% - Major changes by Stephan Kaczmarz at 21OCT15.
% - Major changes by CP 01JAN2018


function [SubjectDir, filelist] = copy_MPR_FLAIR_2_DSC_dir(SubjectDir, filelist,sb)

    % ---------------------------------------------------------------------
    % COPY T1w (MPRAGE)
    % ---------------------------------------------------------------------
    SubjectDir.DSC.T1w = fullfile(SubjectDir.output,'qBOLD','DSC','T1w');
    %create folder (if it does not exist already)
    check_folder(SubjectDir.DSC.T1w);    
    % Copy files
    copyfile(filelist.T1w_nii.anat, SubjectDir.DSC.T1w);
    copyfile(filelist.T1w_nii.GM, SubjectDir.DSC.T1w);
    copyfile(filelist.T1w_nii.WM, SubjectDir.DSC.T1w); 
    copyfile(filelist.T1w_nii.CSF, SubjectDir.DSC.T1w);
    copyfile(filelist.T1w_nii.BrMsk_CSF, SubjectDir.DSC.T1w);
    copyfile(filelist.T1w_nii.BrMsk, SubjectDir.DSC.T1w);
    filelist.DSC.T1w_nii = spm_select( 'FPList', SubjectDir.DSC.T1w, ...
        '^sub.*\.nii$');
	filelist.DSC.c1T1w_nii = spm_select( 'FPList', SubjectDir.DSC.T1w, ...
        '^c1sub.*\.nii$');
	filelist.DSC.c2T1w_nii = spm_select( 'FPList', SubjectDir.DSC.T1w, ...
        '^c2sub.*\.nii$');    
	filelist.DSC.c3T1w_nii = spm_select( 'FPList', SubjectDir.DSC.T1w, ...
        '^c3sub.*\.nii$'); 
 	filelist.DSC.cBrMsk_T1w_nii = spm_select( 'FPList', ...
        SubjectDir.DSC.T1w, '^cBrMsk.nii$');  
  	filelist.DSC.cBrMsk_CSF_T1w_nii = spm_select( 'FPList', ...
        SubjectDir.DSC.T1w, '^cBrMsk.nii$'); 
    
    % ------------------------------------------------------------------------
    % COPY FLAIR
    % ------------------------------------------------------------------------
    SubjectDir.DSC.FLAIR = fullfile(SubjectDir.output,'qBOLD','DSC','FLAIR');
    %create folder (if it does not exist already)
    check_folder(SubjectDir.DSC.FLAIR);    
    % Copy files
    copyfile(filelist.FLAIR_nii.anat, SubjectDir.DSC.FLAIR);
    copyfile(filelist.FLAIR_nii.GM, SubjectDir.DSC.FLAIR);
    copyfile(filelist.FLAIR_nii.WM, SubjectDir.DSC.FLAIR); 
    copyfile(filelist.FLAIR_nii.CSF, SubjectDir.DSC.FLAIR);
    copyfile(filelist.FLAIR_nii.BrMsk_CSF, SubjectDir.DSC.FLAIR);
    copyfile(filelist.FLAIR_nii.BrMsk, SubjectDir.DSC.FLAIR);
    filelist.DSC.FLAIR_nii = spm_select( 'FPList', SubjectDir.DSC.FLAIR, ...
        '^sub.*\.nii$');
	filelist.DSC.c1FLAIR_nii = spm_select( 'FPList', SubjectDir.DSC.FLAIR, ...
        '^c1sub.*\.nii$');
	filelist.DSC.c2FLAIR_nii = spm_select( 'FPList', SubjectDir.DSC.FLAIR, ...
        '^c2sub.*\.nii$');    
	filelist.DSC.c3FLAIR_nii = spm_select( 'FPList', SubjectDir.DSC.FLAIR, ...
        '^c3sub.*\.nii$'); 
 	filelist.DSC.cBrMsk_FLAIR_nii = spm_select( 'FPList', ...
        SubjectDir.DSC.FLAIR, '^cBrMsk.nii$');  
  	filelist.DSC.cBrMsk_CSF_FLAIR_nii = spm_select( 'FPList', ...
        SubjectDir.DSC.FLAIR, '^cBrMsk.nii$');    
    % Print status
    fprintf('Subject %d: Finished copying MPR & FLAIR folders condition DSC folder\n',sb);
end
