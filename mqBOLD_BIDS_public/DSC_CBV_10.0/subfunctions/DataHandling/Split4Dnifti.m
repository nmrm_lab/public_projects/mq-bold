%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Split 4D Nifti DSC data to 3D nifty & extract acqusition parameters
% =========================================================================
%   - CP 16MAR2020
% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  DSC_pars = Split4Dnifti(SubjectDir, filelist, DSC_pars, flag)
 
% extract acqusition info from json file
path_json = dir(fullfile(SubjectDir.DSC.Perfusion, '*.json'));
jsoninfo = jsondecode(fileread(fullfile(path_json.folder, path_json.name)));
DSC_pars.TE = jsoninfo.EchoTime;
DSC_pars.TR = jsoninfo.RepetitionTime;
DSC_pars.Manufacturer = jsoninfo.Manufacturer;
V = spm_vol(filelist.DSC.perfusion.nii);   
dimension = V.dim;
DSC_pars.nx = dimension(1);
DSC_pars.ny = dimension (2);
DSC_pars.nslices = dimension(3);
DSC_pars.nframes = length (V);     
DSC_pars.time_dyn = DSC_pars.TR:DSC_pars.TR:DSC_pars.nframes*DSC_pars.TR; 
    
% split 4D nifty time series of 3D nifti
data = spm_read_vols(V);
 
tV = V(1);
tV = rmfield(tV,'private');
 
[dn,fn,ext] = fileparts(filelist.DSC.perfusion.nii);
 
for ctr=1:size(data,4)
    tV.fname = sprintf('%s%so_%s_%.3d%s',dn,filesep,fn,ctr,ext);
    fprintf('Writing %s\n',tV.fname);
    spm_write_vol(tV,data(:,:,:,ctr));
    if flag.reset_origin
        [~,filename,~] = fileparts(tV.fname);
        reset_origin(SubjectDir.DSC.Perfusion,sprintf('%s.nii',filename));
    end
end
fprintf('done.\n');    

end