function [filelist, WM_T1w, GM_T1w, CSF_T1w, maskWB, maskWM, maskGM, maskCSF, ...
            HealthyMask, LesionMask, LeakageMask, FLAIRmask, fignr] = ...
            GenerateMasks(SubjectDir, filelist, DSC_pars, rMPR, rFLAIR, fignr)
% Initialize Parameters
scrsz       = get(0,'ScreenSize');  % Screensize
prob(1:3)   = [0.75 0.75 0.75];     % Initial WM-GM-CSF Probability

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Generation of Masks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
no_sigmas  = 2.576;     % 99 % of values within 2.576 sigma of mean

%--------------------------------------------------------------------------
% Generate 1st Whole Brain Mask from FLAIR
% read and display coregistered WM / GM / CSF mask
%--------------------------------------------------------------------------
% from FLAIR:
filelist.DSC.rFLAIR_WM = spm_select( 'FPList', SubjectDir.DSC.FLAIR, ...
    	'^rc2sub.*\.nii$');
V = spm_vol(filelist.DSC.rFLAIR_WM);
[WM_FLAIR, ~] = spm_read_vols(V);
WM_FLAIR( isnan(WM_FLAIR)) = 0.0;

filelist.DSC.rFLAIR_GM = spm_select( 'FPList', SubjectDir.DSC.FLAIR, ...
        '^rc1sub.*\.nii$');
V = spm_vol(filelist.DSC.rFLAIR_GM);
[GM_FLAIR, ~] = spm_read_vols(V);
GM_FLAIR( isnan(GM_FLAIR)) = 0.0;

filelist.DSC.rFLAIR_CSF = spm_select( 'FPList', SubjectDir.DSC.FLAIR, ...
        '^rc3sub.*\.nii$');
V = spm_vol(filelist.DSC.rFLAIR_CSF);
[CSF_FLAIR, ~]   = spm_read_vols(V);
CSF_FLAIR( isnan(CSF_FLAIR)) = 0.0;

% Generate 1st Whole Brain Mask from smoothed Segmentation results:
wbmFLAIR       = zeros(DSC_pars.nx, DSC_pars.ny, DSC_pars.nslices);
sGM_FLAIR  = smooth3(GM_FLAIR);
sWM_FLAIR  = smooth3(WM_FLAIR);
sCSF_FLAIR = smooth3(CSF_FLAIR);

for i= 1:DSC_pars.nslices
	wbmFLAIR(:,:,i) = fillholes(sCSF_FLAIR(:,:,i)>0.30 | ...
   	sWM_FLAIR(:,:,i)>0.35 | sGM_FLAIR(:,:,i)>0.35);
end
    
%----------------------------------------------------------------------
% Generate 2nd Whole Brain Mask from T1w MPRAGE
% read and display coregistered WM / GM / CSF mask
%----------------------------------------------------------------------
% from T1w:
filelist.DSC.rT1w_WM = spm_select( 'FPList', SubjectDir.DSC.T1w, ...
    	'^rc2sub.*\.nii$');
V = spm_vol(filelist.DSC.rT1w_WM);
[WM_T1w, ~] = spm_read_vols(V);
WM_T1w( isnan(WM_T1w)) = 0.0;

filelist.DSC.rT1w_GM = spm_select( 'FPList', SubjectDir.DSC.T1w, ...
        '^rc1sub.*\.nii$');
V = spm_vol(filelist.DSC.rT1w_GM);
[GM_T1w, ~] = spm_read_vols(V);
GM_T1w( isnan(GM_T1w)) = 0.0;

filelist.DSC.rT1w_CSF = spm_select( 'FPList', SubjectDir.DSC.T1w, ...
        '^rc3sub.*\.nii$');
V = spm_vol(filelist.DSC.rT1w_CSF);
[CSF_T1w, ~]   = spm_read_vols(V);
	CSF_T1w( isnan(CSF_T1w)) = 0.0;

% Generate 2nd Whole Brain Mask from smoothed Segmentation results:
wbmMPR     = zeros(DSC_pars.nx, DSC_pars.ny, DSC_pars.nslices);
sGM_T1w  = smooth3(GM_T1w);
sWM_T1w  = smooth3(WM_T1w);
sCSF_T1w = smooth3(CSF_T1w);

for i= 1:DSC_pars.nslices
	wbmMPR(:,:,i) = fillholes(sCSF_T1w(:,:,i)>0.30 | ...
            sWM_T1w(:,:,i)>0.35 | sGM_T1w(:,:,i)>0.35);
end

%%-------------------------------------------------------------------------
%% Generate Whole Brain Mask (WBM)- wbm = whole brain mask
%%-------------------------------------------------------------------------
%   for each slice in each dataset, where average baseline signal
%   intensity exceeds 20 SD's of background noise" (Boxerman 2006)
%--------------------------------------------------------------------------

% Show MPRAGE:
fg_MPR   = figure('Position',[1 scrsz(4)/3 scrsz(3)/1.1 scrsz(4)/2]);
figtitle = '';
subplot(1,3,1), newdisp_oxy( rMPR, 0, 0, figtitle, 0, max(rMPR(:)));
fignr      = fignr + 1;

% Show Brain Masks:
figure(fg_MPR);
figtitle = {'Brain Mask:';  'MPRAGE - Mask 1 (from FLAIR) - Mask 2 (from MPRAGE)'};
subplot(1,3,2), newdisp_oxy( wbmFLAIR, 0, 0, figtitle); 
subplot(1,3,3), newdisp_oxy( wbmMPR, 0, 0);
% Save mask
outfile=fullfile(SubjectDir.DSC.toplevel,'Processings','Brainmasks.tif');
check_folder(fileparts(outfile));
saveas(gcf,outfile);

% Choose best wbm:
if DSC_pars.presetvalues == 0
	choice_wbm   = questdlg('Select Whole Brain Mask', ...
        'Whole Brain Mask Selection Dialogue',...
        '1 (left)', '2 (right)','3 (conjunction)','3 (conjunction)');
else
	choice_wbm = DSC_pars.choice_wbm;
end

switch choice_wbm
	case '1 (left)'
        fprintf('1\n')
        maskWB = wbmFLAIR;
	case '2 (right)'
        fprintf('2\n')
        maskWB = wbmMPR;
	case '3 (conjunction)'
        maskWB = wbmMPR | wbmFLAIR;
end
clear wbmFLAIR wbmMPR
%%-------------------------------------------------------------------------
%% Show masks to check if all are correct / Prepare for Analysis:
%%-------------------------------------------------------------------------

fg_mask     = figure('Position',[1 5 scrsz(3)/1.5 scrsz(4)/1.1]);
figtitle    = {'WM - GM' ; 'CSF'};
subplot(2,2,1), newdisp_oxy(WM_T1w  > prob(1), 0, 0, figtitle);
subplot(2,2,2), newdisp_oxy(GM_T1w  > prob(2), 0, 0);
subplot(2,2,3), newdisp_oxy(CSF_T1w > prob(3), 0, 0);
fignr = fignr + 1;

if DSC_pars.presetvalues == 0
	choice_thres = ...
        questdlg('Change Threshold (default: 0.75) for GM / WM / CSF?',...
        'Threshold', 'Yes', 'No','No');
else
    choice_thres = DSC_pars.choice_thres;
end

switch choice_thres
	case 'Yes'
        input_1 = 1;
	case 'No'
        input_1 = 2;
end

while input_1 == 1
	prompt      = {'WM','GM', 'CSF'};
	default     = {num2str(prob(1)), num2str(prob(2)), num2str(prob(3))};
	answer      = inputdlg(prompt,'Probability Threshold',1,default);
	prob(1)     = str2double(answer{1});
	prob(2)     = str2double(answer{2});
	prob(3)     = str2double(answer{3});

	figure(fg_mask);
	figtitle = {'WM - GM' ; 'CSF'};
	subplot(2,2,1), newdisp_oxy(WM_T1w  > prob(1), 0, 0, figtitle);
	subplot(2,2,2), newdisp_oxy(GM_T1w  > prob(2), 0, 0);
	subplot(2,2,3), newdisp_oxy(CSF_T1w > prob(3), 0, 0);

	choice_thres = questdlg('Change Threshold for GM / WM / CSF?', ...
        'Threshold', 'Yes', 'No','No');

	switch choice_thres
        case 'Yes'
            input_1 = 1;
        case 'No'
          	input_1 = 2;
	end
end %while input_1 == 1

% prob(...) for threshold search only:
maskWM = WM_T1w > prob(1);
maskGM = GM_T1w > prob(2);
maskCSF= CSF_T1w> prob(3);

figure(fg_mask);
figtitle = {'WM - GM' ; 'CSF'};
subplot(2,2,1), newdisp_oxy(WM_T1w > prob(1), 0, 0, figtitle);
subplot(2,2,2), newdisp_oxy(GM_T1w > prob(2), 0, 0);
subplot(2,2,3), newdisp_oxy(CSF_T1w> prob(3), 0, 0);
% Save mask
outfile=fullfile(SubjectDir.DSC.toplevel,'Processings','Segments.tif');
check_folder(fileparts(outfile));
saveas(gcf,outfile);
    
%%-------------------------------------------------------------------------
%% Define Oedema & Leakage-Areas 
%%-------------------------------------------------------------------------
%--------------------------------------------------------------------------
% Oedema from FLAIR: (Histogram based)
%--------------------------------------------------------------------------
% Histogram FLAIR:
hist_fl    = reshape( rFLAIR, 1, []);
hist_fl    = sort( hist_fl);
hist_fl( hist_fl == 0) = [];
max_fl     = max( hist_fl);

fg_histfl  = figure(fignr);
fignr      = fignr + 1;
range      = 0:7:max_fl;
subplot(2,2,1);
histogram( hist_fl, range);
axis([0 max_fl 0 0.5*10^4]);
title('Histogram FLAIR')
xlabel('Signal (a.u.)');
ylabel('Counts');
hold on;

% FLAIR GM Histogram:
% adapt GM Probability, to obtain healthy GM only
hist_gm     = reshape( rFLAIR( GM_T1w > prob(2)), 1, []);	
hist_gm     = sort( hist_gm);

figure(fg_histfl);
subplot(2,2,2); 
histogram( hist_gm, range);
axis  ( [0 max_fl 0 0.5*10^4]);
title('Histogram FLAIR - GM')
xlabel( 'Signal (a.u.)');
ylabel( 'Counts');
hold on;

%--------------------------------------------------------------------------
% find threshold for selection of oedema
%--------------------------------------------------------------------------

[mu_gm, sigma_gm]	= normfit( hist_gm);
threshold_gm        = mu_gm + 1.288 * sigma_gm;

% results from normal distribution fit:
fprintf(['\n\nGM mu:\t\t' num2str(mu_gm)]);
fprintf(['\nGM sigma:\t' num2str(sigma_gm)]);
fprintf(['\nGM threshold:\t' num2str(threshold_gm)]);

% plot threshold line in histograms:
figure( fg_histfl);
ylim = [0 0.5*10^4];
subplot(2,2,1), line( [threshold_gm threshold_gm], ylim,...
    'linewidth', 1, 'color', 'red');
hold off;

figure( fg_histfl);
ylim = [0 0.5*10^4];
subplot(2,2,2),line( [threshold_gm threshold_gm], ylim,...
    'linewidth', 1, 'color', 'red');
hold off;

FLAIRmask = (rFLAIR > threshold_gm) .* maskWB;

for i= 1:DSC_pars.nslices
	FLAIRmask(:,:,i) = fillholes(FLAIRmask(:,:,i));
end

figure( fg_histfl);
figtitle = 'Mask - FLAIR';
subplot(2,2,3), newdisp_oxy(FLAIRmask, 0, 0, figtitle);
subplot(2,2,4), newdisp_oxy(rFLAIR);
    
% if segmentation did not work properly, select threshold manually:
if DSC_pars.presetvalues == 0
	choice_thres = questdlg('Change Threshold for FLAIR?', ...
        'Threshold', 'Yes', 'No','No');
else
    choice_thres = DSC_pars.choice_thres;
end

while strcmp(choice_thres,'Yes')
    prompt      = {'Threshold FLAIR'};
	default     = {num2str(threshold_gm)};
	answer      = inputdlg(prompt,'Signal Threshold',1,default);
	threshold_gm= str2double(answer);

 	FLAIRmask = (rFLAIR > threshold_gm) .* maskWB;
    for i= 1:DSC_pars.nslices
        FLAIRmask(:,:,i) = fillholes(FLAIRmask(:,:,i));
    end

	figure( fg_histfl);
	subplot(2,2,3), newdisp_oxy(FLAIRmask);
	subplot(2,2,4), newdisp_oxy(rFLAIR);

	choice_thres = questdlg('Change Threshold for FLAIR?', ...
        'Threshold', 'Yes', 'No','No');
end
% Save mask
outfile=fullfile(SubjectDir.DSC.toplevel,'Processings',...
    'Oedema_FLAIR_hist.tif');
check_folder(fileparts(outfile));
saveas(gcf,outfile);

close(fg_histfl);

%--------------------------------------------------------------------------
% Leakage areas from MPRAGE: (Histogram based)
% - detection of leakage areas actually requires T1w MPR + contrast agent
% - for data from healthy subjects & patients with intact blood brain 
%   barrier (no contrast agent leakage), the algorithm also work with 
%   native T1w mages (it then detects mainly vessels that appear bright due
%   to inflow.
%--------------------------------------------------------------------------

% Histogramm MPR:
hist_mpr    = reshape( rMPR, 1, []);
hist_mpr    = sort( hist_mpr);
hist_mpr( hist_mpr == 0) = [];
max_mpr     = max( hist_mpr);

fg_hist     = figure(fignr);
fignr       = fignr + 1;
range       = 0:7:max_mpr;
subplot(2,2,1); 
histogram( hist_mpr, range);
axis([0 max_mpr 0 0.5*10^4]);
title('T1w histogram')
xlabel('Signal (a.u.)');
ylabel('Counts');
hold on;

% Histogramm WM:
% adapt WM Probability, to obtain healthy WM only
hist_wm     = reshape( rMPR( WM_T1w > prob(1)), 1, []); 
hist_wm     = sort( hist_wm);

figure(fg_hist);
subplot(2,2,2); 
histogram( hist_wm, range);
axis  ( [0 max_mpr 0 0.5*10^4]);
title('T1w GM histogram')
xlabel( 'Signal (a.u.)');
ylabel( 'Counts');
hold on;

%--------------------------------------------------------------------------
% find threshold for selection of leakage areas
%--------------------------------------------------------------------------

% fit normal distribution to WM histogram
[mu_wm, sigma_wm]	= normfit( hist_wm);
threshold_wm        = mu_wm + no_sigmas * sigma_wm;

% results from normal distribution fit:
fprintf(['\n\nWM mu:\t\t' num2str(mu_wm)]);
fprintf(['\nWM sigma:\t' num2str(sigma_wm)]);
fprintf(['\nWM threshold:\t' num2str(threshold_wm)]);

% plot threshold line in histograms
figure( fg_hist);
ylim = [0 0.5*10^4];
subplot(2,2,1), line( [threshold_wm threshold_wm], ylim, ...
    'linewidth', 1, 'color', 'red');
hold off;

figure( fg_hist);
ylim = [0 0.5*10^4];
subplot(2,2,2),line( [threshold_wm threshold_wm], ylim, ...
    'linewidth', 1, 'color', 'red');
hold off;

LeakageMask = (rMPR > threshold_wm) .* maskWB;

figure( fg_hist);
figtitle = 'Mask - MPRAGE';
subplot(2,2,3), newdisp_oxy(LeakageMask, 0, 0, figtitle);
subplot(2,2,4), newdisp_oxy(rMPR);

% if segmentation did not work properly, select threshold manually:
if DSC_pars.presetvalues == 0
	choice_thres = questdlg('Change Threshold for MPRAGE?', ...
        'Threshold', 'Yes', 'No','No');
else
	choice_thres = DSC_pars.choice_thres;
end

while strcmp(choice_thres,'Yes')
    prompt      = {'Threshold MPRAGE'};
    default     = {num2str(threshold_wm)};
    answer      = inputdlg(prompt,'Signal Threshold',1,default);
    threshold_wm= str2double(answer);

    LeakageMask = (rMPR > threshold_wm) .* maskWB;

    figure( fg_hist);
    subplot(2,2,3), newdisp_oxy(LeakageMask);
	subplot(2,2,4), newdisp_oxy(rMPR);

	choice_thres = questdlg('Change Threshold for MPRAGE?', ...
        'Threshold', 'Yes', 'No','No');
end
    
% Save mask
outfile=fullfile(SubjectDir.DSC.toplevel,'Processings',...
    'Leakage_MPR_hist.tif');
check_folder(fileparts(outfile));
saveas(gcf,outfile);

close(fg_hist);

%--------------------------------------------------------------------------
% Define Leakage Mask: 'MPR | FLAIR' (conjunction)
%--------------------------------------------------------------------------
 
HealthyMask = (1 - (LeakageMask | FLAIRmask)) .* maskWB;
LesionMask  = LeakageMask | FLAIRmask;

%%--------------------------------------------------------------------------
% show masks for evaluation:
%%--------------------------------------------------------------------------

maskGM = maskGM & ((1-(maskCSF | LeakageMask | FLAIRmask | LesionMask)));
maskWM = maskWM & ((1-(maskCSF | LeakageMask | FLAIRmask | LesionMask)));

fg_mask     = figure('Position',[1 5 scrsz(3)/1.5 scrsz(4)/1.1]);
figure(fg_mask);
figtitle  = {'GM - WM'; 'CSF - Leakage'};
subplot(2,2,1), newdisp_oxy(maskGM, 0, 0, figtitle);
subplot(2,2,2), newdisp_oxy(maskWM, 0, 0);
subplot(2,2,3), newdisp_oxy(maskCSF, 0, 0);
subplot(2,2,4), newdisp_oxy(LesionMask, 0, 0);

% Save figure
outfile=fullfile(SubjectDir.DSC.toplevel,'Processings',...
    '02_brain_segments.tif');
check_folder(fileparts(outfile));
saveas(gcf,outfile);
end

