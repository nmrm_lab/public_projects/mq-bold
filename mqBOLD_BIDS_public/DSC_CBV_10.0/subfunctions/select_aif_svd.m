
function [aif_cluster, dR2s_a] = select_aif_svd(SubjectDir, dscdata, S0, dR2s,...
            start, ende, Index_A, Index_E, Peak_height, maskWB, maskWM, maskGM, ...
            FLAIRmask, LeakageMask, DSC_pars, fignr)

nx = DSC_pars.nx;
ny = DSC_pars.ny;
nslices = DSC_pars.nslices;
nframes = DSC_pars.nframes;
TE = DSC_pars.TE;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  - *YR24May17*
%      select_aif_svd : Arterial Input Function (AIF)
%           Peak_height: 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic % Start stopwatch timer
warning('off', 'all')

disp(' ');
disp('===================================================================');
disp('Start AIF selection...');
disp('===================================================================');
difference  = Peak_height./S0;

clear aif_curve;

for nn = 1:1
    
    change_aif      = 1;
    change_cluster  = 0;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % - automatic:
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % *********************************************************************
    % (1) Mask Data
    % *********************************************************************
   
    % Restrict Data to:
    % - only some slices:
    % - avoid lower brain slices (i.e. basal slices at hight of cerebellum) 
    %   by determining the relative signal coverage
    for ss = 1:nslices/2
        coverage(ss) = sum(sum(maskWB(:,:,ss)))./(nx*ny);
    end
    
    % start slice
    s1 = find((coverage > (2/3 * coverage(int16((nslices/2)-1)))), 1, 'first'); 
    s2 = s1 + 4; % end slice
    
    % - avoid tumourous regions:
    mask_tumor  = FLAIRmask | LeakageMask;
    for k= 1:nslices
        mask_tumor(:,:,k) = fillholes(mask_tumor(:,:,k));
    end
     
    % - according to SS:AUCfirst => to avoid Partial Volume
    SS              = mean( dR2s(:,:,:,start:ende),4);
    AUC             = trapz(dR2s(:,:,:,start:ende),4);
    SS2AUCfirst  	= SS ./ AUC;
    SS2AUCfirst(isnan(SS2AUCfirst)) = 0;
    SS2AUCfirst(isinf(SS2AUCfirst)) = 0;
    % GM as reference (nach Bleeker 2011):
    SS2AUCfirst_ref  = mean(SS2AUCfirst(maskGM(:,:,s1:s2)==1));
    
    % only within +- 20 % around reference
    mask_rPSR = (SS2AUCfirst >= (SS2AUCfirst_ref-SS2AUCfirst_ref./100.*20)) & ...
        (SS2AUCfirst <= (SS2AUCfirst_ref+SS2AUCfirst_ref./100.*20) ) .* maskWB;
    
    % - according to large AUC => to avoid Partial Volume
    AUC = AUC(:,:,:);
    
    % only largest 10 % (literature) of the AUCs:
    p_AUC = quantile(AUC(maskWB(:,:,s1:s2)==1 & ...
        mask_rPSR(:,:,s1:s2)==1 & mask_tumor(:,:,s1:s2)==0), ...
        0.90);
    mask_pAUC = AUC > p_AUC;
    
    % scale to unit area:
    clear scAUC
    for f = 1:nframes
        scAUC(:,:,:,f) = dR2s(:,:,:,f)./AUC;
    end
    
    % Restrict Data:
    % - according to small roughness => to avoid Partial Volume
    % second order differentiation:
    diff2     = diff(scAUC(:,:,:,:),2, 4);
    
    roughness = trapz((diff2(:,:,:,:)).^2, 4);
    % only smallest 25 % (literature) of the roughness:
    p_rough   = quantile(roughness(maskWB(:,:,s1:s2)==1 &...
        mask_pAUC(:,:,s1:s2)==1 & mask_rPSR(:,:,s1:s2)==1 & ...
        mask_tumor(:,:,s1:s2)==0), 0.25);
    
    mask_pRough   = roughness < p_rough;
 
     
     % Abfangen von Problemen: only two cases in 40:
     if sum(sum(sum(maskWB .* (1-mask_tumor) .* mask_pAUC .* mask_pRough...
             .* mask_rPSR))) == 0
         
         mask_rPSR = maskWB;
         
         % only largest 10 % (literature) of the AUCs:
         p_AUC       = quantile(AUC(maskWB(:,:,s1:s2)==1 & ...
             mask_rPSR(:,:,s1:s2)==1 & mask_tumor(:,:,s1:s2)==0), 0.90);
         mask_pAUC   = AUC > p_AUC;
         
         % scale to unit area:
         clear scAUC
         for f = 1:nframes
             scAUC(:,:,:,f) = dR2s(:,:,:,f)./AUC;
         end
         
         % Restrict Data:
         % - according to small roughness => to avoid Partial Volume
         % second order differentiation:
         diff2     = diff(scAUC(:,:,:,:),2, 4);
         
         roughness = trapz((diff2(:,:,:,:)).^2, 4);
         % only smallest 25 % (literature) of the roughness:
         p_rough   = quantile(roughness(maskWB(:,:,s1:s2)==1 & ...
             mask_pAUC(:,:,s1:s2)==1 & mask_rPSR(:,:,s1:s2)==1 & ...
             mask_tumor(:,:,s1:s2)==0), 0.25);
         
         mask_pRough   = roughness < p_rough;
         
     end
     
    % initialize counter:
    count_redo = 0;
    
    while change_aif == 1
        
        change_aif      = 0;
        idx_plot        = zeros(size(maskWB));
        idx_plot_orig   = idx_plot;
        
        % *****************************************************************
        % (2) Find arteries in 5 selected slices
        % *****************************************************************
        [aif_svd,aif_svd_real,aif_cluster,PVc,PWc,idx,TTPc]= ...
            svd_aif(dscdata,dR2s,DSC_pars,Index_E,Index_A,maskWB,mask_tumor,...
                    mask_pAUC,mask_pRough,mask_rPSR,s1,s2,fignr);
                
            % *************************************************************
            % (3) Find best curve
            % *************************************************************
            idxd   = zeros(nframes,7,nslices);
            numb   = zeros(1,7,nslices);
            S0_cl  = zeros(1,7,nslices);
            
            for xx = 1:nx
                for yy = 1:ny
                    for zz = 1:nslices
                        if maskWM(xx,yy,zz) == 1 && maskWB(xx,yy,zz) == 1
                            idxd(:,6,zz)  = idxd(:,6,zz) + ...
                                squeeze(mean(mean(mean(dscdata(xx,yy,zz,:),1),2),3));
                            numb(1,6,zz)  = numb(1,6,zz) + 1;
                            S0_cl(1,6,zz) = S0_cl(1,6,zz) + S0(xx,yy,zz);
                        end
                        
                        if maskGM(xx,yy,zz) == 1 && maskWB(xx,yy,zz) == 1
                            %summarized signal
                            idxd(:,7,zz)     = idxd(:,7,zz) + ...
                                squeeze(mean(mean(mean(dscdata(xx,yy,zz,:),1),2),3)); 
                            % number of �ixels per slice
                            numb(1,7,zz) = numb(1,7,zz) + 1;                      
                            % summarized baseline signal
                            S0_cl(1,7,zz) = S0_cl(1,7,zz) + S0(xx,yy,zz);          
                        end
                    end
                end
            end
            
            % Select best result if multiple cluster iterations:
            % to check later best of all trials...
            idxd_all(:,:,count_redo+1) = aif_svd(:,:);
            idx_plot_all(:,:,count_redo+1) = idx_plot(:,:,s1+(aif_cluster-1));
            S0_all(1,:,count_redo+1) = max(aif_svd(:,:));
            
            % Check if AIF is narrower than GM, deeper than GM and earlier 
            % or same TTP as GM...:
            std_gm  = -1/TE .* log(std(min((idxd(:,7,:))./repmat(S0_cl(1,7,:), ...
                [nframes 1 1]))));
            std_gm(isnan(std_gm)) = 0;
            [min_gm, TTP_gm] = max( -1/TE .* log(mean(idxd(:,7,:),3)...
                                                ./mean(S0_cl(1,7,:),3)) );
            PW_gm = mean(Index_E(maskGM==1) - Index_A(maskGM==1));
            PWstd_gm = std( Index_E(maskGM==1) - Index_A(maskGM==1)) /2;
           
            % new:
            for l = 1:5 
                    test(:,l) = aif_svd_real(:,l)./max(aif_svd_real(:,l),[],1);
                    numbers = test(test(:,l) >= 0.5);
                    breite(l)  = numel(numbers);
            end
            
            [ ~,ind1] = max(breite);
            QM(ind1)  = 0;
            [~, aif_cluster] = max(QM);
            % end new
            
            while ( PVc(aif_cluster) < (min_gm - std_gm) ) ...
                    || ( PWc(aif_cluster) > (PW_gm + PWstd_gm)) ...
                    || (idx < 3) ...
                    || (TTPc(aif_cluster) > TTP_gm)
                                                             
                QM(aif_cluster)  = 0;
                [~, aif_cluster] = max(QM);
                
                disp('... Curve changed!')
                
                if sum(QM(:)) == 0
                
                    QM = ( PVc./mean(PVc) )  ...
                        ./ ( ( (TTPc./ mean(TTPc)) ...
                        .*  (PWc ./ mean(PWc)) ).^2 ...
                        );
                    
                    QM(ind1)  = 0;
                    
                    % new
                    [~, aif_cluster]    = max(QM);
                    % end new
                    
                    break
                 end
            end       
        
        if change_aif == 0
            idx_plot        = zeros(size(maskWB));
            idx_plot(idx_plot_orig == aif_cluster) = 1;
            
            fh      = figure(fignr);% figure('MenuBar','None'); SK22JUL16
            newdisp_oxy((squeeze(dscdata(:,:,:,round(mean(TTPc)))) + ...
                2*max(dscdata(:))*idx_plot).* maskWB,1,0,...
                'DSC + AIF Voxel (= white)');
            
            % Save AIF selection as tif SK11JUL16
            outfile=fullfile(SubjectDir.DSC.toplevel,'Processings',...
                '03_AIF_selection.tif');
            check_folder(fileparts(outfile));
            saveas(gcf,outfile);
           
            uiwait(gcf,20); % wait until figure is closed or 20 sec are gone           
        end
        
        if change_cluster == 1
            idx_plot_orig(idx_plot_orig~=aif_cluster) = 0;
            idx_plot_orig(idx_plot_orig==aif_cluster) = 1;
            idx_plot = idx_plot_orig;
            
            figure, newdisp_oxy((squeeze(dscdata(:,:,:,round(mean(TTPc)))) ...
                + 2*max(dscdata(:))*idx_plot).* maskWB,1,0,...
                'DSC + AIF Voxel (= white)');
         
        end
    end % while change_aif
   
    % Arrival Time Correction for deconvolution:
    mainsliceaif = s1+(aif_cluster-1);
    
    [~, igm]    = min(idxd(:,7,mainsliceaif));
      [~, iaif]   = max(aif_svd(:,aif_cluster));
     
    di0 = igm-iaif;
    disp(' ');
    disp(['... TTP(AIF) versus TTP(GM): ' num2str(di0)])
    
    bat_aif     = mean(Index_A(idx_plot==1));
    
    % Calculate dR2s curve of AIF:
    dR2s_a = aif_svd(:,aif_cluster);
    
    % Save AIF results as tif SK11JUL16
    fh      = figure(fignr);
    plot(dR2s_a);
    title('Resulting AIF');
    xlabel('Dynamic #');
    xlabel('Signal(dR2s)');
    outfile=fullfile(SubjectDir.DSC.toplevel,'Processings',...
        '04_AIF_results.tif');
    check_folder(fileparts(outfile));
    saveas(gcf,outfile);
    fignr = fignr + 1;
           
    uiwait(gcf,20); % wait until figure is closed or 20 sec are gone           
 

    
    %**********************************************************************
    %% Gamma Variate Fit:
    %**********************************************************************
    
    choice_gvf = 'No';
    
    if strcmp(choice_gvf, 'No') == 1
        dR2s_a(dR2s_a < 0.0) = 0.0;
        dR2s_a_fit = dR2s_a;
    end
    

    
    fignr = fignr + 1;
    
    disp('================================================================');
    disp('End AIF selection');
    disp('================================================================');
    
    aif_curve(:,nn) = dR2s_a;
    disp(num2str(nn));
    
    %figure(99);
    %plot(dR2s_a);
    
end % nn
elapsedTime = toc;
end