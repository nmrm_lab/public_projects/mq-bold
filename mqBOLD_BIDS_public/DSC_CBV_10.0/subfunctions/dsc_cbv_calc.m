function [SubjectDir, filelist, flag] = dsc_cbv_calc(SubjectDir, filelist, ...
                                                            DSC_pars, flag)

% is called from main program dsc_cbv
%% Required Subscripts / Scriptsets:

%   - SPM12_modified
%   .........................
%   - using SPM12:
%   .........................
%   - coreg_general.m
%   - segment_SPM12.m
%   - realign_estimate.m
%   - reorient_achieva.m
%   - reorient_ingenia_3Danatomie.m
%
%   .........................
%   .........................
%   - fillholes.m
%   - newdisp_oxy.m
%   - newroi_dsc.m
%   - normalize_CBV.m
%   - restrict_CBV.m
%
%   .........................
%   .........................
%   - batch_deconv....m
%   - fit_lorentzian.m


%% Modifications:                                                                                                                                                 -
%   - AK_27MAY2015                                                                                                                                           -
%       + Allow more/less than 3 pixel for AIF selection                                                                                                             -
%       + Reference Curve: CSF completely subtracted
%   - AK2016
%       + Implement different leakage correction methods
%       + Implement Batch mode
%   - SK11JUL16
%       + Improve coreg stabilization MPR 2 FLAIR 2 DSC
%       + Extended parameter saving
%       + Debug PAR/REC processing
%       + Save images for quality check in 'DSC/Processings'
%       + Add further comments to sourcecode
%   - *SK13JAN17*
%       + No more PB/MB
%       + Clean up code 
%   - *SK25JAN17*
%       + Debug batch mode
%       + dsc_cbv_calc as a function
% ===============================================================================================
%   - *YR12APR17*
%       + Introducing a new file format as Nifty with amount file_format= 2 
%       + adding new function(Nifty_process) which contains the old transfer to nifty and
%       also the new processing for the Nifty files(for furthure info
%       please look at the fubcation info comments)
%       + Clean up code 
%  - *YR24May17*
%       +all the scripts has been changed to functions
% ===============================================================================================

% ===============================================================================================
%   - CP 07JUL2017
%       - overall clean up
%       - reorganisation of leakage correction subfunctions
%       - set multiplication factor Tc in SVD method = 4 for decreased
%         noise sensitivity
%       - automatic processing tested for Dicom, PAR/REC & Nifti
% ===============================================================================================

%% Main Program

% Steps:
% 0 - Image preparation
% 1 - Image coregistration
% 2 - Bolus tracking 
% 3 - Leakage identification & AIF selection
% 4 - CBV (final workspace)
% 5 - Leakage correction 

% Figure information
fignr       = 3;                    % Initialize figure number

% Load workspace
try
    nworkspaces = size(DSC_pars.list_mat,1);
    fprintf('\n %d *.mat files detected!\n', size(DSC_pars.list_mat,1));
    load(DSC_pars.list_mat(end).name);  
catch
    fprintf('\n Start processing from scratch!\n');
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 0 Preprocessing: read nifti data, slice timing & motion correction
%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% Check recent processing progress by saved workspaces
if nworkspaces == 0
    % extract dimension parameters for convenience
    nx = DSC_pars.nx;
    ny = DSC_pars.ny;
    nslices = DSC_pars.nslices;
    nframes = DSC_pars.nframes;   
    
    % ---------------------------------------------------------------------
    % Slice timing correction and realignment of DSC data (optional)
    % ---------------------------------------------------------------------
    switch DSC_pars.choice_stc
        case 'Yes'          
            % Run slice time correction
            timing_reorder(SubjectDir, DSC_pars);
            % slice timing corrected data are saved with prefix 'ao' 
            prefix = 'ao';
        
        case 'No'
            % uncorrected data are saved with prefix 'o' 
            prefix = 'o';
            fprintf('No slice time correction done\n');
    end
    
    % ---------------------------------------------------------------------
    % Motion correction realignment of DSC data (optional)
    % ---------------------------------------------------------------------
	switch DSC_pars.choice_align
        case 'Yes'
            % set path
            PWI_filepath = spm_select( 'FPList', SubjectDir.DSC.Perfusion, ...
                ['^' prefix '.*\.nii$']);
            % Run Realignment: choose 'estwrite', i.e. estimate and write
            realign_estimate(PWI_filepath, 'estwrite'); 
            prefix = 'rao';           
        case 'No'
            fprintf('No realignment done\n\n');
	end  
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Load DSC time course data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Load Volume information 
	% select path of perfusion data, prefix determines selection of data
    % 'o': uncorrected; 
    % 'ao': slice time corrected; 
    % 'rao': slice time and motion corrected
	PWI_filepath = spm_select( 'FPList', SubjectDir.DSC.Perfusion, ...
                                      ['^' prefix '.*\.nii$']);                              
	V = spm_vol(PWI_filepath);
            
    % Create empty array for DSC time course data
    dscdata = zeros(nx, ny, nslices, ...
        nframes);
    
    % Load images
    for f = 1:1:nframes
        [dscdata(:,:,:,f), ~] = spm_read_vols(V(f));
    end
    
    %replace NaN values by zeros and give alarm message
    dscdata(isnan(dscdata))=0;
    
    % Save workspace
    save(fullfile(SubjectDir.DSC.toplevel, '00_workspace'), '-v7.3');
end % if nworkspaces == 0


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 1 Image coregistration
%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check recent processing progress by saved workspaces
if nworkspaces <= 1

    % ---------------------------------------------------------------------
    % Read & Coregister MPRAGE & FLAIR data
    % ---------------------------------------------------------------------
   
    % Target for coregistration of anatomical data: 
    % mean of realigned DSC data
	coreg_ref = spm_select( 'FPList', SubjectDir.DSC.Perfusion, ...
                                          '^mean.*\.nii$');  
	if isempty(coreg_ref)
        % use first image of coregistered time series as reference
        coreg_ref   = PWI_filepath(1,:);
	end

    % Coreg FLAIR to DSC (SK08JUL16: it works better)
	[SubjectDir, filelist] = coreg_FLAIR2DSC(SubjectDir,filelist,coreg_ref);

    % Coreg MPR to rFLAIR which is already coreg to DSC
	[SubjectDir, filelist] = coreg_MPR2FLAIR2DSC(SubjectDir,filelist);              
               
    % Select & read coregistered T1w MPR file - replace NaNs by '0'
    filelist.DSC.rT1w = spm_select( 'FPList' , SubjectDir.DSC.T1w, ...
        '^rsub.*\.nii$');
	V                	= spm_vol( filelist.DSC.rT1w);
	[rMPR, ~]         	= spm_read_vols(V);
	rMPR(isnan(rMPR)) 	= 0.0;

    % Select & read coregistered FLAIR file - replace NaNs by '0'        
	filelist.DSC.rFLAIR = spm_select( 'FPList' , SubjectDir.DSC.FLAIR, ...
        '^rsub.*\.nii$');
    V                 	  = spm_vol( filelist.DSC.rFLAIR);
    [rFLAIR, ~]           = spm_read_vols(V);
    rFLAIR(isnan(rFLAIR)) = 0.0;
    
    % ---------------------------------------------------------------------
    % Save Analyze Header as template for saving of Maps / CBV / ...:
    % ---------------------------------------------------------------------
    V_save       = V(1);     % save image header info as structure
    V_save.dt    = [16 0];   % 16 stands for float data type 

    % Save workspace
    save(fullfile(SubjectDir.DSC.toplevel, '01_workspace'), '-v7.3');    
end % if nworkspaces <= 1

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 2 Bolus tracking
%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check recent processing progress by saved workspaces
if nworkspaces <= 2
    %----------------------------------------------------------------------
    % Generate masks
    %----------------------------------------------------------------------
    [filelist, WM_T1w, GM_T1w, CSF_T1w, maskWB, maskWM, maskGM, maskCSF, ...
        HealthyMask, LesionMask, LeakageMask, FLAIRmask, fignr] = ...
       	GenerateMasks(SubjectDir, filelist, DSC_pars, rMPR, rFLAIR, fignr);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  Calculation - Parameters from Signal-time-curves  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ---------------------------------------------------------------------
    %  Smoothing of data:
    % ---------------------------------------------------------------------
    dscdata_smoothed = zeros(size(dscdata));
    for s = 1:nslices
        dscdata_smoothed (:,:,s,:) = ...
            smooth3 (squeeze(dscdata(:,:,s,:)), 'gaussian', [3,3,3], 2.00);
    end
    dscdata = dscdata_smoothed;
    clear dscdata_smoothed

    % ---------------------------------------------------------------------
    % TOA = Time of Arrival: (Define start and end of bolus peak)
    % ---------------------------------------------------------------------

    TOA = zeros(size(dscdata));

    % calculate central difference quotient:
    for f = 3:(nframes-2)
        TOA(:,:,:,f) = (dscdata(:,:,:,f-2) - dscdata(:,:,:,f+2)) ./ 4 ;
    end

    % smooth TOA - improved result in Simulations:
    TOA_parfor = reshape(TOA,  [(nx*ny*nslices) nframes]);
    TOA2       = zeros([(nx*ny*nslices) nframes]);

    parfor ll = 1:(nx*ny*nslices) 
        TOA2(ll,:) = smoothdata(TOA_parfor(ll,:));
    end

    TOA = reshape(TOA2, ...
        [nx ny nslices nframes]);
    clear TOA2 TOA_parfor;
    
    % initial signal drop  (= start of peak in signal)
    [slope_A, Index_A] = max(TOA, [], 4); 
    % return to baseline (= end of peak in signal)
    [slope_E, Index_E] = min(TOA, [], 4);    

    % Peak width: instead FWHM = Full Width Half Maximum:
    Peak_width                      = Index_E - Index_A;
    Peak_width(Peak_width < 0.0)    = 0.0;

    % CP - 23.03.2020: Translation of original German comments from AK
    % Because of averaging during search for the inital increase, two
    % index positions are missung that need to be replaced
    % -> increase rather delayed, decrease rather too early (???)
    % (might be helpfulto define a global start and end point)
    % might make sense for integration but is not necessary according to
    % simulations [CP: not really sure what this comment means]
    Index_A   = Index_A - round( 0.5 * Peak_width);
    Index_E   = Index_E + round( 1.5 * Peak_width);

    % Restrictions to exclude unpossible values:
    % index < 1 and > nframes is not possible 
    % neither for start (A) nor end (E)
    Index_A(Index_A < 1)       = median(Index_A(maskWB == 1));   
    Index_E(Index_E > nframes) = nframes;                      
    Index_E(Index_E <= 1)      = median(Index_E(maskWB == 1));   
    % start index_A needs to be smaller than end index_E
    Index_A(Index_A >= Index_E)= median(Index_A(maskWB == 1)); 
    % counter check
    Index_E(Index_A >= Index_E)= median(Index_E(maskWB == 1));   

    % recalculate peak width to avoid that Peak_width = 0, which results
    % in corrupted pixels for AIF selection
    Peak_width                   = Index_E - Index_A;
    Peak_width(Peak_width <= 0.0)= median(Peak_width(maskWB == 1));

    % Define Integration range:
    S_mean  = mean (mean (mean (dscdata(:,:,:,:),1),2),3);

    fg_allg = figure(fignr); subplot(2,2,1);
    plot ( squeeze(S_mean)); hold all
    title( {'mean Signal over all Pixel', ...
        'Is Start and End of the Peak ok', 'Enter'});
    set  ( gca, 'XTick', 0:10:nframes); hold all
    fignr = fignr + 1;

    % ---------------------------------------------------------------------
    % Choose & Check Start and End point of signal drop = Integration range:
    % ---------------------------------------------------------------------
    if DSC_pars.presetvalues == 0
        % ALTERNATIVE (for manual graphical input):
        % start and end point canbe selected by clicking on the 
        % selected points in the graph
        [t0, ~]     = getpts;
        t0          = round(t0);
        start       = t0(1);
        ende        = t0(2);
    else
        % Automated search
        start       = median(Index_A(maskWB == 1));
        ende        = median(Index_E(maskWB == 1));
    end

    plot( [start start], [min(S_mean(:)) max(S_mean(:))])
    plot( [ende ende],   [min(S_mean(:)) max(S_mean(:))])

    if DSC_pars.presetvalues == 0
        prompt      = {'Start:', 'Ende:'};
        def         = {num2str(start), num2str(ende)};
        answer      = inputdlg(prompt,'Integral Grenzen',1,def);
        start       = answer{1};
        start       = str2double( start);
        ende        = answer{2};
        ende        = str2double( ende);

        hold off;
    end
    
    % ---------------------------------------------------------------------
    % Determine Peak higth & TTP = Time to Peak:
    % ---------------------------------------------------------------------
    % select minimum --> because of negative Signal Peak
    [Peak_height, Index_TTP] = min(dscdata(:,:,:,:), [], 4); 
    % broader is definitely no Peak
    % >> nframes damit nicht in AIF selection kommt
    Peak_width(Peak_width > 2*(ende-start))  = nframes;          
                                                                
    % Verification (in single voxel):
    figure(fg_allg); subplot(2,2,2); 
    plot( squeeze(dscdata(50,60,12,:))); hold all
    plot( start,  dscdata(50,60,12,start),	'd');               hold all
    plot( ende,   dscdata(50,60,12,ende),	'o');               hold all
    plot( Index_TTP(50,60,12), dscdata(50,60,12,Index_TTP(50,60,12)),'x');
    title 'smoothed data';

    % ---------------------------------------------------------------------
    % Histogramm of DSC-data at time point mean(TTP):
    % ---------------------------------------------------------------------

    hist_dsc = ...
        reshape( dscdata(:,:,:,round(mean(Index_TTP(maskWB==1)))), 1, []);
    hist_dsc = sort( hist_dsc);
    hist_dsc( hist_dsc == 0) = [];
    maxx_dsc = max(hist_dsc);
    maxy_dsc = size(hist_dsc,2);

    figure(fg_allg);
    subplot(2,2,3), histogram( hist_dsc); 
    axis([0 maxx_dsc/1.5 0 maxy_dsc/10]);
    xlabel( 'Signal (a.u.)' );
    ylabel( 'Counts'        );
    title ( 'DSC Histogram' );
    
    % Save start & end as tif %SK11JUL16
    outfile=fullfile(SubjectDir.DSC.toplevel,...
        'Processings','01_Bolustracking.tif');
    check_folder(fileparts(outfile));
    saveas(gcf,outfile);
    
    close all
    % Save workspace
    save(fullfile(SubjectDir.DSC.toplevel, '02_workspace'), '-v7.3'); 
end % if nworkspaces <= 2


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 3 Identification of leakage areas & AIF selection
%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check recent processing progress by saved workspaces
if nworkspaces <= 3
    % ---------------------------------------------------------------------
    % Define Baseline Signal for DSC:
    %
    % S0 = mean across first time points - average over 4. Dimension
    % ---------------------------------------------------------------------

    % leave first point out if possible, might be outlier:
    if start >= 5
        % mean Signal precontrast for each individual pixel
        S0          = mean (dscdata(:,:,:,2:start), 4); 
        % Standard deviation, normalized by n-1
        S0_sd       = std  (dscdata(:,:,:,2:start), 0, 4); 
    else
        % mean Signal precontrast for each individual pixel
        S0          = mean (dscdata(:,:,:,1:start), 4);  
        % Standard deviation, normalized by n-1
        S0_sd       = std  (dscdata(:,:,:,1:start), 0, 4); 
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %      Calculate Relaxivity-Time-Curves 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%---------------------------------------------------------------------
    % Relaxivity-Time-Curve
    % - for every pixel in every slice:
    %
    % dR2s = - 1/TE * log( S(t) / S0 ) in 1/ms
    % S0   = Baseline Signal
    % S(t) = measured Signal at time point t
    % TE   = Echo Time in ms
    %%---------------------------------------------------------------------

    dR2s = zeros(size(dscdata));

    for f = 1:nframes % for all time points
        dR2s(:,:,:,f) = -(1/DSC_pars.TE) * log(dscdata(:,:,:,f) ./ S0);
    end

    % Baseline (=Precontrast) for every Pixel:
    dR2s_pre = dR2s(:,:,:,1:(start+3));

    % negative values per definition not possible:
    dR2s_pre (dR2s_pre < 0.0) = 0.0;

    for f = 1:(start+3)
        dR2s(:,:,:,f) = dR2s_pre(:,:,:,f);
    end

    dR2s(isnan(dR2s)) = 0.0;
    dR2s(isinf(dR2s)) = 0.0;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Arterial Input Function (AIF)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Manual AIF selection %SK22JUL16
    if DSC_pars.presetvalues == 0
        [aifslice, dR2s_a] = select_aif_manual(SubjectDir, dscdata, S0, start,...
            Index_A, Index_TTP,Peak_height,Peak_width,rMPR,maskWB,DSC_pars,...
            fignr);
    
    else % Automated AIF selection
        [aif_cluster,dR2s_a]= select_aif_svd(SubjectDir, dscdata, S0,dR2s,...
            start, ende, Index_A, Index_E, Peak_height,maskWB, maskWM,...
             maskGM, FLAIRmask, LeakageMask, DSC_pars, fignr);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  estimate dR2s_mean(t) by averaging dR2s(t)
    %   - for all pixels within the healthy tissue mask
    %     (including GM, WM without Leakage - 'HealthyMask' 
    %      = leakage,edema areas and CSF already subtracted)
    %     --> Ausgangskurve fuer FIT
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % estimated dR2s_mean(t): 
    % from Pixel without signal enhancement, within whole brain mask:
    
     dR2s_mean   = zeros(1, nframes);

    for f = 1:nframes
        dR2s_no      = squeeze(dR2s(:,:,:,f));
        % Basisfunktion for FIT
        dR2s_mean(f) = mean(dR2s_no((maskWB & HealthyMask & (1-(CSF_T1w>0.1)) ...
                            & (maskWM | maskGM)) == 1)); 
    end

    dR2s_mean(isnan(dR2s_mean)) = 0;
    dR2s_mean(isinf(dR2s_mean)) = 0;

    % Glaetten der Daten in Zeitrichtung:
    dR2s_mean(1,:)       = smoothdata (squeeze(dR2s_mean(:,:)), 'sgolay');
    dR2s_mean(:,1:start) = 0.0;

    %  figure(fg_allg);
    subplot(2,2,3,'replace'); plot(squeeze(dR2s_mean));
    title('mean dR2s without extravasation');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Conversion Relaxation rate changes (dR2s) to Concentrations (C_t)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%---------------------------------------------------------------------
    % Calculate Concentration time curves
    %%---------------------------------------------------------------------
    % Tissue-Concentration    C_t : (linear relation) in mM
    % - Kjolby et al. 2006/2009 - divided by 10 (= 0.0087), since
    % - Kalavagunta 2010 (0.0048) & Semmineh 2014 (0.0053) ly about 
    % a dimension below
    r_t = 0.0053;                   % [1/(ms * mM)]

    C_t            = dR2s ./ r_t;   % [1/ms / (1/ms*mM) = mM]

    % Concentration for Reference Curve:
    C_t_mean       = dR2s_mean ./ r_t;

    % Arterial-Concentration  C_a [1/(ms * mM)] Blockley 2008
    C_a = dR2s_a' ./ r_t;          
    C_a_fit  = C_a;

    close all
    % Save workspace
    save(fullfile(SubjectDir.DSC.toplevel, '03_workspace'), '-v7.3'); 
end % if nworkspaces <= 3

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 4 Calculate descriptive parameter maps (rPH, rPSR, TTP)
%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check recent processing progress by saved workspaces
if nworkspaces <= 4
    %%---------------------------------------------------------------------
    % Set paths and create directories for resulting parameter maps 
    %%---------------------------------------------------------------------   
    % Set save path for simple parameter maps (i.e., TTP, rPH, rPSR,...)
    SubjectDir.DSC.MAPS = fullfile(SubjectDir.DSC.toplevel,'MAPS');
    check_folder(SubjectDir.DSC.MAPS);
    % create directory for paramater maps related to leakage correction
    SubjectDir.DSC.K2 = fullfile(SubjectDir.DSC.toplevel,'K2');
    check_folder(SubjectDir.DSC.K2)
    % create directory for CBV paramater maps 
    SubjectDir.DSC.CBV = fullfile(SubjectDir.DSC.toplevel,'CBV');
    check_folder(SubjectDir.DSC.CBV)
    
    %%---------------------------------------------------------------------
    % Define HEALTHY WM (NAWM) for normalization etc. 
    %%---------------------------------------------------------------------
    % automatic NAWM:
    TH_WM_NAWM = 0.99;
    NAWM = (WM_T1w>TH_WM_NAWM) & ...
        ((1-(maskCSF | LeakageMask | FLAIRmask | LesionMask)));

    % Save NAWM mask
    V_save.fname = fullfile(SubjectDir.DSC.MAPS, 'mask_NAWM.nii');
    spm_write_vol(V_save, NAWM);
    
    %%*********************************************************************
    % rPH / rPSR CALCULATION (peak height)
    %%*********************************************************************

    % ---------------------------------------------------------------------
    % rPH  = relative Peak Height
    % rPSR = relative percentage of signal recovery
    % ---------------------------------------------------------------------

    if ende < (nframes-20)
        % mean Signal postcontrast for each pixel
        S1 = mean (dscdata(:,:,:,end-20:end), 4);  
    else
        % mean Signal postcontrast for each pixel
        S1 = mean (dscdata(:,:,:,ende:end), 4);    
    end

    for j = 2:nframes
        averDSC  = dscdata(:,:,:,j);
        averS(j) = mean(averDSC(NAWM==1));
    end

    clear averDSC;

    if start >= 5
        % mean Signal precontrast for each pixel
        S0_NAWM     = mean (averS(3:start));    
    else
        % mean Signal precontrast for each pixel
        S0_NAWM     = mean (averS(1:start));    
    end

    if ende < (nframes-20)
        % mean Signal postcontrast for each pixel
        S1_NAWM     = mean (averS(end-20:end));  
    else
        % mean Signal postcontrast for each pixel
        S1_NAWM     = mean (averS(ende:end));    
    end

    % min --> da negativer Signal Peak
    [Smin_NAWM, Index_Smin_NAWM]    = min(averS);  

    % Calculate rPH (relative Peak Hight - see Barajas 2009)
    rPH = (S0 - Peak_height) ./ (S0_NAWM - Smin_NAWM);
    rPH = rPH .* maskWB;
    % Calculate rPSR (relative percentage of signal recovery 
    % - see Barajas 2009)
    rPSR = ((S1 - Peak_height) ./ (S0 - Peak_height)) ./ ...
           ((S1_NAWM - Smin_NAWM) ./ (S0_NAWM - Smin_NAWM));
    rPSR = rPSR .* maskWB;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Display and Save:
    % rPH  = relative Peak Hight
    % rPSR = relative percentage of signal recovery
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    fg_hist     = figure(fignr);
    figure(fg_hist); 
    newdisp_oxy(rPH .* maskWB,  1, 1, ...
        'rPH  - relative Peak Height', 0, 5); colormap(jet)
    figure(fg_hist); 
    newdisp_oxy(rPSR .* maskWB,  1, 1, ...
        'rPSR - relative percentage of signal recovery', 0, 5); 
    colormap(jet)
    
    %save maps
    V_save.fname = fullfile(SubjectDir.DSC.MAPS , 'rPH_%.nii');
    spm_write_vol( V_save, rPH);
    V_save.fname = fullfile(SubjectDir.DSC.MAPS, 'rPSR_%.nii');
    spm_write_vol( V_save, rPSR);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % TTP [sec]
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % - Scaled to individual subjects global bolus arrival time for better
    %   comparability:
    time_dyn = DSC_pars.time_dyn;
    TTP = ((Index_TTP-start) * (time_dyn(2)-time_dyn(1))/1000 ) .* maskWB;   
    TTP(TTP<0) = 0;
    TTP(isnan(TTP)) = 0.0;
    TTP(isinf(TTP)) = 0.0;
    path_TTP = fullfile(SubjectDir.DSC.MAPS, 'TTP_sec.nii');
    V_save.fname = path_TTP;
    spm_write_vol( V_save, TTP);
    
    % TTP Statistics
    TTP_mean_maskWB = mean(TTP(maskWB > 0));
    TTP_mean_GM70 = mean(TTP(GM_T1w > 0.7));
    TTP_mean_WM70 = mean(TTP(WM_T1w > 0.7));
    
    % Smooth TTP
    path_sTTP = fullfile(SubjectDir.DSC.MAPS, 'sTTP_sec.nii');
    smooth_TTP(path_TTP, path_sTTP);
                
    % Save image to check coreg: TTP on MPR
    path_tif_ttp=fullfile(SubjectDir.DSC.toplevel,'Processings/TTP');
    range_width = 4;
    manual_range = [TTP_mean_GM70-range_width/2 TTP_mean_GM70+range_width/2];
    Overlay_anatomy(rMPR, TTP, path_tif_ttp, manual_range)
    
    close all
    % Save workspace
    save(fullfile(SubjectDir.DSC.toplevel, '04_workspace'), '-v7.3'); 
    
end % nworkspaces <= 4


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 5 Calculate CBV (uncorrected & corrected) & save maps
%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % - fp      = first pass integration
    % - full    = full integration
    % - auc     = area under curve
    % - svd     = singular value decomposition
    
% Check recent processing progress by saved workspaces
if nworkspaces <= 5
    
    % Correction factors for absolute calculation:
    roh = 0.0104; % roh = 1.04 [g/ml] -> 0.0104 [100g/ml](Bjornerud 2010)
    kH  = 0.733;  % kH  = 0.733 (Rempp) // kH  = 0.71  (Bjornerud 2010)
    
    opts = optimoptions(@lsqcurvefit,	...
        'Display',      'off',                  ...
        'TolFun',       1.0000e-08,             ...
        'TolX' ,        1.0000e-08,             ...
        'MaxFunEvals',  400,...
        'ScaleProblem', 'Jacobian');
    
    if DSC_pars.presetvalues == 0
        Liste           = {...
            '1: Weisskoff/Boxerman 2006; first pass integration',...
            '2: Weisskoff/Boxerman 2006; full integration',...
            '3: Bjornerud et al. 2011; standard SVD (former method 3c)',...
            '4: Bjornerud et al. 2011; Tikhonov SVD (former method 3d)'
            };
        [choice_fit,~]  = listdlg('ListString',Liste);
    else
        choice_fit = DSC_pars.choice_fit;
    end
    
    m_num = numel(choice_fit); 
    
    for i = 1:m_num
        switch choice_fit(i)             
            case 1 % Calculate method 1 AUC (first pass integration)
                   % leakage correction:
                    calc_CBV_AUC_fp(SubjectDir, DSC_pars, dR2s, dR2s_mean, ...
                        dR2s_a, C_t, C_a, rMPR, NAWM, maskWB, opts, V_save,...
                        start, ende, r_t, kH, roh, fignr)
                    tmp = fullfile(SubjectDir.DSC.CBV, ...
                                                'nCBV_corr_auc_fp.nii');
                    % Verify existence of output & save path to filelist
                    if exist(tmp, 'file') == 2
                        flag.DSCfinished = 1;
                        filelist.DSC.CBV_auc_fp = tmp;
                    else
                        flag.DSCfinished = 0;
                        fprintf('\n %s NOT save!\n',tmp);
                    end                 
            case 2 % Calculate method 1 AUC (full range integration)
                   % leakage correction:
                    calc_CBV_AUC_full(SubjectDir, DSC_pars, dR2s, dR2s_mean, ...
                        dR2s_a, C_t, C_a, rMPR, NAWM, maskWB, opts, V_save,...
                        r_t, kH, roh, fignr)
                    tmp = fullfile(SubjectDir.DSC.CBV, ...
                                            'nCBV_corr_auc_full.nii');
                    % Verify existence of output & save path to filelist                    
                    if exist(tmp, 'file') == 2
                        flag.DSCfinished = 1;
                        filelist.DSC.CBV_auc_full = tmp;
                    else
                        flag.DSCfinished = 0;
                        fprintf('\n %s NOT save!\n',tmp);    
                    end                            
            case 3 % Calculate standard SVD (former method 3c) 
                   % leakage correction: 
                     calc_CBV_standardSVD(SubjectDir, DSC_pars, S0, S0_sd,...
                         C_t, C_a_fit, start, ende, NAWM, maskWB, V_save,...
                         kH, roh, fignr)          
                    tmp = fullfile(SubjectDir.DSC.CBV, ...
                                                'nCBV_corr_sSVD.nii');
                    % Verify existence of output & save path to filelist                        
                    if exist(tmp, 'file') == 2
                        flag.DSCfinished = 1;
                        filelist.DSC.CBV_sSVD = tmp;
                    else
                        flag.DSCfinished = 0;
                        fprintf('\n %s NOT save!\n',tmp);    
                    end
            case 4 % Calculate Tikhonov SVD (former method 3d) 
                % leakage correction:
                    calc_CBV_TikhonovSVD(SubjectDir, DSC_pars, S0, S0_sd,...
                         C_t, C_a_fit, start, ende, NAWM, maskWB, V_save,...
                         kH, roh, fignr)
                    tmp = fullfile(SubjectDir.DSC.CBV, ...
                                                    'CBV_corr_TiSVD.nii');
                    % Verify existence of output & save path to filelist                            
                    if exist(tmp, 'file') == 2
                        flag.DSCfinished = 1;
                        filelist.DSC.CBV_TiSVD = tmp;
                    else
                        flag.DSCfinished = 0;
                        fprintf('\n %s NOT save!\n',tmp);    
                    end 
        end % Switch choice_fit(i)
    end % Loop methods
save(fullfile(SubjectDir.DSC.toplevel, '05_workspace'), '-v7.3');
end % if nworkspaces <= 5


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 6 Clean up
%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

try close(fg_allg);
catch
end
try close(fg_mask); 
catch
end
try close(fg_MPR);  
catch
end
    
% --------------------------------------------------------------------------
% Clean up
% --------------------------------------------------------------------------
delete(fullfile(SubjectDir.DSC.toplevel, '00_workspace.mat'));
delete(fullfile(SubjectDir.DSC.toplevel, '01_workspace.mat'));
delete(fullfile(SubjectDir.DSC.toplevel, '02_workspace.mat'));
delete(fullfile(SubjectDir.DSC.toplevel, '03_workspace.mat'));
delete(fullfile(SubjectDir.DSC.toplevel, '04_workspace.mat'));
end
    