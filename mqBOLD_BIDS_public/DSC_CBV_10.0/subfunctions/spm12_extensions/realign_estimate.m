%% Realignment (Estimate & Reslice)
%  - Realignment to mean
function realign_estimate(path, mode)

% File Names as a list:
clear path_mod matlabbatch

for l = 1:length(path(:,1))
    path_mod{l,:} = char([path(l,:) ',1']);
end

if strcmp(mode, 'estimate') == 1
    
    % If only realign estimate without reslice:
    matlabbatch{1}.spm.spatial.realign.estimate.data                = {path_mod}';
    
    % Realign Paramters:
    matlabbatch{1}.spm.spatial.realign.estimate.eoptions.quality    = 0.90;
    matlabbatch{1}.spm.spatial.realign.estimate.eoptions.sep        = 2;
    matlabbatch{1}.spm.spatial.realign.estimate.eoptions.fwhm       = 5;
    matlabbatch{1}.spm.spatial.realign.estimate.eoptions.rtm        = 1;
    matlabbatch{1}.spm.spatial.realign.estimate.eoptions.interp     = 7;
    
elseif  strcmp(mode, 'estwrite') == 1
    
    matlabbatch{1}.spm.spatial.realign.estwrite.data                = {path_mod}';
    % Realign Paramters:
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.quality    = 0.90;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.sep        = 2;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.fwhm       = 9;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.rtm        = 1;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.interp     = 5;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.weight     = '';
    
    % Reslice Parameters:
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.which      = [2 1];
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.interp     = 5;
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.wrap       = [0 0 0];
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.mask       = 0;
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.prefix     = 'r';
    
end

% Run matlabbatch:
spm_jobman('run', matlabbatch);

end