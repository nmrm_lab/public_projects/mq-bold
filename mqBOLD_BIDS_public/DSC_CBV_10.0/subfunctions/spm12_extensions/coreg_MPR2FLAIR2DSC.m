function [SubjectDir, filelist] = coreg_MPR2FLAIR2DSC(SubjectDir,filelist)
%--------------------------------------------------------------------------
% coregister MPR to FLAIR to Perfusion (SPM)
%--------------------------------------------------------------------------
global defaults;
spm_get_defaults;
flags = defaults.coreg;

% Flags to pass to routine to create resliced images
% (spm_reslice)
resFlags = struct(...
    'interp', flags.write.interp,...       % 1: trilinear interpolation
    'wrap', flags.write.wrap,...           % wrapping info (ignore...)
    'mask', flags.write.mask,...           % masking (see spm_reslice)
    'which',1,...                          % write reslice time series 
    'mean',0);                             % do not write mean image

 fprintf('\n\nNow coregistering MPR to FLAIR (which is coregistered to First DSC Image)...\n\n');

% PG - Tar(G)et image, NEVER CHANGED
% PF - Source image, transformed to match PG
% PO - (O)ther images, originally realigned to PF and transformed again to PF

% TARGET - coregistered FLAIR (in space with PWI time series)
PG = spm_select('FPList', SubjectDir.DSC.FLAIR, '^rsub*.*\.nii$');
VG = spm_vol(PG);

% SOURCE - MPR
filelist.DSC.T1w_nii = spm_select( 'FPList', SubjectDir.DSC.T1w, ...
        '^sub.*\.nii$'); 
PF = filelist.DSC.T1w_nii;
VF = spm_vol(PF);

% select "other images" for coregistration GM, WM, CSF segments & masks
PO = spm_select( 'FPList', SubjectDir.DSC.T1w, '^c\w*.*\.nii$');

if isempty(PO) 
    PO = PF;
else
    PO = char( PF,PO);
end

% do coregistration

% this method from spm_coreg_ui.m
% get coregistration parameters
x  = spm_coreg( VG, VF, flags.estimate);

% get the transformation to be applied with parameters 'x'
M  = inv( spm_matrix(x));

% in MM we put the transformations for the 'other' images
MM = zeros( 4, 4, size( PO,1));

for j=1:size( PO,1)
    % get the transformation matrix for every image
    MM(:,:,j) = spm_get_space( deblank( PO(j,:)));
end

for j=1:size( PO,1)
    % write the transformation applied to every image
    % filename: deblank(PO(j,:))
    spm_get_space( deblank( PO(j,:)), M*MM(:,:,j));
end

P = char( PG, PO);
spm_reslice( P,resFlags);
end
