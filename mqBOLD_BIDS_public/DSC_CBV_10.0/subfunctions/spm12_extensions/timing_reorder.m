function timing_reorder(SubjectDir, DSC_pars)

%% Slice Timing Correction
%  - ordering only valid for EPI Acquisition at PET-MRI
% Get paths of uncorrected Perfusion images

path = cellstr(spm_select( 'FPList', SubjectDir.DSC.Perfusion, '^o.*\.nii$'));

spm_jobman('initcfg')
matlabbatch{1}.spm.temporal.st.scans = {path}';

% Get default slice ordering depending on scanner
if strcmp(DSC_pars.Manufacturer, 'Siemens') % assumed scan order for data
                    % from Siemens scanner is interleaved slice order,
                    % i.e. [2 4 6 ... 1 3 5 ... ]
    slice_order = zeros(1, DSC_pars.nslices);
    % Running index
    i = 1;
    % Get even numbers first
    for j = 2:2:DSC_pars.nslices
       slice_order(1,i) = j;
       i = i + 1;
    end
    clear j;
    % Then get odd numbers
    for j = 1:2:DSC_pars.nslices
       slice_order(1,i) = j;
       i = i + 1;
    end
elseif strcmp(DSC_pars.Manufacturer, 'Philips') % assumed scan order for data
                    % fromPhilips scanner with default slice order
                    % [1 3 5 ... 2 4 6 ...]; % ordering scheme
    slice_order = zeros(1, DSC_pars.nslices);
    % Running index
    i = 1;
    % Get odd numbers first
    for j = 1:2:DSC_pars.nslices
       slice_order(1,i) = j;
       i = i + 1;
    end
    clear j;
    % Then get even numbers
    for j = 2:2:DSC_pars.nslices
       slice_order(1,i) = j;
       i = i + 1;
    end
end

% Paramters:
matlabbatch{1}.spm.temporal.st.nslices  = DSC_pars.nslices;
matlabbatch{1}.spm.temporal.st.tr       = DSC_pars.TR; % in sec
matlabbatch{1}.spm.temporal.st.ta       = DSC_pars.TR - DSC_pars.TR/DSC_pars.nslices; % in sec
matlabbatch{1}.spm.temporal.st.so       = slice_order; 
matlabbatch{1}.spm.temporal.st.refslice = 1; % reference slice
matlabbatch{1}.spm.temporal.st.prefix   = 'a'; % no prefix - overwrites original images

job = spm_jobman('serial', matlabbatch);

end