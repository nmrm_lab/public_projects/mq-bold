function [SubjectDir, filelist] = coreg_FLAIR2DSC(SubjectDir,filelist,coreg_ref)
%--------------------------------------------------------------------------
% coregister MPR to T1 map (SPM)
%--------------------------------------------------------------------------
global defaults;
spm_get_defaults;
flags = defaults.coreg;

% Flags to pass to routine to create resliced images
% (spm_reslice)
resFlags = struct(...
    'interp', flags.write.interp,...       % 1: trilinear interpolation
    'wrap', flags.write.wrap,...           % wrapping info (ignore...)
    'mask', flags.write.mask,...           % masking (see spm_reslice)
    'which',1,...                          % write reslice time series 
    'mean',0);                             % do not write mean image

fprintf('\n\nNow coregistering FLAIR to mean or 1st DSC Image...\n\n');

% PG - Tar(G)et image, NEVER CHANGED
% PF - Source image, transformed to match PG
% PO - (O)ther images, originally realigned to PF and transformed again to PF

% TARGET - mean PWI after motion correction or first DSC Image
PG = coreg_ref; 
VG = spm_vol(PG);

% SOURCE - FLAIR
filelist.DSC.FLAIR_nii = spm_select( 'FPList', SubjectDir.DSC.FLAIR, ...
        '^sub.*\.nii$'); 
PF = filelist.DSC.FLAIR_nii;
VF = spm_vol(PF);

% select "other images" for coregistration GM, WM, CSF segments & masks
PO = spm_select( 'FPList', SubjectDir.DSC.FLAIR, '^c\w*.*\.nii$');

if isempty(PO) 
    PO = PF;
else
    PO = char( PF,PO);
end

% do coregistration

% this method from spm_coreg_ui.m
% get coregistration parameters
x  = spm_coreg( VG, VF, flags.estimate);

% get the transformation to be applied with parameters 'x'
M  = inv( spm_matrix(x));

% in MM we put the transformations for the 'other' images
MM = zeros( 4, 4, size( PO,1));

for j=1:size( PO,1)
    % get the transformation matrix for every image
    MM(:,:,j) = spm_get_space( deblank( PO(j,:)));
end

for j=1:size( PO,1)
    % write the transformation applied to every image
    % filename: deblank(PO(j,:))
    spm_get_space( deblank( PO(j,:)), M*MM(:,:,j));
end

P = char( PG, PO);
spm_reslice( P,resFlags);
end