%% Restrict CBV
%   - Restriction to reasonable values

function [CBVout] = restrict_CBV(CBVin)

CBVin(isnan(CBVin)) = 0.0;
CBVin(isinf(CBVin)) = 0.0;
CBVin(CBVin < 0.0)  = 0.0;
CBVin(CBVin > 100.0)= 100.0;

CBVout              = CBVin;

end
