%% Gleichung nach Boxerman/Weiskoff 2006 %%

function  dR2s = model_Boxerman(coeff,time)

% Zu berechnende Koefizienten:
K1      = coeff(1);  % Proportional CBV
K2      = coeff(2);  % Proportional Permeabilit�t

% Reference curve:
%global    dR2s_mean; % to load it from main programm

dR2s_mean = load('savedR2s_mean.mat');

Y       = dR2s_mean.dR2s_mean(:)';

% Delete if other method works stable:
%{
% Formel f�r 1. Zeitpunkt, da Integral = 0, nur 1. Term der Gleichung:
dR2s(1) = K1 .* dR2s_mean(1);

% Formel der folgenden Zeitpunkte:
for i = 2:length(t)
    % Y(t) = k1 * X1(t) - k2 * integral von X2(t) nach dt;
    dR2s(i) = (K1 .* dR2s_mean(i)) - (K2 .* trapz( t(1:i), Y(1:i)));
    % K1 quasi zur H�henscalierung, K2 verhalten des Kurvenendes
    
    if (isnan( dR2s(i)) || ~isfinite( dR2s(i)) || ~isreal( dR2s(i)))
        dR2s(i) = 0;
    end;
    
end;
%}

% cumulatives integral over time points:
dR2s = (K1 .* Y) - (K2 .* cumtrapz( time, Y));

if (isnan( mean(dR2s)) || ~isfinite(mean(dR2s)) || ~isreal( mean(dR2s)))
    dR2s(isnan(dR2s)) = 0;
    dR2s(isinf(dR2s)) = 0;
end

end