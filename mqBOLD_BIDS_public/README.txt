# #########################################################################
# CP 03JUL2020 mqBOLD_BIDS_public
# #########################################################################
#
# IMPORTANT NOTE:
# The Code requires SPM12 (https://www.fil.ion.ucl.ac.uk/spm/software/spm12/)
# mainly for spatial coregistration and segmentation and is not provided in
# this folder.
#
# The folder 'mqBOLD_BIDS_public' contains Matlab Code compatible with
# R2019b that allows processing of DSC-MRI and multiparametric quantitative 
# BOLD data. It contains the following subfolders: 
# -------------------------------------------------------------------------
#   
# 	- CommonUtilities: Contains functions that are commonly used 
#
# -------------------------------------------------------------------------
#   
#	- DSC_CBV_10.0: contains code to perform CBV evaluations 
#           - dsc_settings.m: Allows adjustment of default setting. 
#                             Should work with default dettings-
#           - sortData_dsc.m: Prepares data for processing. 
#                             Needs to be adapted to input.
#           - run2_dsc_cbv.m: Main function
#   NOTE: Detailed information on the implementation of DSC processing 
#       can befound in:
#	Kluge A, Lukas M, Toth V, Pyka T, Zimmer C, Preibisch C. Analysis 
#       of three leakage-correction methods for DSC-based measurement 
#       of relative cerebral blood volume with respect to heterogeneity 
#       in human gliomas. Magn Reson Imaging. 2016 May;34(4):410-21. 
#       doi: 10.1016/j.mri.2015.12.015. Epub 2015 Dec 17.
#   Hedderich D, Kluge A, Pyka T, Zimmer C, Kirschke JS, Wiestler B, 
#       Preibisch C. Consistency of normalized cerebral blood volume values 
#       in glioblastoma using different leakage correction algorithms on 
#       dynamic susceptibility contrast magnetic resonance imaging data 
#       without and with preload. J Neuroradiol. 2019 Feb;46(1):44-51. 
#       doi: 10.1016/j.neurad.2018.04.006. Epub 2018 May 21.
#
# -------------------------------------------------------------------------
#  
#	- mqBOLD_rOEF_10.0: contains code to perform OEF calculations  
#           - T2s_settings.m: Allows IMPORTANT SETTINGS, mainly for T2*
#                             motion and magnetic background field 
#                             correction.
#                             CAVE: Requires sequence specific settings.
#                                   Current settings will not work.
#                                   Consult papers below for guidance.
#           - sortData_T2_T2s.m: Prepares data for processing. 
#                                Needs to be adapted to input.
#           - run3_mqBOLD_rOEF.m: Main function
#
# ------------------------------------------------------------------------- 
#   NOTE: Detailed information on the implementation of T2, T2* and rOEF
#         processing can befound in:
#   Hirsch NM, Toth V, F�rschler A, Kooijman H, Zimmer C, Preibisch C. 
#       Technical considerations on the validity of blood oxygenation 
#       level-dependent-based MR assessment of vascular deoxygenation.
#       NMR Biomed. 2014 Jul;27(7):853-62. doi: 10.1002/nbm.3131. 
#   Kaczmarz S, Hyder F, Preibisch C. Oxygen extraction fraction mapping 
#       with multi-parametric quantitative BOLD MRI: reduced transverse 
#       relaxation bias using 3D-GraSE imaging. Neuroimage. 2020 
#       Jun 26:117095. doi: 10.1016/j.neuroimage.2020.117095.
#       Online ahead of print. PMID: 32599265 
# ---------------------------------------------------------------------                               
# T2* MOTION CORRECTION:
# ---------------------------------------------------------------------
# - T2* maps will be motion corrected if full and half resolution
#   multi-GE Data are available
# - Improved Correction if quarter resolutlion data are available:
#   Noeth U, Volz S, Hattingen E, Deichmann R.An improved method for 
#       retrospective motion correction in quantitative T2* mapping. 
#       Neuroimage. 2014 May 15;92:106-19. 
#       doi: 10.1016/j.neuroimage.2014.01.050. Epub 2014 Feb 4.)
#   --> ralf_t2s_toolbox_24jan18
# -------------------------------------------------------------------------
# T2*MAGNETIC BACKGROUND FIELD CORRECTION
# -------------------------------------------------------------------------    
#  Correction of T2* maps for magnetic background gradients needs  
#   - Information on
#      + RF excitation pulse shape (SG, SINC, EXPO (recommended)) 
#      + RF pulse duration
#      + slice selection gradient strength
#      CAVE: 
#          - sequence adaptation and parameter retrieval needs 
#            expert knowledge
#          - wrong parameter setting cause weird results
#  Correction is performed according to:   
#  Baudrexel S, Volz S, Preibisch C, Klein JC, Steinmetz H, Hilker R, 
#       Deichmann R.Rapid single-scan T2*-mapping using exponential 
#       excitation pulses and image-based correction for linear 
#       background gradients. Magn Reson Med. 2009 Jul;62(1):263-8. 
#  Hirsch NM, Preibisch C.T2* mapping with background gradient 
#       correction using different excitation pulse shapes. AJNR 
#       Am J Neuroradiol. 2013 Jun-Jul;34(6):E65-8. 
#       doi: 10.3174/ajnr.A3021. Epub 2012 Jul 26.      
###########################################################################
