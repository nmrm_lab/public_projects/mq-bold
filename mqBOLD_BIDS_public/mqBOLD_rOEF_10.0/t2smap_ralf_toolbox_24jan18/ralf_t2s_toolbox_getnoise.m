function noise=ralf_t2s_toolbox_getnoise(input_size)

% noise=ralf_t2s_toolbox_getnoise(input_size)
% calculation of complex noise image with matrix given by nsize
% examples for use: ralf_t2s_toolbox_getnoise([100,100]) or ralf_t2s_toolbox_getnoise(size(image))
% noise has standard deviation of 1, both in real and in imaginary part

% Initialize random number generator
%randn('state',sum(100*clock));
randn('state',1e5);

%get real and imaginary parts
xwert=randn(input_size);
ywert=randn(input_size);

%Calculate complex noise
noise=xwert+sqrt(-1)*ywert;
