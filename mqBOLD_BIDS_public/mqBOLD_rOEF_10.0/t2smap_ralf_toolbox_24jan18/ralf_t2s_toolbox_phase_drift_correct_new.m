function newcdata2=ralf_t2s_toolbox_phase_drift_correct_new(cdata1,cdata2,te1,dte,maske)

% newcdata2=ralf_t2s_toolbox_phase_drift_correct_new(cdata1,cdata2,te1,dte,maske)
% cdata1 and cdata2 are both complex 3D image data sets
% The first 2 dimensions are spatial
% The 3rd dimension refers to measurements at different TE
% The function corrects for phase differences due to a frequency shift between measurements of both data sets
% cdata1 is used as reference
% The function returns newcdata2, i.e. cdata2 corrected for frequency shift induced phase differences
% te1 and dte are the first TE value and the TE increment, both in msec
% maske is a binary mask (2D) matching the spatial part of cdata1 and cdata2, indicating where signal is to be found


% Get number of data sets acquired at different TE:
nte=size(cdata1,3);

% List of TE values in msec
te_list=te1+dte*(0:nte-1);

% Quotient if data sets:
quot=cdata2./(eps+cdata1);
% In quot, the phase should increase linearly with TE
% Due to the constant TE increment, the quotient of subsequent quot entries should yield the same phases: 
quotinc=quot(:,:,2:end)./(eps+quot(:,:,1:end-1));
% Thus, the 7 entries of quotinc only have to be averaged 
% The phase of this average correponds to the phase increase per dte
% Mask quotinc:
quotinc=quotinc.*repmat(maske,[1 1 nte-1]);
% For weighted averaging: weight the i-th entry in quotinc with the entry i+1 in abs(cdata1)
% Scale so sum of weighting factors is 1
% This can be done in one line:
for i=1:nte-1
 w(:,:,i)=abs(cdata1(:,:,i+1)).*maske;
end
skal=sum(w,3);
for i=1:nte-1
 w(:,:,i)=w(:,:,i)./(eps+skal);
end

% Weighted average:
avquotinc=sum(quotinc.*w,3);

% Calculate map showing the phase increase per msec:
slopemap=angle(avquotinc)/dte;

%Remove outliers:
mm=mean(abs(slopemap(:)));
ss=std(abs(slopemap(:)));
badI=find(abs(slopemap)>mm+5*ss);
slopemap(badI)=0;


% Smooth this map:
width=5;
zaehler=ralf_t2s_toolbox_gaussian_smoothing_2D(slopemap,width,width);
nenner=ralf_t2s_toolbox_gaussian_smoothing_2D(maske,width,width);
sslopemap=zaehler./(eps+nenner).*maske;
sslopemap(find(isnan(sslopemap)))=0;
%fprintf(1,'Filter Width: %d\n',width);

% Correct cdata2:
for i=1:nte
 corrfeld(:,:,i)=exp(-sqrt(-1)*sslopemap*te_list(i));
end
 
newcdata2=cdata2.*corrfeld;

