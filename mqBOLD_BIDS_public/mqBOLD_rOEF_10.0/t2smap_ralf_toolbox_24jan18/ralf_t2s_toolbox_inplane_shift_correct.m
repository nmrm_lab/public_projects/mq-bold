function [newbild2,corrfeld]=ralf_t2s_toolbox_inplane_shift_correct(bild1,bild2)

% [newbild2,corrfeld]=ralf_t2s_toolbox_inplane_shift_correct(bild1,bild2)
% This function compares 2 images, bild1 and bild2
% bild1 and bild2 are both 2D modulus images with the same matrix size
% bild1 is the reference image
% bild2 has to be shifted in-plane so it matches the reference
% The function exports newbild2 and corrfeld
% newbild2 is a 2D modulus image and corresponds to the shifted version of bild2
% corrfeld is a complex field allowing to shift other images in the same way
% Manual for shifting an image bild3 using corrfield:
%      raw=ifftshift(ifft2(fftshift(bild3)));
%      raw=raw.*corrfeld;
%      newbild3=abs(ifftshift(fft2(fftshift(raw))));


% get matrix sizes np1 and np2 in both directions
% get center points cp1 and cp2 in k-space in both directions
np1=size(bild1,1);
cp1=floor(np1/2+1);
np2=size(bild1,2);
cp2=floor(np2/2+1);

% convert both images into k-space
raw1=ifftshift(ifft2(fftshift(bild1)));
raw2=ifftshift(ifft2(fftshift(bild2)));

% get quotient of k-space data
% a linear increase of the phase of quot indicates an image shift in the respective direction
quot=raw2./(eps+raw1);
aquot=exp(sqrt(-1)*angle(quot));
% Basically, division of raw2 by aquot and FFT of the result would yield the corrected version of bild2
% However, aquot contains useful data only in the center, outside is noise

% Apply filter: circular gauss, FWHM corresponds to radius of 10 pixels:
rad=10;
konst=log(2)/rad^2;
[xx,yy]=ndgrid(-cp1+1:-cp1+np1,-cp2+1:-cp2+np2);
filter=exp(-konst*(xx.^2+yy.^2));
% multiply aquot by this filter:
aquotf=aquot.*filter;
% Get FT of aquotf
% This should result in a PSF, shifted in the same way as bild2, convolved with a gaussian:
psf=abs(ifftshift(fft2(fftshift(aquotf))));
% We are only interested in the shift, so square psf twice to make it sharper:
psfsq=psf.^4;
quot2=ifftshift(ifft2(fftshift(psfsq)));
% quot2 has now decent phase information almost across whole k-space

% Average central 7 columns of quot2
linie=mean(quot2(:,cp2-3:cp2+3),2);
% fit linear increase of angle across central 21 values
xxx=(cp1-10:cp1+10)';
yyy=unwrap(angle(linie(cp1-10:cp1+10)));
P=polyfit(xxx,yyy,1);
% Get from this linearly increasing phase across all pixels
phaslist1=((1:np1)-cp1)'*P(1);
% Calculate correction field for this direction
feld1=repmat(exp(-sqrt(-1)*phaslist1),[1 np2]);

% do the same thing for rows
linie=mean(quot2(cp1-3:cp1+3,:),1);
xxx=(cp2-10:cp2+10);
yyy=unwrap(angle(linie(cp2-10:cp2+10)));
P=polyfit(xxx,yyy,1);
phaslist2=((1:np2)-cp2)*P(1);
feld2=repmat(exp(-sqrt(-1)*phaslist2),[np1 1]);

% Calculate total correction field
corrfeld=feld1.*feld2;

% Get corrected version of bild2 according to manual in header
raw=raw2.*corrfeld;
newbild2=abs(ifftshift(fft2(fftshift(raw))));                                        
