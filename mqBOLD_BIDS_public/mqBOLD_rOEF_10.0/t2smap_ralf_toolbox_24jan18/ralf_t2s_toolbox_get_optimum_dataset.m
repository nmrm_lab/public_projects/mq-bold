function cdatopt=ralf_t2s_toolbox_get_optimum_dataset(cdat1,cdat2,dpe,te1,dte,maske)          

% cdatopt=ralf_t2s_toolbox_get_optimum_dataset(cdat1,cdat2,dpe,te1,dte,maske);
% function chooses which of 2 input data sets has best quality
% cdat1, and cdat2 are complex 3D image data sets with identical matrix size
% The first 2 dimensions are spatial
% The 3rd dimension refers to measurements at different TE
% dpe is the direction of PE (1 or 2 for first or second dimension)
% te1 and dte are the first TE and the TE increment in msec
% maske is a spatial 2D binary mask indicating where non-zero data are found in cdat1 and cdat2
% maske has the same dimensions as cdat1(:,:,1) and cdat2(:,:,1)
% The function returns the best data set which is identical to either cdat1 or cdat2

% Get matrix size in PE direction:
n0=size(cdat1,dpe);

% Get number of TE values:
nte=size(cdat1,3);

if dpe==2
 cdat1=permute(cdat1,[2 1 3]);
 cdat2=permute(cdat2,[2 1 3]);
 maske=permute(maske,[2 1 3]);
end

% Create target data set that will yield for each pixel highest correlation upon T2* fitting:
cdattarg=ralf_t2s_toolbox_create_target_2datasets(cdat1,cdat2,te1,dte).*repmat(maske,[1 1 nte]);
% Fit cdattarg as linear combination of cdat1 and cdat2
[w1,w2,cdatfit]=ralf_t2s_toolbox_fit_2datasets_to_target_fast(cdat1,cdat2,n0,cdattarg);
% w1 and w2 are the weighting factors
% Decide for which data set higher weightings are achieved:
if sum(w1)>sum(w2)
 cdatopt=cdat1;
else
 cdatopt=cdat2;
end

if dpe==2
 cdatopt=permute(cdatopt,[2 1 3]);
end



