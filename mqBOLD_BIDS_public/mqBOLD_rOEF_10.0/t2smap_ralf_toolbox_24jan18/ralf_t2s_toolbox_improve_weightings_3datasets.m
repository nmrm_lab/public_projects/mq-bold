function [cdatcomb,w1n,w2n,w3n]= ralf_t2s_toolbox_improve_weightings_3datasets(cdat1,cdat2,cdat3,dpe,w1,w2,w3)


% [cdatcomb,w1n,w2n,w3n]= ralf_t2s_toolbox_improve_weightings_3datasets(cdat1,cdat2,cdat3,dpe,w1,w2,w3)
% function for improving pre-calculated weighting factors
% cdat1, cdat2, cdat3 are complex image data sets to be combined with different weighting for each k-space line
% dpe is the direction of phase encoding (1 or 2)
% w1, w2, w3 are the input weightings
% The function returns the improved weighting factors w1n, w2n, w3n
% The function returns also the data set cdatcomb based on the use of the improved weigting factors


% The following code is written under the assumption that PE is in the first direction
% if this is not the case: swap dimensions (have to be swapped back afterwards)
if dpe==2
 cdat1=permute(cdat1,[2 1 3]);
 cdat2=permute(cdat2,[2 1 3]);
 cdat3=permute(cdat3,[2 1 3]);
end

% Get matrix size in PE direction:
n0=size(cdat1,1);

% Get raw data
raw1=ifftshift(ifft2(fftshift(cdat1)));
raw2=ifftshift(ifft2(fftshift(cdat2)));
raw3=ifftshift(ifft2(fftshift(cdat3)));

nenner=w1.^2+w2.^2+w3.^2;
w1n=w1.^2./nenner;
w2n=w2.^2./nenner;
w3n=w3.^2./nenner;


% Now construct raw data with optimum weighting factors....
for i=1:n0
 rawcomb(i,:,:)=w1n(i)*raw1(i,:,:)+w2n(i)*raw2(i,:,:)+w3n(i)*raw3(i,:,:);
end
% ....and transform it back into image space
cdatcomb=ifftshift(fft2(fftshift(rawcomb)));


% Swapping back:
if dpe==2
 cdatcomb=permute(cdatcomb,[2 1 3]);
end

