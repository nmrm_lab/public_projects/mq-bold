function newcdata=ralf_t2s_toolbox_gaussian_smoothing_2D(cdata,w1,w2)

% newcdata=ralf_t2s_toolbox_gaussian_smoothing_2D(cdata,w1,w2)
% cdata is a complex or real 2D image data set
% cdata is smoothed by convolution with a gaussian kernel
% w1 and w2 are the FWHM (in pixels) of the kernel (in image space)
% w1 and w2 are NOT restricted to integer values
% for w1,w2=0, no smoothing is performed
% newcdata is the smoothed version of cdata
% if cdata is real, only the real part of newcdata will be exported

% get matrix sizes np1 and np2 in both spatial directions
% get center points cp1 and cp2 in k-space in both directions
np1=size(cdata,1);
cp1=floor(np1/2+1);
np2=size(cdata,2);
cp2=floor(np2/2+1);

% convert data set into k-space
raworig=ifftshift(ifft2(fftshift(cdata)));

% Apply filter
k1=pi^2/4/log(2)*w1^2/np1^2;
k2=pi^2/4/log(2)*w2^2/np2^2;
[xx,yy]=ndgrid(-cp1+1:-cp1+np1,-cp2+1:-cp2+np2);
filter=exp(-k1*xx.^2-k2*yy.^2);
raw=raworig.*filter;

% Calculate smoothed data set:
newcdata=ifftshift(fft2(fftshift(raw)));
if isempty(find(imag(cdata)~=0))
 newcdata=real(newcdata);
end



