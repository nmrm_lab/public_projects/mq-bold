function t2smap_corrected=ralf_t2s_toolbox_t2smap_with_corr(compdat,maske,te1,dte,dx,dy,dz,gap,rfpulse)

% t2smap_corrected=ralf_t2s_toolbox_t2smap_with_corr(compdat,maske,te1,dte,dx,dy,dz,gap,rfpulse)
% T2* mapping with correction for B0 field inhomogeneities in all directions for equidistant TE increase
% compdat is a 4D complex data set
% The first 2 dimensions are read and phase (any order), the 3rd dimension is slice, the 4th is TE-weighting
% maske is a binary mask with the same dimensions as compdat(:,:,:,1), i.e. same spatial coverage
% T2* will be evaluated only where maske is non-zero
% te1 and dte are the first TE value and the TE increment, both in msec (!)
% dx, dy are the spatial resolution in the first/second dimension, in mm
% dz is the slice thickness, in mm
% gap is the gap between slices, in mm
% rfpulse is a string denoting the excitation pulse shape (referring to sequence ralf_t2smap_fc_dum)
% rfpulse can take the following 3 values: 'SINC', 'EXPO', 'GAUSS'




%number of different TE:
nte=size(compdat,4);
% list of TE values, in msec:
te=te1:dte:te1+dte*(nte-1);
% matrix sizes:
n1 = size(compdat,1);
n2 = size(compdat,2);
n3 = size(compdat,3);

% Slice excitation gradient strength in uT/m, depends on slice thickness dz and RF pulse:
if strcmp(rfpulse,'SINC')
 gs1=46970/dz;
elseif strcmp(rfpulse,'EXPO')
 gs1=46970/dz;
elseif strcmp(rfpulse,'GAUSS')
 gs1=26780/dz;
else
 fprintf(1,'Unknown excitation pulse\n');
 return
end



%CALCULATE GRADIENT MAPS
%Calculate complex quotient of data sets for subsequent TE: 
quotfeld=compdat(:,:,:,2:end)./(compdat(:,:,:,1:end-1)+eps);

% Due to the constant TE increment, subsequent entries in quotfeld should basically be identical
% Thus, the nte-1 entries of quotfeld only have to be averaged
% The local B0 offset can be directly obtained from the phases of this average
% For weighted averaging: weight the i-th entry in quotfeld with the entry i+1 in abs(compdat)
% Scale so sum of weighting factors is 1
for i=1:nte-1
 w(:,:,:,i)=abs(compdat(:,:,:,i+1));
end
skal=sum(w,4);
for i=1:nte-1
 w(:,:,:,i)=w(:,:,:,i)./(eps+skal);
end

% Weighted average:
quot=sum(quotfeld.*w,4);

%Calculation x-gradient in uT/m
myhelp=zeros(n1,n2,n3);
myhelp(1,:,:)=quot(2,:,:)./(eps+quot(1,:,:));
myhelp(2:n1-1,:,:)=0.5*(quot(3:n1,:,:)./(eps+quot(2:n1-1,:,:))+quot(2:n1-1,:,:)./(eps+quot(1:n1-2,:,:)));
myhelp(n1,:,:)=quot(n1,:,:)./(eps+quot(n1-1,:,:));
gradx=angle(myhelp)/2/pi/42.5764/dte/dx*1e6;
gradx=gradx.*(abs(gradx)<400)+400*(abs(gradx)>=400);
%Smooth and calculate modulus:
absgradx=abs(smooth3(gradx.*maske,'box',[5,5,1])./(eps+smooth3(maske,'box',[5,5,1])));

%Calculation y-gradient in uT/m
myhelp=zeros(n1,n2,n3);
myhelp(:,1,:)=quot(:,2,:)./(eps+quot(:,1,:));
myhelp(:,2:n2-1,:)=0.5*(quot(:,3:n2,:)./(eps+quot(:,2:n2-1,:))+quot(:,2:n2-1,:)./(eps+quot(:,1:n2-2,:)));
myhelp(:,n2,:)=quot(:,n2,:)./(eps+quot(:,n2-1,:));
grady=angle(myhelp)/2/pi/42.5764/dte/dy*1e6;
grady=grady.*(abs(grady)<400)+400*(abs(grady)>=400);
%Smooth and calculate modulus:
absgrady=abs(smooth3(grady.*maske,'box',[5,5,1])./(eps+smooth3(maske,'box',[5,5,1])));

%Calculation z-gradient in uT/m
myhelp=zeros(n1,n2,n3);
myhelp(:,:,1)=quot(:,:,2)./(eps+quot(:,:,1));
myhelp(:,:,2:n3-1)=0.5*(quot(:,:,3:n3)./(eps+quot(:,:,2:n3-1))+quot(:,:,2:n3-1)./(eps+quot(:,:,1:n3-2)));
myhelp(:,:,n3)=quot(:,:,n3)./(eps+quot(:,:,n3-1));
gradz=angle(myhelp)/2/pi/42.5764/dte/(dz+gap)*1e6;
gradz=gradz.*(abs(gradz)<400)+400*(abs(gradz)>=400);
%Smooth and calculate modulus:
absgradz=abs(smooth3(gradz.*maske,'box',[5,5,1])./(eps+smooth3(maske,'box',[5,5,1])));




% Perform intensity correction, depending on local z-gradient and RF pulse shape:
if strcmp(rfpulse,'SINC')
 for k=1:nte
  corrfact=1./(eps+ralf_t2s_toolbox_sinc(2*absgradz/gs1*te(k)).*cos(pi/2*absgradz/gs1*te(k)));
  corrfact=corrfact.*(abs(corrfact)<5);
  corrdata(:,:,:,k)=abs(compdat(:,:,:,k)).*corrfact;
 end
elseif strcmp(rfpulse,'EXPO')
 for k=1:nte
  corrfact=exp(4*absgradz/gs1*te(k));
  corrfact=corrfact.*(abs(corrfact)<5);
  corrdata(:,:,:,k)=abs(compdat(:,:,:,k)).*corrfact;
 end
elseif strcmp(rfpulse,'GAUSS')
 for k=1:nte
  corrfact=exp(4*(absgradz/gs1*te(k)).^2);
  corrfact=corrfact.*(abs(corrfact)<5);
  corrdata(:,:,:,k)=abs(compdat(:,:,:,k)).*corrfact;
 end
end


% T2S-MAPPING
% with correction for field inhomogeneities in x/y direction
% Evaluation based on corrected modulus data in corrdata (i.e. corrected for field inhomogeneities in z direction)


%Further correction: according to Baudrexel Paper data should not be used if TE exceeds following value:
%TEmax=8220/grad/res  where TEmax is in msec, grad is an in-plane gradient in uT/m and res the respective resolution in mm
% In summary, out of the nte data points per pixel, on some occasions only a limited number should be used due to:
% 1. for remaining points, TE exceeds 8220/absgradx/dx
% 2. for remaining points, TE exceeds 8220/absgrady/dy
% 3. for remaining points, corrdata is zero as corrfact was too large

% Check 1: calculate nmap1
% nmap1 gives for each pixel the maximum number of usable data points due to condition 1 above
TEmax=8220./(1+absgradx)/dx;
nmap1=floor((TEmax-te1)/dte+1);
% nmap1 must never exceed nte:
nmap1=nmap1.*(nmap1<=nte)+nte*(nmap1>nte);

% same for condition 2:
TEmax=8220./(1+absgrady)/dy;
nmap2=floor((TEmax-te1)/dte+1);
nmap2=nmap2.*(nmap2<=nte)+nte*(nmap2>nte);

% same for condition 3:
nmap3=sum(corrdata>0,4);

% get for each pixel minimum of nmap1,nmap2,nmap3 in 2 steps:
minmap=min(nmap1,nmap2);
nmap=min(nmap3,minmap);

% At least the first 4 data points should be used, so proceed as follows:
% Calculate T2* map using only the first i data points
% Get from this T2* maps the pixels where nmap is i and save this submap
% Repeat for all values of i from 4 to nte and sum the submaps
% Initialize matrix for adding up the submaps:
buildmap=zeros(n1,n2,n3);
for i=4:nte
 [amap,t2smap,serie_fitted,rmap]=my_t2s_fit_corr(te1,dte,abs(corrdata(:,:,:,1:i)));
 t2smap=t2smap.*(t2smap>1 & t2smap<2000)+2000*(t2smap>=2000);
 mymask=maske.*(i==nmap);
 if isempty(mymask)==0
  buildmap=buildmap+t2smap.*mymask;
 end
end
t2smap_corrected=buildmap;

