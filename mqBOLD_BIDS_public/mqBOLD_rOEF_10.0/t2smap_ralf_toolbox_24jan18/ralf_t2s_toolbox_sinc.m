function y=ralf_t2s_toolbox_sinc(x)

% y=ralf_t2s_toolbox_sinc(x)
% Function for sinc calculation
% WARNING ABOUT SCALING: the function returns sin(PI*x)/(PI*x)
% This corresponds to matlab function "sinc"
% The purpose of this function is simply to avoid using 
% up a license for the signal proc. toolbox


y=sin(pi*x)./(eps+pi*x).*(x~=0)+(x==0);
