function  [newcomp1,newcomp2,dpe,npes1,npes2]=ralf_t2s_toolbox_get_PEdir_get_PEsteps(comp1,comp2)

% [newcomp1,newcomp2,dpe,npes1,npes2]=ralf_t2s_toolbox_get_PEdir_get_PEsteps(comp1,comp2)
% The function analyzes 2 complex image data sets comp1, comp2
% Both data sets can have 3 or 4 dimensions
% The first 2 dimensions must be read and phase (in any order)
% The 3rd and 4th dimensions are slices and time points 
% comp1 was acquired with full spatial resolution
% comp2 was acquired with reduced spatial resolution in phase encoding (PE) direction
% comp2 was smoothed to get the same matrix size as comp1
% The function helps to reconstruct clean raw data from comp1 and comp2, handling the following problem:
% For comp1, raw data can be obtained by FFT
% For comp2, due to the smoothing an FFT will not yield clean raw data
% The function returns newcomp1, newcomp2, dpe, npes1, and npes2
% newcomp1 is a new version of comp1 with the following feature:
% There are no edge lines or edge rows with zeros
% newcomp2 is a new version of comp2 with the following features:
% 1. An FFT of newcomp2 yields more or less clean raw data
% 2. an FFT of newcomp2 yields in PE direction the acquired non-zero lines, the other lines are zero
% 3. the effect of the smoothing filter in these data is compensated
% 4. There are no edge lines or edge rows with zeros
% dpe is the number of the dimension in which phase encoding is directed (value 1 or 2)
% npes1 is the number of phase encoding steps in comp1
% npes2 is the number of non-zero phase encoding steps in comp2
% npes2 < npes1 because the resolution in comp2 is reduced


datdim=ndims(comp1);

% First, replace first or last row or column by opposite row or column, if it contains zeros only
% To make code easier, 4D data will be assumed
% If data set is 3D, a singleton dimension will be inserted as 3rd dimension
% In this case, use squeeze later to throw out this dimension again
if datdim==3
 comp1=permute(shiftdim(comp1,-1),[2 3 1 4]);
 comp2=permute(shiftdim(comp2,-1),[2 3 1 4]);
end

% flags indicating if first or last row or column contains zeros:
first_row_empty=0;
last_row_empty=0;
first_col_empty=0;
last_col_empty=0;

% Check if first row is zero:
myhelp=comp1(1,:,:,:);
if sum(abs(myhelp(:)))==0
 first_row_empty=1;
end

% Check last row:
myhelp=comp1(end,:,:,:);
if sum(abs(myhelp(:)))==0
 last_row_empty=1;
end

% Check first column:
myhelp=comp1(:,1,:,:);
if sum(abs(myhelp(:)))==0
 first_col_empty=1;
end

% Check last column:
myhelp=comp1(:,end,:,:);
if sum(abs(myhelp(:)))==0
 last_col_empty=1;
end

% If first or last row is zero, replace it by last or first one, respectively:
if first_row_empty==1
 comp1(1,:,:,:)=comp1(end,:,:,:);
 comp2(1,:,:,:)=comp2(end,:,:,:);
elseif last_row_empty==1
 comp1(end,:,:,:)=comp1(1,:,:,:);
 comp2(end,:,:,:)=comp2(1,:,:,:);
end

% Similar for columns:
if first_col_empty==1
 comp1(:,1,:,:)=comp1(:,end,:,:);
 comp2(:,1,:,:)=comp2(:,end,:,:);
elseif last_col_empty==1
 comp1(:,end,:,:)=comp1(:,1,:,:);
 comp2(:,end,:,:)=comp2(:,1,:,:);
end

% Squeeze out singleton dimension:
if datdim==3
 comp1=squeeze(comp1);
 comp2=squeeze(comp2);
end

% Export newcomp1 (i.e. comp1 without zero rows/columns
newcomp1=comp1;


% Perform 2D FFT of both data stes and calculate quotient of 2nd/1st
raw1=ifftshift(ifft2(fftshift(comp1)));
raw2=ifftshift(ifft2(fftshift(comp2)));
quot=abs(raw2)./(eps+abs(raw1));
quot=quot.*(quot<2);
% Calculate mean across 3rd dimension and 4th dimension (if available):
if datdim==3
 mquot=mean(quot,3);
end
if datdim==4
 mquot=mean(mean(quot,3),4);
end
% Calculate integral:
integ=sum(mquot(:))/size(mquot,2)/size(mquot,1);
% integ is a value between 0 and 1 and indicates the approximate percentage of PE sampling
% However, the PE direction is still not known

% get matrix sizes np1 and np2 in both directions
% get center points cp1 and cp2 in k-space in both directions
np1=size(mquot,1);
cp1=floor(np1/2+1);
np2=size(mquot,2);
cp2=floor(np2/2+1);

% Assumption 1: PE is in 1st direction, so get central portion from integ and calculate mean value across this portion:
teil=mquot((cp1-round(0.5*integ*np1)):(cp1+round(0.5*integ*np1)),:);
cover1=mean(teil(:));
% Assumption 2: PE is in 2nd direction
teil=mquot(:,(cp2-round(0.5*integ*np2)):(cp2+round(0.5*integ*np2)));
cover2=mean(teil(:));
% Now the PE direction can be obtained
if(cover1>cover2)
 dpe=1;
else
 dpe=2;
end
npes1=size(raw1,dpe);
% Now the direction of phase encoding is known, and also npes1


% For the subsequent analysis:
% if several time points were acquired, only the first one will be analyzed:
if datdim==3
 cdat1=comp1;
 cdat2=comp2;
end
if datdim==4
 cdat1=comp1(:,:,:,1);
 cdat2=comp2(:,:,:,1);
end


% Perform FFT of both data sets in PE direction and calculate quotient of 2nd/1st
raw1=ifftshift(ifft(fftshift(cdat1),[],dpe));
raw2=ifftshift(ifft(fftshift(cdat2),[],dpe));
quot=abs(raw2./(eps+raw1));
quot=quot.*(quot<2);

% Calculate median of mquot across read direction for central 50% of k-space, this is the profile:
if dpe==1
 teil=quot(:,ceil(np2/4):ceil(np2*3/4),:);
 testprofile=median(median(teil,2),3);
end
if dpe==2
 teil=quot(ceil(np1/4):ceil(np1*3/4),:,:);
 testprofile=median(median(teil,1),3);
end

% Get number of seemingly acquired PE steps
% This is the number of steps where the profile is sufficiently high
npes2=sum(testprofile>0.25);
npes2=2*round(npes2/2);

% testprofile will be unsymmetric
% calculate a symmetric profile by averaging left-hand and right-hand branch 
profile=testprofile;
if dpe==1
 mw=(np1+1)/2;
 for i=2:np1-1
  profile(i)=0.5*(testprofile(i)+testprofile(2*mw-i));
 end
end
if dpe==2
 mw=(np2+1)/2;
 for i=2:np2-1
  profile(i)=0.5*(testprofile(i)+testprofile(2*mw-i));
 end
end

% Perform FFT of second data set in PE direction
raw=ifftshift(ifft(fftshift(comp2),[],dpe));
% Correct data in this way:
% 1. keep only central npes2 lines, the other ones are set to zero
% 2. correct amplitudes by division by profile
if datdim==3
 if dpe==1
  corrfeld=repmat(1./(eps+profile), [1 size(raw,2) size(raw,3)]);
  raw=raw.*corrfeld;
  raw(1:cp1-npes2/2-1,:,:)=0;
  raw(cp1+npes2/2:end,:,:)=0;
 end
 if dpe==2
  corrfeld=repmat(1./(eps+profile), [size(raw,1) 1 size(raw,3)]);
  raw=raw.*corrfeld;
  raw(:,1:cp2-npes2/2-1,:)=0;
  raw(:,cp2+npes2/2:end,:)=0;
 end
end
if datdim==4
 if dpe==1
  corrfeld=repmat(1./(eps+profile), [1 size(raw,2) size(raw,3) size(raw,4)]);
  raw=raw.*corrfeld;
  raw(1:cp1-npes2/2-1,:,:,:)=0;
  raw(cp1+npes2/2:end,:,:,:)=0;
 end
 if dpe==2
  corrfeld=repmat(1./(eps+profile), [size(raw,1) 1 size(raw,3) size(raw,4)]);
  raw=raw.*corrfeld;
  raw(:,1:cp2-npes2/2-1,:,:)=0;
  raw(:,cp2+npes2/2:end,:,:)=0;
 end
end
% Convert corrected raw data into image space:
newcomp2=ifftshift(fft(fftshift(raw),[],dpe));

