function cdattarg=ralf_t2s_toolbox_create_target_3datasets(cdat1,cdat2,cdat3,te1,dte)

% function cdattarg=ralf_t2s_toolbox_create_target_3datasets(cdat1,cdat2,cdat3,te1,dte)
% Creates a target data set cdattarg that yields highest correlation values upon T2* fitting
% cdat1, cdat2, cdat3 are complex input data sets, with 3 dimensions each
% The first 2 dimensions are spatial, the 3rd one corresponds to different TE values
% te1 is the first TE value (in msec), dte is the TE increment (in msec)
% For each pixel, cdattarg assumes the data from the input data set yielding highest correlation

% Get number of TE values:
nte=size(cdat1,3);

% Perform T2* fit for each data set
rmap=zeros(size(cdat1,1),size(cdat1,2),3);
[amap,t2smap1,serie_fitted,rmap(:,:,1)]=my_t2s_fit_corr(te1,dte,abs(cdat1));       
[amap,t2smap2,serie_fitted,rmap(:,:,2)]=my_t2s_fit_corr(te1,dte,abs(cdat2));       
[amap,t2smap3,serie_fitted,rmap(:,:,3)]=my_t2s_fit_corr(te1,dte,abs(cdat3));       

% Create masks: which data set yields highest correlation values?
[y,maxind]=max(rmap,[],3);
m1=(maxind==1);
m2=(maxind==2);
m3=(maxind==3);

%old code:
%rmap1=rmap(:,:,1);
%rmap2=rmap(:,:,2);
%rmap3=rmap(:,:,3);
%m1=rmap1>rmap2 & rmap1>rmap3;
%m2=rmap2>rmap1 & rmap2>rmap3;
%m3=rmap3>rmap1 & rmap3>rmap2;


m1=repmat(m1,[1,1,nte]);
m2=repmat(m2,[1,1,nte]);                                                                   
m3=repmat(m3,[1,1,nte]);                                                                   

% Construct cdattarg
cdattarg=m1.*cdat1+m2.*cdat2+m3.*cdat3;

