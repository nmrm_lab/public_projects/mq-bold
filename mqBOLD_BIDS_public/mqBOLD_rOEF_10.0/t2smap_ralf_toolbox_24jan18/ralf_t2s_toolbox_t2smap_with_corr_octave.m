function [t2smap_corrected, SubjectDir, filelist] = ...
    ralf_t2s_toolbox_t2smap_with_corr_octave(compdat, maske, te1, dte, dx, ...
    dy, dz, gap, T2s_pars, V_save, SubjectDir, filelist, subject_name, ncd) 
    
rfpulse = T2s_pars.pulseshape;

% Original comment of Ralf Deichmann (RD)
% t2smap_corrected=ralf_t2s_toolbox_t2smap_with_corr_octave(compdat,maske,...
%           te1,dte,dx,dy,dz,gap,rfpulse)
% T2* mapping with correction for B0 field inhomogeneities in all directions 
% for equidistant TE increase
% This corresponds to ralf_t2s_toolbox_t2smap_with_corr, fixing the Octave 
% ideosyncras(z)y (see below)
% compdat is a 4D complex data set
% The first 2 dimensions are read and phase (any order), the 3rd dimension
% is slice, the 4th is TE-weighting
% maske is a binary mask with the same dimensions as compdat(:,:,:,1), 
% i.e. same spatial coverage
% T2* will be evaluated only where maske is non-zero
% te1 and dte are the first TE value and the TE increment, both in msec (!)
% dx, dy are the spatial resolution in the first/second dimension, in mm
% dz is the slice thickness, in mm
% gap is the gap between slices, in mm
% rfpulse is a string denoting the excitation pulse shape 
% (referring to sequence ralf_t2smap_fc_dum)
% rfpulse can take the following 3 values: 'SINC', 'EXPO', 'SG'

%number of different TE:
nte=size(compdat,4);
% list of TE values, in msec:
te=te1:dte:te1+dte*(nte-1);
% matrix sizes:
n1 = size(compdat,1);
n2 = size(compdat,2);
n3 = size(compdat,3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CP: Original settings of RD (JAN 2018) 
% % Slice excitation gradient strength in uT/m,
% % depends on slice thickness dz and RF pulse:
% if strcmp(rfpulse,'SINC')
%  gs1=46970/dz;
% elseif strcmp(rfpulse,'EXPO')
%  gs1=46970/dz;
% elseif strcmp(rfpulse,'GAUSS')
%  gs1=26780/dz;
% else
%  fprintf(1,'Unknown excitation pulse\n');
%  return
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CP 03JUL2018: replaced by settings for Philips Ingenia
% CAVE: needs to be adapted according to sequence
% CP 01APR20: setting of gs1 and pulsedur is shifted to initialisation part
% --> RUN_mqBOLD_auto() --> T2s_pars = T2s_settings()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set slice ex gradient strength (for variable pulse duration!!!)
% gs1: Strength of slice selection gradient [uT/m]
% RF pulseshape dependent settings are performed in T2s_settings()
fprintf('\nPerform correction of magnetic background gradients, with:');
fprintf('\nfor %s data and RF pulse shape %s with',...
        T2s_pars.manufacturer, T2s_pars.pulseshape);
if (dz == 1) % slice thickness 1 mm
    if T2s_pars.gs1(1) > 0
        gs1 = T2s_pars.gs1(1);
        pulsedur = T2s_pars.pulsedur(1);
        fprintf('\n slice thickness %d, gradient strength %d, RF pulse duration %3.2f\n',...
            dz, gs1, pulsedur); 
    else
        fprintf('\nCannot perform valid correction for slice thickness %d!',dz);
        return;
    end
elseif (dz == 2)
    if T2s_pars.gs1(2) > 0
        gs1 = T2s_pars.gs1(2);
        pulsedur = T2s_pars.pulsedur(2);
        fprintf('\n slice thickness %d, gradient strength %d, RF pulse duration %3.2f\n',...
            dz, gs1, pulsedur);  
    else
        fprintf('\nCannot perform valid correction for slice thickness %d!',dz);
        return;
    end      
elseif (dz == 3) % DEFAULT setting!!!
    if T2s_pars.gs1(3) > 0
        gs1 = T2s_pars.gs1(3);
        pulsedur = T2s_pars.pulsedur(3);
        fprintf('\n slice thickness %d, gradient strength %d, RF pulse duration %3.2f\n',...
            dz, gs1, pulsedur);
    else
        fprintf('\nCannot perform valid correction for slice thickness %d!',dz);
        return;
    end        
elseif (dz == 4)
    if T2s_pars.gs1(4) > 0
        gs1 = T2s_pars.gs1(4);
        pulsedur = T2s_pars.pulsedur(4);
        fprintf('\n slice thickness %d, gradient strength %d, RF pulse duration %3.2f\n',...
            dz, gs1, pulsedur); 
    else
        fprintf('\nCannot perform valid correction for slice thickness %d!',dz);
        return;
    end       
elseif (dz == 5)
    if T2s_pars.gs1(5) > 0
        gs1 = T2s_pars.gs1(5);
        pulsedur = T2s_pars.pulsedur(5);
        fprintf('\n slice thickness %d, gradient strength %d, RF pulse duration %3.2f\n',...
            dz, gs1, pulsedur); 
    else
        fprintf('\nCannot perform valid correction for slice thickness %d!',dz);
        return;
    end          
else
 	fprintf('\nCannot perform valid correction for slice thickness %d!',dz);
  	fprintf('\nValid slice thicknesses: 1, 2, 3, 4 or 5 mmm.');
	fprintf('\nPress any key to go on with the uncorrected T2* map.');
  	return;
end % if (dz == 1) % slice thickness 1 mm


%CALCULATE GRADIENT MAPS
%Calculate complex quotient of data sets for subsequent TE: 
quotfeld=compdat(:,:,:,2:end)./(compdat(:,:,:,1:end-1)+eps);

% Due to the constant TE increment, subsequent entries in quotfeld should 
% basically be identical
% Thus, the nte-1 entries of quotfeld only have to be averaged
% The local B0 offset can be directly obtained from the phases of this average
% For weighted averaging: 
% weight the i-th entry in quotfeld with the entry i+1 in abs(compdat)
% Scale so sum of weighting factors is 1
for i=1:nte-1
 w(:,:,:,i)=abs(compdat(:,:,:,i+1));
end
skal=sum(w,4);
for i=1:nte-1
 w(:,:,:,i)=w(:,:,:,i)./(eps+skal);
end

% Weighted average:
quot=sum(quotfeld.*w,4);

%Calculation x-gradient in uT/m
myhelp=zeros(n1,n2,n3);
myhelp(1,:,:)=quot(2,:,:)./(eps+quot(1,:,:));
myhelp(2:n1-1,:,:)=0.5*(quot(3:n1,:,:)./...
    (eps+quot(2:n1-1,:,:))+quot(2:n1-1,:,:)./(eps+quot(1:n1-2,:,:)));
myhelp(n1,:,:)=quot(n1,:,:)./(eps+quot(n1-1,:,:));
gradx=angle(myhelp)/2/pi/42.5764/dte/dx*1e6;
gradx=gradx.*(abs(gradx)<400)+400*(abs(gradx)>=400);
%Smooth and calculate modulus:

% Comment RD: This was the original command:
% absgradx=abs(smooth3(gradx.*maske,'box',[5,5,1])./...
%             (eps+smooth3(maske,'box',[5,5,1])));
% Problem in Octave: the box of size [5,5,1] in smooth3 collapses 
% as the last singleton dimension is removed, so box becomes 2D
% Workaround: use convn and 5x5x3 kernel, but only middle layer is non-zero

kernel=zeros(5,5,3);
kernel(:,:,2)=1/25;
absgradx=abs(convn(gradx.*maske,kernel,'same')./...
    (eps+convn(maske,kernel,'same')));

%Calculation y-gradient in uT/m
myhelp=zeros(n1,n2,n3);
myhelp(:,1,:)=quot(:,2,:)./(eps+quot(:,1,:));
myhelp(:,2:n2-1,:)=0.5*(quot(:,3:n2,:)./...
    (eps+quot(:,2:n2-1,:))+quot(:,2:n2-1,:)./(eps+quot(:,1:n2-2,:)));
myhelp(:,n2,:)=quot(:,n2,:)./(eps+quot(:,n2-1,:));
grady=angle(myhelp)/2/pi/42.5764/dte/dy*1e6;
grady=grady.*(abs(grady)<400)+400*(abs(grady)>=400);
%Smooth and calculate modulus:
absgrady=abs(convn(grady.*maske,kernel,'same')./...
    (eps+convn(maske,kernel,'same')));

%Calculation z-gradient in uT/m
myhelp=zeros(n1,n2,n3);
myhelp(:,:,1)=quot(:,:,2)./(eps+quot(:,:,1));
myhelp(:,:,2:n3-1)=0.5*(quot(:,:,3:n3)./...
    (eps+quot(:,:,2:n3-1))+quot(:,:,2:n3-1)./(eps+quot(:,:,1:n3-2)));
myhelp(:,:,n3)=quot(:,:,n3)./(eps+quot(:,:,n3-1));
gradz=angle(myhelp)/2/pi/42.5764/dte/(dz+gap)*1e6;
gradz=gradz.*(abs(gradz)<400)+400*(abs(gradz)>=400);
%Smooth and calculate modulus:
absgradz=abs(convn(gradz.*maske,kernel,'same')./...
    (eps+convn(maske,kernel,'same')));

%CP 03JUL2018: Write absgrad x, y, z to file
absgradb0sq = absgradz.^2 + absgradx.^2 + absgrady.^2;
absgradb0 = absgradb0sq.^0.5;
absgradb0 = absgradb0.*maske;

if nargin > 9
        filelist.condition(ncd).T2s.absgradx = ...
            fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
            sprintf('absgradx_%s.nii',subject_name));
        V_save.fname = filelist.condition(ncd).T2s.absgradx;
        spm_write_vol(V_save, absgradx);

        filelist.condition(ncd).T2s.absgrady = ...
            fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
            sprintf('absgrady_%s.nii',subject_name));
        V_save.fname = filelist.condition(ncd).T2s.absgrady;
        spm_write_vol(V_save, absgrady);
      
        filelist.condition(ncd).T2s.absgradz = ...
            fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
            sprintf('absgradz_%s.nii',subject_name));
        V_save.fname = filelist.condition(ncd).T2s.absgradz;
        spm_write_vol(V_save, absgradz);

        filelist.condition(ncd).T2s.absgradb0 = ...
            fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
            sprintf('absgradb0_%s.nii',subject_name));
        V_save.fname = filelist.condition(ncd).T2s.absgradb0;
        spm_write_vol(V_save, absgradb0);  
end

% % Original Correction factors by RD (JAN 2018)
% % Perform intensity correction, depending on local z-gradient 
% % and RF pulse shape:
% if strcmp(rfpulse,'SINC')
%  for k=1:nte
%   corrfact=1./(eps+ralf_t2s_toolbox_sinc(2*absgradz/gs1*te(k))...
%               .*cos(pi/2*absgradz/gs1*te(k)));
%   corrfact=corrfact.*(abs(corrfact)<5);
%   corrdata(:,:,:,k)=abs(compdat(:,:,:,k)).*corrfact;
%  end
% elseif strcmp(rfpulse,'EXPO')
%  for k=1:nte
%   corrfact=exp(4*absgradz/gs1*te(k));
%   corrfact=corrfact.*(abs(corrfact)<5);
%   corrdata(:,:,:,k)=abs(compdat(:,:,:,k)).*corrfact;
%  end
% elseif strcmp(rfpulse,'GAUSS')
%  for k=1:nte
%   corrfact=exp(4*(absgradz/gs1*te(k)).^2);
%   corrfact=corrfact.*(abs(corrfact)<5);
%   corrdata(:,:,:,k)=abs(compdat(:,:,:,k)).*corrfact;
%  end
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CP 03JUL2018: replaced by adapted settings for Ingenia
% CAVE: needs to be adapted to employed pulses
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k=1:nte
	if (strcmpi( rfpulse, 'SG'))
     	corrfact=sin(1.5*pi*absgradz/gs1*(te(k)/(2.7282*pulsedur/2)))./...
          	    (eps+1.5*pi*absgradz/gs1*(te(k)/(2.7282*pulsedur/2)))...
                *2.25.*exp(-(absgradz/gs1*(te(k)/(2.7282*pulsedur/2))/1.67).^2);
  	elseif (strcmpi( rfpulse, 'SINC'))
       	corrfact=sin(2*pi*absgradz/gs1*(te(k)/(pulsedur/2)))./ ...
                (eps+2*pi*absgradz/gs1*(te(k)/(pulsedur/2)));
        corrfact=corrfact.*cos(pi/2*absgradz/gs1*(te(k)/(pulsedur/2)));
	elseif (strcmpi( rfpulse, 'EXPO'))
       	corrfact=exp(-4*absgradz/gs1*(te(k)/(pulsedur/2)));
	end %if (strcmpi( rfpulse, 'SG'))
	corrfact=1./(corrfact+eps);
	corrfact=corrfact.*(abs(corrfact)<5);
	corrdata(:,:,:,k)=abs(compdat(:,:,:,k)).*corrfact;
end

% T2S-MAPPING
% with correction for field inhomogeneities in x/y direction
% Evaluation based on corrected modulus data in corrdata 
% (i.e. corrected for field inhomogeneities in z direction)


%Further correction: according to Baudrexel Paper data should not be used 
%                    if TE exceeds following value:
% TEmax=8220/grad/res  where TEmax is in msec, grad is an in-plane gradient 
%                      in uT/m and res the respective resolution in mm
% In summary, out of the nte data points per pixel, on some occasions only 
%             a limited number should be used due to:
% 1. for remaining points, TE exceeds 8220/absgradx/dx
% 2. for remaining points, TE exceeds 8220/absgrady/dy
% 3. for remaining points, corrdata is zero as corrfact was too large

% Check 1: calculate nmap1
% nmap1 gives for each pixel the maximum number of usable data points due 
% to condition 1 above
TEmax=8220./(1+absgradx)/dx;
nmap1=floor((TEmax-te1)/dte+1);
% nmap1 must never exceed nte:
nmap1=nmap1.*(nmap1<=nte)+nte*(nmap1>nte);

% same for condition 2:
TEmax=8220./(1+absgrady)/dy;
nmap2=floor((TEmax-te1)/dte+1);
nmap2=nmap2.*(nmap2<=nte)+nte*(nmap2>nte);

% same for condition 3:
nmap3=sum(corrdata>0,4);

% get for each pixel minimum of nmap1,nmap2,nmap3 in 2 steps:
minmap=min(nmap1,nmap2);
nmap=min(nmap3,minmap);

% At least the first 4 data points should be used, so proceed as follows:
% Calculate T2* map using only the first i data points
% Get from this T2* maps the pixels where nmap is i and save this submap
% Repeat for all values of i from 4 to nte and sum the submaps
% Initialize matrix for adding up the submaps:
buildmap=zeros(n1,n2,n3);
for i=4:nte
    [amap,t2smap,serie_fitted,rmap] = ...
        my_t2s_fit_corr(te1,dte,abs(corrdata(:,:,:,1:i)));
    t2smap=t2smap.*(t2smap>1 & t2smap<2000)+2000*(t2smap>=2000);
    mymask=maske.*(i==nmap);
    if isempty(mymask)==0
        buildmap=buildmap+t2smap.*mymask;
    end
end
t2smap_corrected = buildmap;
end
