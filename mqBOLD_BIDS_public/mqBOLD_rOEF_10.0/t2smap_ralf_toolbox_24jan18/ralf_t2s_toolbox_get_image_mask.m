function [imamask,noiselimit]=ralf_t2s_toolbox_get_image_mask(vol)
% [imamask,noiselimit]=ralf_t2s_toolbox_get_image_mask(vol)
% function for finding a mask excluding noise in 2D or 3D image data
% vol is a 2D or 3D modulus image data set
% imamask is a mask being zero wherever vol contains noise only
% noiselimit is the intensity below which data in vol are assumed to represent noise only


% Order signal intensities in descending order
values=sort(vol(:),1,'descend');
% number of pixels:
npix=size(values,1);
% Get maximum intensity, but ignore first 1%
% i.e. ignore 1% brightest pixels to prevent single spikes from messing up program
%maxval=values(round(0.01*npix));
%Probably better: just ignore the 100 brightest pixels
maxval=values(101);
%maxval is now more or less the upper limit for signal intensities

% get histogram for image pixels up to maxval:
xx=2:maxval;
yy=hist(vol(:),xx);
% The last bin will contain all pixels beyond maxval
% This causes a peak in the last entry of yy
% remove this peak in yy by setting:
yy(end)=yy(end-1);

% The histogram yy contains now 4 regions of interest, see plot(xx,yy):
% 1. low values: many pixels, arises from noise.
% 2. intermediate values: few pixels, this is the gap between noise and object intensities
% 3. higher values: peak in yy, due to pixels inside object
% 4. beyond: few pixels with intensities much higher than average object signal
% What we want to know: positions of peaks forming region 1 and region 3
% The limit separating noise from data should be the average of these two values

%Square this function:
yy=yy.^2;
% Assume that yy is a distribution of masses

% find center of mass (COM)
xs=ceil(sum(xx.*yy)/sum(yy));
% find index of center of mass
Is=find(xx==xs);

% mass right of COM:
mass_r=sum(yy(Is+1:end));
% angular moment of this mass:
moment_r=sum((xx(Is+1:end)-xs).*yy(Is+1:end));
% assume this mass is concentrated at one point, get distance of this point from COM:
abstand_r=ceil(moment_r/mass_r);
% so right-hand peak is approximately here:
peak_r=xs+abstand_r;

% similar for masses to the left of COM:
mass_l=sum(yy(1:Is-1));
moment_l=sum((xs-xx(1:Is-1)).*yy(1:Is-1));
abstand_l=ceil(moment_l/mass_l);
peak_l=xs-abstand_l;

% so the noise limit is between these peaks:
noiselimit=0.5*(peak_l+peak_r);

% Mask cutting off background noise
imamask=vol>noiselimit;

