function [SubjectDir, filelist] = calc_T2S_Moco_Gcorr_RD(SubjectDir, filelist, ...
            subject_name, ncd, vol_T2S_fr, vol_T2S_hr, vol_T2S_qr, T2s_pars, ...
            nechoes, TE, nslices, sliceThickness, voxel_size, V_save,...
            Manufacturer)

% Set this flag to 1 to show plots of weighting factors for each slice:
display_flag=0;

%   Parameters required
    nslc = nslices;  % number of slices
    nte = nechoes;   % number of TE values
    te1 = TE(1);    % first TE in msec
    dte = TE(2)-TE(1);    % TE increment in msec
    dx = voxel_size(1); % resolution in first in-plane dimension in mm
    dy = voxel_size(2); % resolution in second in-plane dimension in mm
    dz = sliceThickness; % slice thickness in mm (not counting any gaps)
    gap = voxel_size(3) - sliceThickness;  % gap between slices in mm

if strcmp(Manufacturer, 'Philips')
    % vol contains 3 elements:
    % 1. the magnitude data
    % 2. the real part (with offset of 2047)
    % 3. the imaginary part (with offset of 2047)
    offset=2047;
    mymag=vol_T2S_fr(:,:,:,1:nte);
    myreal=vol_T2S_fr(:,:,:,nte+1:2*nte)-offset;
    myimag=vol_T2S_fr(:,:,:,2*nte+1:3*nte)-offset;
    comp1=myreal+sqrt(-1)*myimag;

    % Same for half resolution dat set:
    offset=2047;
    mymag=vol_T2S_hr(:,:,:,1:nte);
    myreal=vol_T2S_hr(:,:,:,nte+1:2*nte)-offset;
    myimag=vol_T2S_hr(:,:,:,2*nte+1:3*nte)-offset;
    comp2=myreal+sqrt(-1)*myimag;

    % Same for quarter resolution dat set:
    offset=2047;
    mymag=vol_T2S_qr(:,:,:,1:nte);
    myreal=vol_T2S_qr(:,:,:,nte+1:2*nte)-offset;
    myimag=vol_T2S_qr(:,:,:,2*nte+1:3*nte)-offset;
    comp3=myreal+sqrt(-1)*myimag;
elseif strcmp(Manufacturer, 'Siemens')
    % For Siemens data, vol contains 2 elements:
    % 1. the magnitude data
    % 2. the phase data (with offset????)
    maxval=4096;
    mymag=vol_T2S_fr(:,:,:,1:nte);
    myphase=vol_T2S_fr(:,:,:,nte+1:2*nte);
    phi = 2*pi*myphase/maxval;
    comp1 = mymag.*exp(sqrt(-1)*phi);

    % Same for half resolution dat set:
    mymag=vol_T2S_hr(:,:,:,1:nte);
    myphase=vol_T2S_hr(:,:,:,nte+1:2*nte);
    phi = 2*pi*myphase/maxval;
    comp2 = mymag.*exp(sqrt(-1)*phi);

    % Same for quarter resolution dat set:
    mymag=vol_T2S_qr(:,:,:,1:nte);
    myphase=vol_T2S_qr(:,:,:,nte+1:2*nte);
    phi = 2*pi*myphase/maxval;
    comp3 = mymag.*exp(sqrt(-1)*phi);     
end %if strcmp(Manufacturer, 'Philips' // 'Siemens')

% PART 2:
% Get number of acquired PE steps and direction of PE
fprintf('T2*-Mapping: Preparatory analysis of data\n\n');
[~,newcomp2,dpe,npes1,npes2] =...
    ralf_t2s_toolbox_get_PEdir_get_PEsteps(comp1,comp2);
fprintf('T2*-Mapping: Comparing full and half resolution data:\n');
fprintf('   ----> Phase encoding is in dimension %d\n',dpe);
fprintf('   ----> Number of PE steps in full/half resolution data is %d/%d\n\n',...
                                                            npes1,npes2);

[newcomp1,newcomp3,dpe,npes1,npes3] = ...
    ralf_t2s_toolbox_get_PEdir_get_PEsteps(comp1,comp3);
fprintf('T2*-Mapping: Comparing full and quarter resolution data:\n');
fprintf('   ----> Phase encoding is in dimension %d\n',dpe);
fprintf('   ----> Number of PE steps in quarter resolution data is %d\n',...
                                                            npes3);


% PART 3: construct combined data set comp4, do this slice-wise:
fprintf('T2*-Mapping: Movement correction, will be done slice-wise\n');
comp4=zeros(size(comp1));
w0list=zeros(npes1,nslc);
w1list=zeros(npes1,nslc);
w2list=zeros(npes1,nslc);

%--------------------------------------------------------------------------
% Perform movement correction of T2* weighted data according to
% N�th U, Volz S, Hattingen E, Deichmann R. An improved method for 
% retrospective motion correction in quantitative T2* mapping.
% Neuroimage. 2014 May 15;92:106-19. 
% doi: 10.1016/j.neuroimage.2014.01.050. Epub 2014 Feb 4.
%--------------------------------------------------------------------------
if display_flag
    figure;
end
for cslc=1:nslc
 fprintf(1,'T2*-Mapping: movement correction of slice %d out of %d\n',...
                                                            cslc,nslc);
 % extract data for this slice:
 cb1=squeeze(newcomp1(:,:,cslc,:));
 cb2=squeeze(newcomp2(:,:,cslc,:));
 cb3=squeeze(newcomp3(:,:,cslc,:));

 % get magnitude images for lowest TE as references 
 % and perform movement correction:
 ref1=abs(cb1(:,:,1));
 ref2=abs(cb2(:,:,1));
 ref3=abs(cb3(:,:,1));

 [~,corrfeld]=ralf_t2s_toolbox_inplane_shift_correct(ref1,ref2);
 raw=ifftshift(ifft2(fftshift(cb2)));
 raw=raw.*repmat(corrfeld, [1 1 nte]);
 cb2mc=ifftshift(fft2(fftshift(raw)));
 
 [~,corrfeld]=ralf_t2s_toolbox_inplane_shift_correct(ref1,ref3);
 raw=ifftshift(ifft2(fftshift(cb3)));
 raw=raw.*repmat(corrfeld, [1 1 nte]);
 cb3mc=ifftshift(fft2(fftshift(raw)));

 %Result: cb2mc and cb3mc are movement corrected versions of  cb2 and cb3
 
 bild=1/3*(abs(cb1(:,:,1))+abs(cb2mc(:,:,1))+abs(cb3mc(:,:,1)));
 [maske,~]=ralf_t2s_toolbox_get_image_mask(bild);

 % perform phase correction for temperature drifts
 cb2mcpc=ralf_t2s_toolbox_phase_drift_correct_new(cb1,cb2mc,te1,dte,maske);
 cb3mcpc=ralf_t2s_toolbox_phase_drift_correct_new(cb1,cb3mc,te1,dte,maske);

 %Result: cb2mcpc and cb3mcpc

 % Construct optimum combination of 3 data sets
 % noise level of 10%:
 nl=0.1;
 [~,w0,w1,w2]=ralf_t2s_toolbox_combine_3_datasets_with_noise(cb1,cb2mcpc,...
                                cb3mcpc,npes2,npes3,dpe,te1,dte,maske,nl);
 [icdatcomb,iw0,iw1,iw2]=ralf_t2s_toolbox_improve_weightings_3datasets(cb1,...
                                cb2mcpc,cb3mcpc,dpe,w0,w1,w2);
 if display_flag==1
  plot(1:npes1,iw0,1:npes1,iw1,1:npes1,iw2)
  drawnow
 end
 w0list(:,cslc)=iw0;
 w1list(:,cslc)=iw1;
 w2list(:,cslc)=iw2;

 comp4(:,:,cslc,:)=icdatcomb;
end

fprintf('T2*-Mapping: Saving weighting factors in RESULTS as weighting_factors_t2star_moco.mat\n');
filename = fullfile(SubjectDir.condition(ncd).T2s.toplevel,...
                                'weighting_factors_t2star_moco.mat');
save(filename, 'w0list', 'w1list', 'w2list', '-v7')


% Create mask from abs(comp4) for first TE:
fprintf('T2*-Mapping: Masking data\n');
[maske,~] = ralf_t2s_toolbox_get_image_mask(abs(comp4(:,:,:,1)));

%--------------------------------------------------------------------------
% perform T2* fit with correction for field inhomogeneities in 3 directions
% according to:
% Baudrexel S, Volz S, Preibisch C, Klein JC, Steinmetz H, Hilker R, 
% Deichmann R. Rapid single-scan T2*-mapping using exponential excitation 
% pulses and image-based correction for linear background gradients.
% Magn Reson Med. 2009 Jul;62(1):263-8. doi: 10.1002/mrm.21971.
%--------------------------------------------------------------------------
fprintf('T2*-Mapping: Calculating the T2* Map (at last)\n');
fprintf('T2*-Mapping of motion corrected data, please be patient\n');
[T2S_corr, SubjectDir, filelist] = ralf_t2s_toolbox_t2smap_with_corr_octave(...
    comp4, maske, te1, dte, dx, dy, dz, gap, T2s_pars, V_save, SubjectDir, ...
    filelist, subject_name, ncd);

% set upper limit to avoid interpolation problem at coregistration &
% normalizaton
T2S_corr(T2S_corr > 150) = 150;

fprintf('T2*-Mapping of uncorrected data (no MoCo), please be patient again\n');
[T2S_uncorr, SubjectDir, filelist] = ralf_t2s_toolbox_t2smap_with_corr_octave(...
    comp1, maske, te1, dte, dx, dy, dz, gap, T2s_pars, V_save, SubjectDir, ...
    filelist, subject_name, ncd);
% set upper limit to avoid interpolation problem at coregistration &
% normalizaton
T2S_uncorr(T2S_uncorr > 150) = 150;
    
% LAST PART: Save Data
fprintf('T2*-Mapping: Saving motion corrected T2* Map\n');

filelist.condition(ncd).T2s.uncorr = ...
	fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
	sprintf('T2s_uncorr_%s.nii',subject_name));
V_save.fname = filelist.condition(ncd).T2s.uncorr;
spm_write_vol(V_save, T2S_uncorr);
        
filelist.condition(ncd).T2s.corr = ...
	fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
 	sprintf('T2s_%s.nii',subject_name));
V_save.fname = filelist.condition(ncd).T2s.corr;
spm_write_vol(V_save, T2S_corr);
      
fprintf(1,'\n#############################\n');
fprintf(1,'#  FINISHED: T2*-MAPPING\n');
fprintf(1,'#############################\n\n\n');

