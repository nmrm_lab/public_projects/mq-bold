function [cdatcomb,w0,w1,w2]=ralf_t2s_toolbox_combine_3_datasets_with_noise(cdat0,cdat1,cdat2,np1,np2,dpe,te1,dte,maske,nl)          

% [cdatcomb,w0,w1,w2]=ralf_t2s_toolbox_combine_3_datasets_with_noise(cdat0,cdat1,cdat2,np1,np2,dpe,te1,dte,maske,nl) 
% cdat0, cdat1, and cdat2 are complex 3D image data sets with identical matrix size
% The first 2 dimensions are spatial
% The 3rd dimension refers to measurements at different TE
% cdat0 was acquired with full spatial resolution
% cdat1 and cdat2 were acquired with reduced spatial resolution in PE direction
% The function extracts raw data from the 3 data sets
% Raw data are combined with individual weighting for each phase encoding (PE) step
% Weighting is chosen to suppress the effects of movement
% np1 and np2 are the numbers of available k-space lines in cdat1 and cdat2
% WARNING: np1 > np2 required! (i.e. cdat2 must have the lowest spatial resolution)
% dpe is the direction of PE (1 or 2 for first or second dimension)
% te1 and dte are the first TE and the TE increment in msec
% maske is a spatial 2D binary mask indicating where non-zero data are found in cdat0 and cdat1
% maske has the same dimensions as cdat0(:,:,1), cdat1(:,:,1) and cdat2(:,:,1)
% nl is the noise level, i.e. the level of added noise relative to the average image intensity at the lowest TE (values 0 to 1)
% recommended: use nl=0.1
% The function returns the combined complex image data set cdatcomb
% w0, w1, and w2 show the weighting of each data set for each PE step


% Get matrix size in PE direction and central point:
n0=size(cdat0,dpe);
n1=np1;
n2=np2;
cp=floor(n0/2+1);
% Get number of TE values:
nte=size(cdat0,3);
% get first/last PE line available from cdat1 and cdat2
g1l=cp-n1/2;
g1u=cp+n1/2-1;
g2l=cp-n2/2;
g2u=cp+n2/2-1;

% The following code is written under the assumption that PE is in the first direction
% if this is not the case: swap dimensions (have to be swapped back afterwards)
if dpe==2
 cdat0=permute(cdat0,[2 1 3]);
 cdat1=permute(cdat1,[2 1 3]);
 cdat2=permute(cdat2,[2 1 3]);
 maske=permute(maske,[2 1 3]);
end

% Get raw data
raw0=ifftshift(ifft2(fftshift(cdat0)));
raw1=ifftshift(ifft2(fftshift(cdat1)));
raw2=ifftshift(ifft2(fftshift(cdat2)));
% Insert raw data from lowres measurement into raw data from hires measurement:
raw01=raw0;
raw01(g1l:g1u,:,:)=raw1(g1l:g1u,:,:);
cdat01=ifftshift(fft2(fftshift(raw01)));
raw02=raw0;
raw02(g2l:g2u,:,:)=raw2(g2l:g2u,:,:);
cdat02=ifftshift(fft2(fftshift(raw02)));

cdattarg=ralf_t2s_toolbox_create_target_2datasets(cdat0,cdat01,te1,dte).*repmat(maske,[1 1 nte]);
myhelp=abs(cdattarg(:,:,1));
avint=median(myhelp(find(maske)));
cdattarg=cdattarg+nl*avint*ralf_t2s_toolbox_getnoise(size(cdattarg));
[w0,w1,cdatfit]=ralf_t2s_toolbox_fit_2datasets_to_target_fast(cdat0,cdat1,np1,cdattarg);
rawn1=raw0;
rawn2=raw01;
for i=g1l:g1u
 if w1(i)>1.5*w0(i)
  rawn1(i,:,:)=raw01(i,:,:);
 end
 if w0(i)>1.5*w1(i)
  rawn2(i,:,:)=raw0(i,:,:);
 end
end
cdat0n1=ifftshift(fft2(fftshift(rawn1))).*repmat(maske,[1 1 nte]);
cdat01n1=ifftshift(fft2(fftshift(rawn2))).*repmat(maske,[1 1 nte]);

cdattarg=ralf_t2s_toolbox_create_target_2datasets(cdat0,cdat02,te1,dte).*repmat(maske,[1 1 nte]);
myhelp=abs(cdattarg(:,:,1));
avint=median(myhelp(find(maske)));
cdattarg=cdattarg+nl*avint*ralf_t2s_toolbox_getnoise(size(cdattarg));
[w0,w1,cdatfit]=ralf_t2s_toolbox_fit_2datasets_to_target_fast(cdat0,cdat2,np1,cdattarg);
rawn1=raw0;
rawn2=raw02;
for i=g2l:g2u
 if w1(i)>1.5*w0(i)
  rawn1(i,:,:)=raw02(i,:,:);
 end
 if w0(i)>1.5*w1(i)
  rawn2(i,:,:)=raw0(i,:,:);
 end
end
cdat0n2=ifftshift(fft2(fftshift(rawn1))).*repmat(maske,[1 1 nte]);
cdat02n1=ifftshift(fft2(fftshift(rawn2))).*repmat(maske,[1 1 nte]);

cdattarg=ralf_t2s_toolbox_create_target_2datasets(cdat01,cdat02,te1,dte).*repmat(maske,[1 1 nte]);
myhelp=abs(cdattarg(:,:,1));
avint=median(myhelp(find(maske)));
cdattarg=cdattarg+nl*avint*ralf_t2s_toolbox_getnoise(size(cdattarg));
[w0,w1,cdatfit]=ralf_t2s_toolbox_fit_2datasets_to_target_fast(cdat1,cdat2,np1,cdattarg);
rawn1=raw01;
rawn2=raw02;
for i=g2l:g2u
 if w1(i)>1.5*w0(i)
  rawn1(i,:,:)=raw02(i,:,:);
 end
 if w0(i)>1.5*w1(i)
  rawn2(i,:,:)=raw01(i,:,:);
 end
end
cdat01n2=ifftshift(fft2(fftshift(rawn1))).*repmat(maske,[1 1 nte]);
cdat02n2=ifftshift(fft2(fftshift(rawn2))).*repmat(maske,[1 1 nte]);

cdat0n=ralf_t2s_toolbox_get_optimum_dataset(cdat0n1,cdat0n2,1,te1,dte,maske);
cdat01n=ralf_t2s_toolbox_get_optimum_dataset(cdat01n1,cdat01n2,1,te1,dte,maske);
cdat02n=ralf_t2s_toolbox_get_optimum_dataset(cdat02n1,cdat02n2,1,te1,dte,maske);

cdattarg=ralf_t2s_toolbox_create_target_3datasets(cdat0n,cdat01n,cdat02n,te1,dte).*repmat(maske,[1 1 nte]);
myhelp=abs(cdattarg(:,:,1));
avint=median(myhelp(find(maske)));
cdattarg=cdattarg+nl*avint*ralf_t2s_toolbox_getnoise(size(cdattarg));
[w0,w1,w2,cdatcomb]=ralf_t2s_toolbox_fit_3datasets_to_target_fast(cdat0,cdat1,cdat2,np1,np2,cdattarg);

% Swapping back:
if dpe==2
 cdatcomb=permute(cdatcomb,[2 1 3]);
end


