function [amap,t2smap,serie_fitted,rmap]=my_t2s_fit_corr(te1,dte,serie)

% [amap,t2smap,serie_fitted,rmap]=my_t2s_fit_corr(te1,dte,serie)
% function for fast T2* fitting
% te1 is the first echo time in msec
% dte is the echo time increment in msec
% serie contains the modulus data
% serie must have 3 or 4 dimensions
% the last dimension corresponds to the different time points
% amap and t2smap are the spin density and T2* map (in msec)
% serie_fitted is the fitted data set
% rmap is the map of empirical correlation coefficients 
% between original and fitted data

dim=ndims(serie);
nte=size(serie,dim);
maxe=1-1e-6;

if dim==4
 zaehler=sum(serie(:,:,:,1:end-1).*serie(:,:,:,2:end),4);
 nenner=sum(serie(:,:,:,1:end-1).*serie(:,:,:,1:end-1),4);
 emap=zaehler./(eps+nenner);
 %emap=emap.*(emap>0 & emap<1);
 emap=emap.*(emap>0 & emap<maxe)+maxe*(emap>=maxe);
 t2smap=-dte./(eps+log(eps+emap));
 pmap=cumprod(repmat(emap,[1 1 1 nte]),4);
 zaehler=sum(serie.*pmap,4);
 nenner=sum(pmap.*pmap,4);
 myhelp=zaehler./(eps+nenner);
 amap=myhelp.*(eps+emap).^(1-te1/dte);
 serie_fitted=pmap.*repmat(myhelp,[1 1 1 nte]);
 zaehler=mean(serie.*serie_fitted,4)-mean(serie,4).*mean(serie_fitted,4);
 %nenner1=sqrt(mean(serie.^2,4)-(mean(serie,4)).^2);
 %nenner2=sqrt(mean(serie_fitted.^2,4)-(mean(serie_fitted,4)).^2);
 nenner1=std(serie,1,4);
 nenner2=std(serie_fitted,1,4);
 rmap=zaehler./(eps+nenner1.*nenner2);
elseif dim==3
 zaehler=sum(serie(:,:,1:end-1).*serie(:,:,2:end),3);
 nenner=sum(serie(:,:,1:end-1).*serie(:,:,1:end-1),3);
 emap=zaehler./(eps+nenner);
 %emap=emap.*(emap>0 & emap<1);
 emap=emap.*(emap>0 & emap<maxe)+maxe*(emap>=maxe);
 t2smap=-dte./(eps+log(eps+emap));
 pmap=cumprod(repmat(emap,[1 1 nte]),3);
 zaehler=sum(serie.*pmap,3);
 nenner=sum(pmap.*pmap,3);
 myhelp=zaehler./(eps+nenner);
 amap=myhelp.*(eps+emap).^(1-te1/dte);
 serie_fitted=pmap.*repmat(myhelp,[1 1 nte]);
 zaehler=mean(serie.*serie_fitted,3)-mean(serie,3).*mean(serie_fitted,3);
 %nenner1=sqrt(mean(serie.^2,3)-(mean(serie,3)).^2);
 %nenner2=sqrt(mean(serie_fitted.^2,3)-(mean(serie_fitted,3)).^2);
 nenner1=std(serie,1,3);
 nenner2=std(serie_fitted,1,3);
 rmap=zaehler./(eps+nenner1.*nenner2);
end
