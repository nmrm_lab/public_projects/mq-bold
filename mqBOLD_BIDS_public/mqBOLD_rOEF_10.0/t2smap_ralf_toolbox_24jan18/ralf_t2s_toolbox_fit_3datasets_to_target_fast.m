function [w1,w2,w3,cdatfit]= ralf_t2s_toolbox_fit_3datasets_to_target_fast(cdat1,cdat2,cdat3,n2,n3,cdattarg)

% [w1,w2,w3,cdatfit]= ralf_t2s_toolbox_fit_3datasets_to_target_fast(cdat1,cdat2,cdat3,n2,n3,cdattarg)
% This function averages 3 data sets cdat1, cdat2, cdat3, so the average best matches the target data set cdattarg
% Weighted averaging is performed, i.e. each PE step has a different weighting w1,w2,w3 for the 3 data sets, with w1+w2+w3=1
% The function exports the values w1,w2,w3 for all PE steps and the fitted data set
% cdat1 was acquired with full spatial resolution in PE direction
% cdat2 and cdat3 were acquired with reduced spatial resolution in PE direction
% n2 and n3 are the number of PE steps that were covered in cdat2 and cdat3
% WARNING: this functions assumes that PE is in the FIRST SPATIAL DIRECTION!!
% WARNING2: n3<n2 must hold, i.e. the 3. data set must have the lowest resolution!!


% Get matrix size in PE direction and central point:
n0=size(cdat1,1);
cp=floor(n0/2+1);
% Get number of TE values:
nte=size(cdat1,3);
% get first/last PE line available for each cdat:
g2l=cp-n2/2;
g2u=cp+n2/2-1;
g3l=cp-n3/2;
g3u=cp+n3/2-1;

% Get raw data
raw1=ifftshift(ifft2(fftshift(cdat1)));
raw2=ifftshift(ifft2(fftshift(cdat2)));
raw3=ifftshift(ifft2(fftshift(cdat3)));
rawtarg=ifftshift(ifft2(fftshift(cdattarg)));

% Create "datlist"
% This list shows for each PE step how many data sets are available for averaging
datlist=ones(n0,1);
datlist(g2l:g2u)=datlist(g2l:g2u)+1;
datlist(g3l:g3u)=datlist(g3l:g3u)+1;



% Some help variables:
% differences between raw1 and rawtarget:
myprod=abs(raw1-rawtarg).^2;
% sum of squares across 2 dimensions:
cs1=sum(sum(myprod,2),3);
% Same for raw2 and raw3:
myprod=abs(raw2-rawtarg).^2;
cs2=sum(sum(myprod,2),3);
myprod=abs(raw3-rawtarg).^2;
cs3=sum(sum(myprod,2),3);
nenner1=cs1+cs2;
nenner2=cs1.*cs2+cs1.*cs3+cs2.*cs3;

% if only 1 data set available: trivial, just take it
w11=ones(n0,1);
w12=zeros(n0,1);
w13=zeros(n0,1);

 % if 2 data sets available: weighting with reciprocal of respective cs
w21=cs2./(eps+nenner1);
w22=cs1./(eps+nenner1);
w23=zeros(n0,1);

% if 3 data sets available: weighting with reciprocal of respective cs
w31=cs2.*cs3./(eps+nenner2);
w32=cs1.*cs3./(eps+nenner2);
w33=cs1.*cs2./(eps+nenner2);

w1=w11.*(datlist==1)+w21.*(datlist==2)+w31.*(datlist==3);
w2=w12.*(datlist==1)+w22.*(datlist==2)+w32.*(datlist==3);
w3=w13.*(datlist==1)+w23.*(datlist==2)+w33.*(datlist==3);


w1f=repmat(w1,[1, size(raw1,2), size(raw1,3)]);
w2f=repmat(w2,[1, size(raw1,2), size(raw1,3)]);
w3f=repmat(w3,[1, size(raw1,2), size(raw1,3)]);

% Now construct raw data with optimum weighting factors....
rawfit=w1f.*raw1+w2f.*raw2+w3f.*raw3;

% ....and transform it back into image space
cdatfit=ifftshift(fft2(fftshift(rawfit)));
