%% Automatic sorting of data (per subject) 
%  for automated qBOLD processing
% - Stephan Kaczmarz 15MAR17
% - CP 28AUG 2018
% - CP 15DEC 2019
%   - Detecting corresponding files and conditions from BIDS convention
%   - Copying files to respective output directories
%--------------------------------------------------------------------------

function [SubjectDir, filelist, flag] = sortData_T2_T2s(conditions, ...
                                    SubjectDir, filelist, T2s_pars, flag)

%determine number of conditions
nconditions = size(conditions,2);   
for ncd = 1:nconditions
    SubjectDir.condition(ncd).toplevel = fullfile(SubjectDir.output,'qBOLD',...
                                            cell2mat(conditions(ncd)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% Check for T2 multi-echo data (T2 map); If data exist: unzip and copy
% Also: Check for multiple conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%create file list
liste = dir(fullfile(SubjectDir.input,'t2map'));
files = {liste.name};   
% Iterate through all foldernames and fill structure with filenames
T2_Fctr_nii(1:nconditions+1) = 0;
T2_Fctr_json(1:nconditions+1) = 0;
for i = size(files,2) : -1 : 1
    tmp_str = string(files(i));
    if (regexpi(tmp_str, 'T2map')>0)
        if T2s_pars.T2_nconditions > 1 % multiple T2 data sets are sorted
            for ct = 1:nconditions     % according to their condition label
                if (regexpi(tmp_str,conditions(ct))>0)
                    check_folder(fullfile(SubjectDir.output,'qBOLD',...
                        conditions(ct)));
                    if (regexpi(tmp_str,'nii.gz')>0)
                        T2_Fctr_nii(ct) = T2_Fctr_nii(ct) +1;
                        filename = cell2mat(files(i));
                        filelist.condition(ct).T2.nii(T2_Fctr_nii(ct)) = ...
                            cellstr(filename(1:end-3));
                        if isfile(fullfile(SubjectDir.input,'t2map',files(i)))
                            if T2_Fctr_nii(ct) == 1
                                SubjectDir.condition(ct).T2 = ...
                                    fullfile(SubjectDir.output,'qBOLD',...
                                    cell2mat(conditions(ct)),'T2');
                                check_folder(SubjectDir.condition(ct).T2);
                                flag.condition(ct).T2 = 1;
                            end % if T2_Fctr_nii(ct) == 1
                            gunzip(fullfile(SubjectDir.input,'t2map',files(i))); 
                            [~,filename,~] = fileparts(cell2mat(files(i)));
                            movefile(fullfile(SubjectDir.input,'t2map',...
                                filename),SubjectDir.condition(ct).T2);
                            if flag.reset_origin
                                reset_origin(SubjectDir.condition(ct).T2,...
                                    filename); 
                            end
                        else
                            flag.condition(ct).T2 = 0;
                        end 
                    elseif (regexpi(tmp_str,'json')>0)
                        T2_Fctr_json(ct) = T2_Fctr_json(ct) +1;
                        filelist.condition(ct).T2.json(T2_Fctr_json(ct)) = ...
                            files(i);
                        copyfile(cell2mat(fullfile(SubjectDir.input,'t2map',...
                            files(i))), SubjectDir.condition(ct).T2); 
                    end %if (regexpi(tmp_str,'nii.gz')>0)
                end %if (regexpi(tmp_str,conditions(ct))>0)  
            end %for ct = 1:nconditions 
        elseif (T2s_pars.T2_nconditions == 1) % only one T2 data set is
                            % expected and copied to toplevel qBOLD folder
                            % -> non-condition specific T2 measurement
            check_folder(fullfile(SubjectDir.output,'qBOLD','T2'));
            if (regexpi(tmp_str,'nii.gz')>0)
                T2_Fctr_nii(nconditions+1) = T2_Fctr_nii(nconditions+1) +1;
                filename = cell2mat(files(i));
                filelist.T2.nii(T2_Fctr_nii(nconditions+1)) = ...
                                            cellstr(filename(1:end-3));
                if isfile(fullfile(SubjectDir.input,'t2map',files(i)))
                  	if T2_Fctr_nii(nconditions+1) == 1
                    	SubjectDir.T2 = ...
                        	fullfile(SubjectDir.output,'qBOLD','T2');
                       	check_folder(SubjectDir.T2);
                       	flag.T2 = 1;
                    end % if T2_Fctr_nii(nconditions+1) == 1
                   	gunzip(fullfile(SubjectDir.input,'t2map',files(i))); 
                    [~,filename,~] = fileparts(cell2mat(files(i)));
                  	movefile(fullfile(SubjectDir.input,'t2map',filename),...
                    	SubjectDir.T2);  
                    if flag.reset_origin
                        reset_origin(SubjectDir.T2,filename); 
                  	end
                else
                   	flag.T2 = 0;
                end 
            elseif (regexpi(tmp_str,'json')>0)
            	T2_Fctr_json(nconditions+1) = T2_Fctr_json(nconditions+1) +1;
              	filelist.T2.json(T2_Fctr_json(nconditions+1)) = files(i);
             	copyfile(cell2mat(fullfile(SubjectDir.input,'t2map',...
                        files(i))), SubjectDir.T2); 
          	end %if (regexpi(tmp_str,'nii.gz')>0)               
        end  %if T2s_pars.T2_nconditions > 1 
    end % (regexpi(tmp_str, 'T2map')>0)
end %for i = size(files,2) : -1 : 1


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% Check for T2* multi-echo data (T2star); If data exist: unzip and copy
% Also: Check for 
%       - multiple conditions 
%       - fullres / MoCoHR / MoCoQR
%       - mangitude (no tag!!!) / real / imaginary / phase
%       - echo-01 ... echo-12
% --> Copy data to sorted folder structure in output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%create file list
liste = dir(fullfile(SubjectDir.input,'t2star'));
files = {liste.name};   
% Iterate through all foldernames and fill structure with filenames
% Initialize file counter ...
T2sFR_Re_Fctr_nii(1:nconditions) = 0;     %Real, fullres (Philips)
T2sFR_Re_Fctr_json(1:nconditions) = 0;    %Real, fullres (Philips)
T2sFR_Im_Fctr_nii(1:nconditions) = 0;     %Imaginary, fullres (Philips)
T2sFR_Im_Fctr_json(1:nconditions) = 0;    %Imaginary, fullres (Philips)
T2sFR_Ph_Fctr_nii(1:nconditions) = 0;     %Phase, fullres (Siemens)
T2sFR_Ph_Fctr_json(1:nconditions) = 0;    %Phase, fullres (Siemens)
T2sFR_Magn_Fctr_nii(1:nconditions) = 0;   %Magnitude, fullres
T2sFR_Magn_Fctr_json(1:nconditions) = 0;  %Magnitude, fullres

T2sHR_Re_Fctr_nii(1:nconditions) = 0;     %Real, halfres (Philips)
T2sHR_Re_Fctr_json(1:nconditions) = 0;    %Real, halfres (Philips)
T2sHR_Im_Fctr_nii(1:nconditions) = 0;     %Imaginary, halfres (Philips)
T2sHR_Im_Fctr_json(1:nconditions) = 0;    %Imaginary, halfres (Philips)
T2sHR_Ph_Fctr_nii(1:nconditions) = 0;     %Imaginary, halfres (Siemens)
T2sHR_Ph_Fctr_json(1:nconditions) = 0;    %Imaginary, halfres (Siemens)
T2sHR_Magn_Fctr_nii(1:nconditions) = 0;   %Magnitude, halfres 
T2sHR_Magn_Fctr_json(1:nconditions) = 0;  %Magnitude, halfres 

T2sQR_Re_Fctr_nii(1:nconditions) = 0;     %Real, QR (Philips)
T2sQR_Re_Fctr_json(1:nconditions) = 0;    %Real, QR (Philips)
T2sQR_Im_Fctr_nii(1:nconditions) = 0;     %Imaginary, QR (Philips)
T2sQR_Im_Fctr_json(1:nconditions) = 0;    %Imaginary, QR (Philips)
T2sQR_Ph_Fctr_nii(1:nconditions) = 0;     %Imaginary, QR (Siemens)
T2sQR_Ph_Fctr_json(1:nconditions) = 0;    %Imaginary, QR (Siemens)
T2sQR_Magn_Fctr_nii(1:nconditions) = 0;   %Magnitude, quarter resolution
T2sQR_Magn_Fctr_json(1:nconditions) = 0;  %Magnitude, quarter resolution
% Initialize flags
for ct = 1:nconditions
    flag.condition(ct).T2s.FR.Magn = 0;
    flag.condition(ct).T2s.FR.Re = 0;
    flag.condition(ct).T2s.FR.Im = 0;
    flag.condition(ct).T2s.FR.Ph = 0;
    flag.condition(ct).T2s.HR.Magn = 0;
    flag.condition(ct).T2s.HR.Re = 0;
    flag.condition(ct).T2s.HR.Im = 0;
    flag.condition(ct).T2s.HR.Ph = 0;
    flag.condition(ct).T2s.QR.Magn = 0;
    flag.condition(ct).T2s.QR.Re = 0;
    flag.condition(ct).T2s.QR.Im = 0;
    flag.condition(ct).T2s.QR.Ph = 0;
end %for ct = 1:nconditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem: magn json files do not feature a unique identifier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = size(files,2) : -1 : 1
    tmp_str = string(files(i));
    if (regexpi(tmp_str, 'T2star')>0)    
        for ct = 1:nconditions
            SubjectDir.condition(ct).T2s.toplevel = ...
                fullfile(SubjectDir.output,'qBOLD', cell2mat(conditions(ct)),...
                        'T2S');
            check_folder(SubjectDir.condition(ct).T2s.toplevel);
            if (regexpi(tmp_str,conditions(ct))>0)
                check_folder(fullfile(SubjectDir.output,'qBOLD',conditions(ct)));
                if (regexpi(tmp_str,'QR')>0) % quarter resolution data
                    check_folder(fullfile(SubjectDir.output,'qBOLD',...
                    	conditions(ct),'T2S','QR'));
                    if (regexpi(tmp_str,'imaginary')>0) % QR imaginary data
                        check_folder(fullfile(SubjectDir.output,'qBOLD',...
                        	conditions(ct),'T2S','QR', 'Im')); 
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sQR_Im_Fctr_nii(ct) = T2sQR_Im_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.QR.Im.nii(T2sQR_Im_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sQR_Im_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.QR.Im = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                    	cell2mat(conditions(ct)),'T2S','QR','Im');
                                    check_folder(SubjectDir.condition(ct).T2s.QR.Im);                                            
                                    flag.condition(ct).T2s.QR.Im = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i))); 
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename),SubjectDir.condition(ct).T2s.QR.Im); 
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.QR.Im,...
                                        filename); 
                                end     
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sQR_Im_Fctr_json(ct) = T2sQR_Im_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.QR.Im.json(T2sQR_Im_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.QR.Im);
                        end %if (regexpi(tmp_str,'nii.gz')>0)
                    elseif (regexpi(tmp_str,'real')>0) % QR real data                                    
                        check_folder(fullfile(SubjectDir.output,'qBOLD',...
                        	conditions(ct),'T2S','QR','Re')); 
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sQR_Re_Fctr_nii(ct) = T2sQR_Re_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.QR.Re.nii(T2sQR_Re_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sQR_Re_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.QR.Re = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                    	cell2mat(conditions(ct)),'T2S','QR','Re');
                                    check_folder(SubjectDir.condition(ct).T2s.QR.Re);
                                    flag.condition(ct).T2s.QR.Re = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i))); 
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename),SubjectDir.condition(ct).T2s.QR.Re); 
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.QR.Re,...
                                        filename); 
                                end                                       
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sQR_Re_Fctr_json(ct) = T2sQR_Re_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.QR.Re.json(T2sQR_Re_Fctr_json(ct)) = ...
                                                                        files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.QR.Re);
                        end %if (regexpi(tmp_str,'nii.gz')>0)   
                    elseif (regexpi(tmp_str,'_ph_')>0) % QR phase data                                    
                        check_folder(fullfile(SubjectDir.output,'qBOLD',...
                        	conditions(ct),'T2S','QR','Ph')); 
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sQR_Ph_Fctr_nii(ct) = T2sQR_Ph_Fctr_nii(ct) + 1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.QR.Ph.nii(T2sQR_Ph_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sQR_Ph_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.QR.Ph = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                    	cell2mat(conditions(ct)),'T2S','QR','Ph');
                                    check_folder(SubjectDir.condition(ct).T2s.QR.Ph);
                                    flag.condition(ct).T2s.QR.Ph = 1;
                                end % if T2sQR_Ph_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i)));
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename),SubjectDir.condition(ct).T2s.QR.Ph); 
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.QR.Ph,...
                                        filename); 
                                end                                
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sQR_Ph_Fctr_json(ct) = T2sQR_Ph_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.QR.Ph.json(T2sQR_Ph_Fctr_json(ct)) = ...
                                                                        files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.QR.Ph);
                        end %if (regexpi(tmp_str,'nii.gz')>0)     
                    else % QR magnitude data
                        check_folder(fullfile(SubjectDir.output,'qBOLD',...
                                     conditions(ct),'T2S','QR','Magn'));
                        if (regexpi(tmp_str,'nii.gz')>0)                        
                            T2sQR_Magn_Fctr_nii(ct) = T2sQR_Magn_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.QR.Magn.nii(T2sQR_Magn_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sQR_Magn_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.QR.Magn = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                       	cell2mat(conditions(ct)),'T2S','QR','Magn');
                                    check_folder(SubjectDir.condition(ct).T2s.QR.Magn);               
                                    flag.condition(ct).T2s.QR.Magn = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i)));
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename), SubjectDir.condition(ct).T2s.QR.Magn);  
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.QR.Magn,...
                                        filename); 
                                end                                 
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sQR_Magn_Fctr_json(ct) = T2sQR_Magn_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.QR.Magn.json(T2sQR_Magn_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.QR.Magn);
                        end %if (regexpi(tmp_str,'nii.gz')>0)                        
                    end %if (regexpi(tmp_str,'imaginary')>0) % imaginary data
                elseif (regexpi(tmp_str,'HR')>0) % half resolution data
                    check_folder(fullfile(SubjectDir.output,'qBOLD',...
                    	conditions(ct),'T2S','HR'));
                    if (regexpi(tmp_str,'imaginary')>0) % HR imaginary data
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sHR_Im_Fctr_nii(ct) = T2sHR_Im_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.HR.Im.nii(T2sHR_Im_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sHR_Im_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.HR.Im = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                      	cell2mat(conditions(ct)),'T2S','HR','Im');
                                    check_folder(SubjectDir.condition(ct).T2s.HR.Im);                 
                                    flag.condition(ct).T2s.HR.Im = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i)));
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename),SubjectDir.condition(ct).T2s.HR.Im); 
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.HR.Im,...
                                        filename); 
                                end  
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sHR_Im_Fctr_json(ct) = T2sHR_Im_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.HR.Im.json(T2sHR_Im_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.HR.Im);
                        end %if (regexpi(tmp_str,'nii.gz')>0)                         
                    elseif (regexpi(tmp_str,'real')>0) % HR real data
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sHR_Re_Fctr_nii(ct) = T2sHR_Re_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.HR.Re.nii(T2sHR_Re_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sHR_Re_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.HR.Re = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                    	cell2mat(conditions(ct)),'T2S','HR','Re');                                 
                                    check_folder(SubjectDir.condition(ct).T2s.HR.Re);                 
                                    flag.condition(ct).T2s.HR.Re = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i)));
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename), SubjectDir.condition(ct).T2s.HR.Re);  
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.HR.Re,...
                                        filename); 
                                end
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sHR_Re_Fctr_json(ct) = T2sHR_Re_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.HR.Re.json(T2sHR_Re_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.HR.Re);
                        end %if (regexpi(tmp_str,'nii.gz')>0)
                    elseif (regexpi(tmp_str,'_ph_')>0) % HR phase data
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sHR_Ph_Fctr_nii(ct) = T2sHR_Ph_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.HR.Ph.nii(T2sHR_Ph_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sHR_Ph_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.HR.Ph = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                    	cell2mat(conditions(ct)),'T2S','HR','Ph');                                 
                                    check_folder(SubjectDir.condition(ct).T2s.HR.Ph);                 
                                    flag.condition(ct).T2s.HR.Ph = 1;
                                end % if T2sHR_Ph_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i)));
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename), SubjectDir.condition(ct).T2s.HR.Ph);   
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.HR.Ph,...
                                        filename); 
                                end
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sHR_Ph_Fctr_json(ct) = T2sHR_Ph_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.HR.Ph.json(T2sHR_Ph_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.HR.Ph);
                        end %if (regexpi(tmp_str,'nii.gz')>0)     
                    else % HR magnitude data
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sHR_Magn_Fctr_nii(ct) = T2sHR_Magn_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.HR.Magn.nii(T2sHR_Magn_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sHR_Magn_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.HR.Magn = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                    	cell2mat(conditions(ct)),'T2S','HR','Magn');
                                    check_folder(SubjectDir.condition(ct).T2s.HR.Magn);                 
                                    flag.condition(ct).T2s.HR.Magn = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i))); 
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename),SubjectDir.condition(ct).T2s.HR.Magn);
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.HR.Magn,...
                                        filename); 
                                end
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sHR_Magn_Fctr_json(ct) = T2sHR_Magn_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.HR.Magn.json(T2sHR_Magn_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))), SubjectDir.condition(ct).T2s.HR.Magn);                                      
                        end %if (regexpi(tmp_str,'nii.gz')>0)  
                    end %if (regexpi(tmp_str,'imaginary')>0) % HR imaginary data
                else %fullres
                   	check_folder(fullfile(SubjectDir.output,'qBOLD',...
                    	conditions(ct),'T2S','FR'));
                    if (regexpi(tmp_str,'imaginary')>0) % fullres imaginary data
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sFR_Im_Fctr_nii(ct) = T2sFR_Im_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.FR.Im.nii(T2sFR_Im_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sFR_Im_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.FR.Im = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                    	cell2mat(conditions(ct)),'T2S','FR','Im');                          
                                    check_folder(SubjectDir.condition(ct).T2s.FR.Im );                
                                    flag.condition(ct).T2s.FR.Im = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i))); 
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename),SubjectDir.condition(ct).T2s.FR.Im);
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.FR.Im,...
                                        filename); 
                                end
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sFR_Im_Fctr_json(ct) = T2sFR_Im_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.FR.Im.json(T2sFR_Im_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.FR.Im);
                        end %if (regexpi(tmp_str,'nii.gz')>0) 
                    elseif (regexpi(tmp_str,'real')>0) % fullres real data
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sFR_Re_Fctr_nii(ct) = T2sFR_Re_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.FR.Re.nii(T2sFR_Re_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sFR_Re_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.FR.Re = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                      	cell2mat(conditions(ct)),'T2S','FR','Re');
                                    check_folder(SubjectDir.condition(ct).T2s.FR.Re);                
                                    flag.condition(ct).T2s.FR.Re = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i))); 
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename),SubjectDir.condition(ct).T2s.FR.Re); 
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.FR.Re,...
                                        filename); 
                                end
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sFR_Re_Fctr_json(ct) = T2sFR_Re_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.FR.Re.json(T2sFR_Re_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.FR.Re);
                        end %if (regexpi(tmp_str,'nii.gz')>0)                         
                    elseif (regexpi(tmp_str,'ph')>0) % fullres phase data
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sFR_Ph_Fctr_nii(ct) = T2sFR_Ph_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.FR.Ph.nii(T2sFR_Ph_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sFR_Ph_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.FR.Ph = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                      	cell2mat(conditions(ct)),'T2S','FR','Ph');
                                    check_folder(SubjectDir.condition(ct).T2s.FR.Ph);                
                                    flag.condition(ct).T2s.FR.Ph = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i))); 
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename),SubjectDir.condition(ct).T2s.FR.Ph); 
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.FR.Ph,...
                                        filename); 
                                end
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sFR_Ph_Fctr_json(ct) = T2sFR_Ph_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.FR.Ph.json(T2sFR_Ph_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.FR.Ph);
                        end %if (regexpi(tmp_str,'nii.gz')>0) 
                    else % fullres magnitude data
                        if (regexpi(tmp_str,'nii.gz')>0)
                            T2sFR_Magn_Fctr_nii(ct) = T2sFR_Magn_Fctr_nii(ct) +1;
                            filename = cell2mat(files(i));
                            filelist.condition(ct).T2s.FR.Magn.nii(T2sFR_Magn_Fctr_nii(ct)) = ...
                            	cellstr(filename(1:end-3));
                            if isfile(fullfile(SubjectDir.input,'t2star',files(i)))
                                if T2sFR_Magn_Fctr_nii(ct) == 1
                                    SubjectDir.condition(ct).T2s.FR.Magn = ...
                                    	fullfile(SubjectDir.output,'qBOLD',...
                                    	cell2mat(conditions(ct)),'T2S','FR','Magn');
                                    check_folder(SubjectDir.condition(ct).T2s.FR.Magn); 
                                    flag.condition(ct).T2s.FR.Magn = 1;
                                end % if T2_Fctr_nii(ct) == 1
                                gunzip(fullfile(SubjectDir.input,'t2star',files(i))); 
                                [~,filename,~] = fileparts(cell2mat(files(i)));
                                movefile(fullfile(SubjectDir.input,'t2star',...
                                    filename),SubjectDir.condition(ct).T2s.FR.Magn); 
                                if flag.reset_origin
                                    reset_origin(SubjectDir.condition(ct).T2s.FR.Magn,...
                                        filename); 
                                end
                            end 
                        elseif (regexpi(tmp_str,'json')>0)
                            T2sFR_Magn_Fctr_json(ct) = T2sFR_Magn_Fctr_json(ct) +1;
                            filelist.condition(ct).T2s.FR.Magn.json(T2sFR_Magn_Fctr_json(ct)) = ...
                            	files(i);
                            copyfile(cell2mat(fullfile(SubjectDir.input,'t2star',...
                            	files(i))),SubjectDir.condition(ct).T2s.FR.Magn);                                                                 
                        end %if (regexpi(tmp_str,'nii.gz')>0) 
                    end %(regexpi(tmp_str,'imaginary')>0) % fullres imaginary data
                end %if (regexpi(tmp_str,'QR')>0)
            end %if (regexpi(tmp_str,conditions(ct))>0)
        end %for ct = 1:nconditions
    end %if (regexpi(tmp_str, 't2star')>0)  
end %for i = size(files,2) : -1 : 1
    
end