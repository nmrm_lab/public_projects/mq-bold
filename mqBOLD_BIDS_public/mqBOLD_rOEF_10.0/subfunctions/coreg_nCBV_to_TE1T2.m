function filelist = coreg_nCBV_to_TE1T2(SubjectDir, filelist, subject_name, ...
                                     conditions, nconditions, T2s_pars, flag)
	global defaults;
	spm_get_defaults;
	flags = defaults.coreg;

	% Flags to pass to routine (spm_reslice) to create resliced images
    resFlags = struct(...
    	'interp', 1,...%flags.write.interp,... % 1: trilinear interpolation
    	'wrap', flags.write.wrap,...           % wrapping info (ignore...)
      	'mask', flags.write.mask,...           % masking (see spm_reslice)
      	'which',1,...                          % write reslice time series for 
      	'mean',0);                             % later use do not write 
    %----------------------------------------------------------------------   
	% PG - Tar(G)et image, NEVER CHANGED
	% PF - Source image, transformed to match PG
	% PO - (O)ther images, already realigned to PF and transformed with PF
    %----------------------------------------------------------------------
	% T2 directory of 1st condition or single T2 contains reference (target)
    %----------------------------------------------------------------------
    if T2s_pars.T2_nconditions > 1 % coregister all T2 maps for different
                                   % conditions to TE1 of condition 1
        T2_reference = SubjectDir.condition(1).T2;
    else % only one T2 map exists in toplevel qBOLD folder that is used as 
         % as target for coregistration and R2' calculation
        T2_reference = SubjectDir.T2;
    end    
    
	% TARGET: T2, first echo
	PG = spm_select('FPList', T2_reference, '^TE1.*.nii$');
    if isempty(PG)
        error('No Target (T2, TE1) selected. Check files or file names!');
    end      
	PG = PG(1,:);
	VG = spm_vol(PG);
 

if strcmp(flag.DSC,'singleDSC') % CAVE: only one DSC expected 
	%----------------------------------------------------------------------
	% only one CBV map exists in toplevel qBOLD/DSC/CBV folders
	%----------------------------------------------------------------------
    % SOURCE: FLAIR data coregistered to nCBV 
	PF = spm_select('FPList', SubjectDir.DSC.FLAIR, '^rsub.*.nii$');  
	if isempty(PF)
     	error('No Source (coregistered FLAIR) selected. Check files or file names!');
	end
    PF = PF(1,:);
 	VF = spm_vol(PF);

    % select "other images" for coregistration (FLAIR segments)
	PO1 = spm_select( 'FPList', SubjectDir.DSC.FLAIR, '^rc.*.nii$');
    if isempty(PO1)
    	error('No tissue segments selected. Check files or file names!');
    end
    
    % select nCBV maps for coregistration
	PO2 = spm_select( 'FPList', SubjectDir.DSC.CBV, '^nCBV.*.nii$');
    if isempty(PO2)
    	error('No nCBV maps selected. Check files or file names!');
    end

    % select "other images" for coregistration (T1w segments)
	PO3 = spm_select( 'FPList', SubjectDir.DSC.T1w, '^r.*.nii$');
    if isempty(PO1)
    	error('No T1w tissue segments selected. Check files or file names!');
    end 
    
	if isempty(PO1) && isempty(PO2) && isempty(PO3)
      	PO = PF;
	else
    	PO = char(PF,PO1,PO2,PO3);
	end

	% do coregistration, i.e. get coregistration parameters
	x  = spm_coreg( VG, VF, flags.estimate);

	% get the transformation to be applied with parameters 'x'
	M  = inv( spm_matrix(x));

	% in MM we put the transformations for the 'other' images
	MM = zeros( 4, 4, size( PO,1));

	for j=1:size( PO,1)
        % get the transformation matrix for every image
      	MM(:,:,j) = spm_get_space( deblank( PO(j,:)));
    end

	for j=1:size( PO,1)
        % write the transformation applied to every image
      	% filename: deblank(PO(j,:))
      	spm_get_space( deblank( PO(j,:)), M*MM(:,:,j));
	end

 	P = char( PG, PO);
 	spm_reslice( P,resFlags);   
    
    % Copy coregistered nCBV, FLAIR and T1w data to toplevel condition dirs
    fprintf('\nCopying %s''s coregistered nCBV, FLAIR and T1w to toplevel condition dir',...
        subject_name);
    for ncd = 1:nconditions 
        filelist.condition(ncd).CBV = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'CBV.nii');
        sourcefile = fullfile(SubjectDir.DSC.CBV, 'rnCBV_corr_auc_full.nii');
        copyfile(sourcefile, filelist.condition(ncd).CBV);
        
        filelist.condition(ncd).FLAIR = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'FLAIR.nii');
        sourcefile = fullfile(SubjectDir.DSC.FLAIR, ...
                        sprintf('rr%s_FLAIR.nii',subject_name));
        copyfile(sourcefile, filelist.condition(ncd).FLAIR);
        
        filelist.condition(ncd).T1w = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'T1w.nii');
        sourcefile = fullfile(SubjectDir.DSC.T1w, ...
                        sprintf('rr%s_T1w.nii',subject_name));
        copyfile(sourcefile, filelist.condition(ncd).T1w);
        
        filelist.condition(ncd).GM = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'GM.nii');
        sourcefile = fullfile(SubjectDir.DSC.T1w, ...
                        sprintf('rrc1%s_T1w.nii',subject_name));
        copyfile(sourcefile, filelist.condition(ncd).GM);
        
        filelist.condition(ncd).WM = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'WM.nii');
        sourcefile = fullfile(SubjectDir.DSC.T1w, ...
                        sprintf('rrc2%s_T1w.nii',subject_name));
        copyfile(sourcefile, filelist.condition(ncd).WM);
       
        filelist.condition(ncd).BrMsk_CSF = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'BrMsk_CSF.nii');
        sourcefile = fullfile(SubjectDir.DSC.T1w, 'rrcBrMsk_CSF.nii');
        copyfile(sourcefile, filelist.condition(ncd).BrMsk_CSF);
    end % for ncd = 1:nconditions 
    
elseif strcmp(flag.DSC,'conditionDSC')
	%----------------------------------------------------------------------
	% CBV maps exist in each condition's DSC/CBV folders
	%----------------------------------------------------------------------
	fprintf('\n Now coregistering %s''s all conditions nCBV maps to TE1 of reverence T2.\n\n',...
                                                            subject_name); 
	for ncd = 1:nconditions % for each subject
        % SOURCE: FLAIR data coregistered to nCBV 
        PF = spm_select('FPList', SubjectDir.condition(ncd).DSC.FLAIR,...
            '^rsub.*.nii$');  
        if isempty(PF)
            error('No Source (coregistered FLAIR) selected. Check files or file names!');
        end
        PF = PF(1,:);
        VF = spm_vol(PF);

        % select "other images" for coregistration (FLAIR segments)
        PO1 = spm_select( 'FPList', SubjectDir.condition(ncd).DSC.FLAIR,...
            '^rc.*.nii$');
        if isempty(PO1)
            error('No FLAIR tissue segments selected. Check files or file names!');
        end

        % select nCBV maps for coregistration
        PO2 = spm_select( 'FPList', SubjectDir.condition(ncd).DSC.CBV,...
            '^nCBV.*.nii$');
        if isempty(PO2)
            error('No nCBV maps selected. Check files or file names!');
        end
        
        % select "other images" for coregistration (T1w segments)
        PO3 = spm_select( 'FPList', SubjectDir.condition(ncd).DSC.T1w,...
            '^r.*.nii$');
        if isempty(PO1)
            error('No T1w tissue segments selected. Check files or file names!');
        end 

        if isempty(PO1) && isempty(PO2) && isempty(PO3)
            PO = PF;
        else
            PO = char(PF,PO1,PO2,PO3);
        end

        % do coregistration, i.e. get coregistration parameters
        x  = spm_coreg( VG, VF, flags.estimate);

        % get the transformation to be applied with parameters 'x'
        M  = inv( spm_matrix(x));

        % in MM we put the transformations for the 'other' images
        MM = zeros( 4, 4, size( PO,1));

        for j=1:size( PO,1)
            % get the transformation matrix for every image
            MM(:,:,j) = spm_get_space( deblank( PO(j,:)));
        end

        for j=1:size( PO,1)
            % write the transformation applied to every image
            % filename: deblank(PO(j,:))
            spm_get_space( deblank( PO(j,:)), M*MM(:,:,j));
        end

        P = char( PG, PO);
        spm_reslice( P,resFlags);    
        
        % Copy coregistered nCBV, FLAIR and T1w data to toplevel condition dirs
        fprintf('\nCopying %s''s coregistered nCBV, FLAIR and T1w to toplevel %s condition dir',...
            subject_name, conditions(ncd));    
        
        sourcefile = fullfile(SubjectDir.condition(ncd).DSC.CBV, ...
            'rnCBV_corr_auc_full.nii');
        filelist.condition(ncd).CBV = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'CBV.nii');       
        copyfile(sourcefile, filelist.condition(ncd).CBV);
        
        sourcefile = fullfile(SubjectDir.condition(ncd).DSC.FLAIR, ...
                        sprintf('rr%s_FLAIR.nii',subject_name));
        filelist.condition(ncd).FLAIR = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'FLAIR.nii');   
        copyfile(sourcefile, filelist.condition(ncd).FLAIR);
        
        sourcefile = fullfile(SubjectDir.condition(ncd).DSC.T1w, ...
                        sprintf('rr%s_T1w.nii',subject_name));
        filelist.condition(ncd).T1w = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'T1w.nii');        
        copyfile(sourcefile, filelist.condition(ncd).T1w);
        
        sourcefile = fullfile(SubjectDir.condition(ncd).DSC.T1w, ...
                        sprintf('rrc1%s_T1w.nii',subject_name));
        filelist.condition(ncd).GM = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'GM.nii');       
        copyfile(sourcefile, filelist.condition(ncd).GM);
        
        filelist.condition(ncd).WM = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'WM.nii');
        sourcefile = fullfile(SubjectDir.condition(ncd).DSC.T1w, ...
                        sprintf('rrc2%s_T1w.nii',subject_name));
        copyfile(sourcefile, filelist.condition(ncd).WM);
       
        sourcefile = fullfile(SubjectDir.condition(ncd).DSC.T1w, ...
            'rrcBrMsk_CSF.nii');
        filelist.condition(ncd).BrMsk_CSF = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'BrMsk_CSF.nii');       
        copyfile(sourcefile, filelist.condition(ncd).BrMsk_CSF);
	end % for ncd = 1:nconditions
end % if strcmp(flag.DSC,'singleDSC')                                   
end

