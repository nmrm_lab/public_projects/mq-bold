%% Coreg ASL perfusion based fMRI data for both labels
% From Stephan Kaczmarz 20OCT15

%% Required subscripts

% - SPM12


%% Modifications


%% Main program

% ------------------------------------------------------------------------
% INITIALIZE
% ------------------------------------------------------------------------
function coreg_pCASL_volumes(SubjectDir, subject_name, nconditions)

    % ---------------------------------------------------------------------
    % COREGISTRATION
    %----------------------------------------------------------------------
    % Print status
    fprintf('Started coreg for pCASL vols 1 and 2 for all conditions of %s\n',...
        subject_name);

    % Load defaults
    global defaults;
    spm_defaults;
    flags = defaults.coreg;

    % Flags to pass to routine to create resliced images
    % (spm_reslice)
    resFlags = struct(...
        'interp', flags.write.interp,...       % 1: trilinear interpolation
        'wrap', flags.write.wrap,...           % wrapping info (ignore...)
        'mask', flags.write.mask,...           % masking (see spm_reslice)
        'which',1,...                          % write reslice time series 
        'mean',0,...                           % do not write mean image
        'prefix', 'r');                        % use prefix r


	% ---------------------------------------------------------------------
	% Set source and target
	% ---------------------------------------------------------------------
    % PG - Tar(G)et image, NEVER CHANGED
	% PF - Source image, transformed to match PG
	% PO - (O)ther images, originally realigned to PF and transformed with PF

	% TARGET: Mean pCASL vol 1
    ncd = 1;
	PG = spm_select('FPList',SubjectDir.condition(ncd).pCASL.toplevel,...
        '^meano.*\_1.nii$');
    ncd = ncd+1;
	while isempty(PG) && ncd <= nconditions % look for mean in other conditions
        PG = spm_select('FPList',SubjectDir.condition(2).pCASL.toplevel,...
            '^meano.*\_1.nii$');
        ncd = ncd+1;
	end 
    
  	%SOURCE: Mean pCASL vol 2
    ncd = 1;
  	PF = spm_select('FPList',SubjectDir.condition(ncd).pCASL.toplevel,...
        '^meano.*\_2.nii$');
    ncd = ncd+1;
	while isempty(PG) && ncd < nconditions % look for mean in other conditions
        PF = spm_select('FPList',SubjectDir.condition(ncd).pCASL.toplevel,...
            '^meano.*\_2.nii$');
        ncd = ncd+1;
	end

    % Load volumes
    VG = spm_vol(PG);
    VF = spm_vol(PF);
    
    %Other images: realigned vol 2 time series images for all conditions
	for ncd=1:nconditions
        if ncd == 1
             PO = spm_select('ExtFPList',...
                 SubjectDir.condition(ncd).pCASL.toplevel,'^ro.*_2.nii$');
        else
            Ptmp = spm_select('ExtFPList',...
                SubjectDir.condition(ncd).pCASL.toplevel, '^ro.*_2.nii$');
            PO = char(PO, Ptmp);
        end       
	end % for ncd =1:nconditions    

 	%PO = PF; --> this if there are no 'other' images
	if isempty(PO) 
        PO=PF;
    else
      	PO = char(PF,PO);
    end

	% ---------------------------------------------------------------------
	% Get coregistration parameters
	% ---------------------------------------------------------------------

	% This method from spm_coreg_ui.m
	% Get coregistration parameters
	x  = spm_coreg(VG, VF,flags.estimate);

	% Get the transformation to be applied with parameters 'x'
	M  = inv(spm_matrix(x));

	% Transform the mean image
	% spm_get_space(deblank(PG),M);
	% In MM we put the transformations for the 'other' images
	MM = zeros(4,4,size(PO,1));
	for j=1:size(PO,1),
        %get the transformation matrix for every image
        MM(:,:,j) = spm_get_space(deblank(PO(j,:)));
	end
	for j=1:size(PO,1),
        %write the transformation applied to every image
        %filename: deblank(PO(j,:))
      	spm_get_space(deblank(PO(j,:)), M*MM(:,:,j));
    end

	% ---------------------------------------------------------------------
	% Apply coregistration 
	% ---------------------------------------------------------------------

	P = char(PG, PO);
	spm_reslice(P,resFlags);

    fprintf('Finished coreg for pCASL label 1 and 2 for all subjects\n');
end