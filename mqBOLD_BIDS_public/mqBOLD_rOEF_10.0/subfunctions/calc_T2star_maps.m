% CP MAR/APR 2020: Clean data input and call T2* calculation
function filelist = calc_T2star_maps(SubjectDir, filelist, subject_name,...
                                            nconditions, T2s_pars, flag)

fprintf('\nCalculate T2* parameter map for multiple conditions:\n\n');

for ncd = 1:nconditions % T2* processing for all conditions
    
    if flag.condition(ncd).T2s.FR.Magn  % if  full resolution (FR)
                                        % multiecho T2* data exist
        %------------------------------------------------------------------
        % Extract parameters from nii and json files (Siemens & Philips)
        %------------------------------------------------------------------ 
        %Read json file from 1st data set from list to determine parameters
        jsoninfo = jsondecode(...
            fileread(fullfile(SubjectDir.condition(ncd).T2s.FR.Magn, ...
            cell2mat(filelist.condition(ncd).T2s.FR.Magn.json(1)))));    
            %nechoes = jsoninfo.EchoTrainLength; % does not always exist
        nechoes = size(filelist.condition(ncd).T2s.FR.Magn.nii,2);
        Manufacturer = jsoninfo.Manufacturer;
        if strcmp(Manufacturer, T2s_pars.manufacturer) == 0
            fprintf('\n\nWARNING:');
            fprintf('\nManufacturer info in json file (%s) differs from T2s_pars settings (%s),',...
                Manufacturer, T2s_pars.manufacturer);
            fprintf('\nT2* motion correction crucially depends on correct settings!');
        end
%       sliceThickness = jsoninfo.SliceThickness; % does not always exist
%       voxel_size = headerinfo_T2s.PixelDimensions; % does not always exist
        % -----------------------------------------------------------------
        % Use headerinfo from niftiread to obtain voxel size and slice
        % thickness
        % -----------------------------------------------------------------
        headerinfo = ...
            niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.FR.Magn,...
            cell2mat(filelist.condition(ncd).T2s.FR.Magn.nii(1))));        
        voxel_size = headerinfo.PixelDimensions;     
        sliceThickness = floor(voxel_size(3)); 
        
        % -----------------------------------------------------------------
        % determine data dimension etc. from 1st data set in list
        % -----------------------------------------------------------------
        headerinfo_T2s = ...
            spm_vol(fullfile(SubjectDir.condition(ncd).T2s.FR.Magn,...
            cell2mat(filelist.condition(ncd).T2s.FR.Magn.nii(1)))); 
        [vol, ~] = spm_read_vols( headerinfo_T2s);
        [nx, ny, nslices] = size(vol);

        % -----------------------------------------------------------------
        % Save nifti Header as template for saving of Maps (TE1, T2, errors)
        % -----------------------------------------------------------------
        % CP 30JUN20: Writing calculated maps via spm_write_vol with header 
        %             info of input data obtained via spm_vol, ensures
        %             correct orientation of output data (= input), even if
        %             data are read via niftiread because of scaling
        %             issues. But this needs to be checked when input
        %             changes.
        % -----------------------------------------------------------------
        V_save       = headerinfo_T2s(1); % save image header info as structure
        V_save.dt    = [64 0];           % 64 stands for float data type 

    end % if flag.condition(ncd).T2s.FR.Magn

    %----------------------------------------------------------------------
    % read full resolution magnitude & In/Re (Philips) OR phase (Siemens)
    %----------------------------------------------------------------------
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % READ PHILIPS DATA (if available)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmp(Manufacturer, 'Philips')
         fprintf('\nCondition %d/%d: data from %s MR scanner: Expecting Magn, Re, Im data\n',...
                 ncd, nconditions, Manufacturer);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        % read T2* full resolution (FR) data
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%          
        % read FR magnitude data
        if flag.condition(ncd).T2s.FR.Magn
            %--------------------------------------------------------------
            % Initialize variables for T2* echo data: 1:3: Magn, Re ,Im
            %--------------------------------------------------------------   
            vol_T2S_fr(1:nx,1:ny,1:nslices,1:nechoes,1:3) = 0;
            TE(1:nechoes) = 0;
            TR(1:nechoes) = 0;
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.FR.Magn, ...
                    cell2mat(filelist.condition(ncd).T2s.FR.Magn.json(i)))));
                necho = jsoninfo.EchoNumber;
                TE(necho) = jsoninfo.EchoTime * 1000; % [in ms]
                TR(necho) = jsoninfo.RepetitionTime * 1000;  % [in ms]
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.FR.Magn,...
                    cell2mat(filelist.condition(ncd).T2s.FR.Magn.nii(i)))); 
                vol_T2S_fr(:,:,:,necho,1) = niftiread( headerinfo_T2s);
            end % for i = 1:nechoes	
            % save 1st echo image for coregistration
            filelist.condition(ncd).T2s.TE1 = ...
                fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
                sprintf('TE1_%s.nii',subject_name));
            V_save.fname = filelist.condition(ncd).T2s.TE1;
            spm_write_vol(V_save, squeeze(vol_T2S_fr(:,:,:,1,1)));
        end % if flag.condition(ncd).T2s.FR.Magn
        if flag.condition(ncd).T2s.FR.Re
            % read FR real data
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.FR.Re, ...
                    cell2mat(filelist.condition(ncd).T2s.FR.Re.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.FR.Re,...
                    cell2mat(filelist.condition(ncd).T2s.FR.Re.nii(i)))); 
                vol_T2S_fr(:,:,:,necho,2) = niftiread( headerinfo_T2s);
            end % i = 1:nechoes              
        end %if flag.condition(ncd).T2s.FR.Re
        if flag.condition(ncd).T2s.FR.Im
            % read FR imaginary data
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.FR.Im, ...
                    cell2mat(filelist.condition(ncd).T2s.FR.Im.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.FR.Im,...
                    cell2mat(filelist.condition(ncd).T2s.FR.Im.nii(i)))); 
                vol_T2S_fr(:,:,:,necho,3) = niftiread( headerinfo_T2s);
            end % i = 1:nechoes              
        end %if flag.condition(ncd).T2s.FR.Im
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % read T2* half resolution (HR) data (if available)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%          
        % read HR magnitude data
        if flag.condition(ncd).T2s.HR.Magn
            %--------------------------------------------------------------
            % Initialize variables for T2* echo data: 1:3: Magn, Re ,Im
            %--------------------------------------------------------------   
            vol_T2S_hr(1:nx,1:ny,1:nslices,1:nechoes,1:3) = 0;
            TE(1:nechoes) = 0;
            TR(1:nechoes) = 0;        
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.HR.Magn, ...
                    cell2mat(filelist.condition(ncd).T2s.HR.Magn.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.HR.Magn,...
                    cell2mat(filelist.condition(ncd).T2s.HR.Magn.nii(i)))); 
                vol_T2S_hr(:,:,:,necho,1) = niftiread( headerinfo_T2s);
            end % for i = 1:nechoes	
        end % if flag.condition(ncd).T2s.HR.Magn
        if flag.condition(ncd).T2s.HR.Re
            % read HR real data
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.HR.Re, ...
                    cell2mat(filelist.condition(ncd).T2s.HR.Re.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.HR.Re,...
                    cell2mat(filelist.condition(ncd).T2s.HR.Re.nii(i)))); 
                vol_T2S_hr(:,:,:,necho,2) = niftiread( headerinfo_T2s);
            end % i = 1:nechoes              
        end %if flag.condition(ncd).T2s.HR.Re
        if flag.condition(ncd).T2s.HR.Im
            % read HR imaginary data
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.HR.Im, ...
                    cell2mat(filelist.condition(ncd).T2s.HR.Im.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.HR.Im,...
                    cell2mat(filelist.condition(ncd).T2s.HR.Im.nii(i)))); 
                vol_T2S_hr(:,:,:,necho,3) = niftiread(headerinfo_T2s);
            end % i = 1:nechoes              
        end %if flag.condition(ncd).T2s.HR.Im    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % read T2* quarter resolution (QR) data (if available)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%           
        % read QR magnitude data 
        if flag.condition(ncd).T2s.QR.Magn
            %--------------------------------------------------------------
            % Initialize variables for T2* echo data: 1:3: Magn, Re ,Im
            %--------------------------------------------------------------   
            vol_T2S_qr(1:nx,1:ny,1:nslices,1:nechoes,1:3) = 0;
            TE(1:nechoes) = 0;
            TR(1:nechoes) = 0;
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.QR.Magn, ...
                    cell2mat(filelist.condition(ncd).T2s.QR.Magn.json(i)))));
                necho = jsoninfo.EchoNumber;
                TE(necho) = jsoninfo.EchoTime * 1000; % [in ms]
                TR(necho) = jsoninfo.RepetitionTime * 1000;  % [in ms]
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.QR.Magn,...
                    cell2mat(filelist.condition(ncd).T2s.QR.Magn.nii(i)))); 
                vol_T2S_qr(:,:,:,necho,1) = niftiread( headerinfo_T2s);
            end % for i = 1:nechoes	
        end % if flag.condition(ncd).T2s.QR.Magn
        if flag.condition(ncd).T2s.QR.Re
            % read QR real data
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.QR.Re, ...
                    cell2mat(filelist.condition(ncd).T2s.QR.Re.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.QR.Re,...
                    cell2mat(filelist.condition(ncd).T2s.QR.Re.nii(i)))); 
                vol_T2S_qr(:,:,:,necho,2) = niftiread( headerinfo_T2s);
            end % i = 1:nechoes              
        end %if flag.condition(ncd).T2s.QR.Re
        if flag.condition(ncd).T2s.QR.Im
            % read QR imaginary data
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.QR.Im, ...
                    cell2mat(filelist.condition(ncd).T2s.QR.Im.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.QR.Im,...
                    cell2mat(filelist.condition(ncd).T2s.QR.Im.nii(i)))); 
                vol_T2S_qr(:,:,:,necho,3) = niftiread( headerinfo_T2s);
            end % i = 1:nechoes              
        end %if flag.condition(ncd).T2s.QR.Im       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % READ SIEMENS DATA (if available)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    elseif strcmp(Manufacturer, 'Siemens')
        fprintf('\nCondition %d/%d: data from %s MR scanner: Expecting Magn & Phase data\n',...
                 ncd, nconditions, Manufacturer);          
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        % read T2* full resolution (FR) data
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         	
        % read FR magnitude data
        if flag.condition(ncd).T2s.FR.Magn
            %--------------------------------------------------------------
            % Initialize variables for T2* echo data: 1:3: Magn, Phase
            %--------------------------------------------------------------   
            vol_T2S_fr(1:nx,1:ny,1:nslices,1:nechoes,1:2) = 0;
            TE(1:nechoes) = 0;
            TR(1:nechoes) = 0;
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.FR.Magn, ...
                    cell2mat(filelist.condition(ncd).T2s.FR.Magn.json(i)))));
                necho = jsoninfo.EchoNumber;
                TE(necho) = jsoninfo.EchoTime * 1000; % [in ms]
                TR(necho) = jsoninfo.RepetitionTime * 1000;  % [in ms]
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.FR.Magn,...
                    cell2mat(filelist.condition(ncd).T2s.FR.Magn.nii(i)))); 
                vol_T2S_fr(:,:,:,necho,1) = niftiread( headerinfo_T2s);
            end % for i = 1:nechoes
            % save 1st echo image for coregistration
            filelist.condition(ncd).T2s.TE1 = ...
                fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
                sprintf('TE1_%s.nii',subject_name));
            V_save.fname = filelist.condition(ncd).T2s.TE1;
            spm_write_vol(V_save, squeeze(vol_T2S_fr(:,:,:,1,1)));            
        end % if flag.condition(ncd).T2s.FR.Magn
        if flag.condition(ncd).T2s.FR.Ph
            % read FR phase data
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.FR.Ph, ...
                    cell2mat(filelist.condition(ncd).T2s.FR.Ph.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.FR.Ph,...
                    cell2mat(filelist.condition(ncd).T2s.FR.Ph.nii(i)))); 
                vol_T2S_fr(:,:,:,necho,2) = niftiread( headerinfo_T2s);
            end % i = 1:nechoes              
        end %if flag.condition(ncd).T2s.FR.Ph
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        % read T2* half resolution (HR) data (if available)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%          
        % read HR magnitude data 
        if flag.condition(ncd).T2s.HR.Magn
            %--------------------------------------------------------------
            % Initialize variables for T2* echo data: 1:2: Magn, Phase
            %--------------------------------------------------------------   
            vol_T2S_hr(1:nx,1:ny,1:nslices,1:nechoes,1:2) = 0;
            TE(1:nechoes) = 0;
            TR(1:nechoes) = 0;
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.HR.Magn, ...
                    cell2mat(filelist.condition(ncd).T2s.HR.Magn.json(i)))));
                necho = jsoninfo.EchoNumber;
                TE(necho) = jsoninfo.EchoTime * 1000; % [in ms]
                TR(necho) = jsoninfo.RepetitionTime * 1000;  % [in ms]
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.HR.Magn,...
                    cell2mat(filelist.condition(ncd).T2s.HR.Magn.nii(i)))); 
                vol_T2S_hr(:,:,:,necho,1) = niftiread( headerinfo_T2s);
            end % for i = 1:nechoes	
        end % if flag.condition(ncd).T2s.HR.Magn
        if flag.condition(ncd).T2s.HR.Ph
            % read HR phase data
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.HR.Ph, ...
                    cell2mat(filelist.condition(ncd).T2s.HR.Ph.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.HR.Ph,...
                    cell2mat(filelist.condition(ncd).T2s.HR.Ph.nii(i)))); 
                vol_T2S_hr(:,:,:,necho,2) = niftiread( headerinfo_T2s);
            end % i = 1:nechoes              
        end %if flag.condition(ncd).T2s.HR.Ph   
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % read T2* quarter resolution (QR) data
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%           
        % read QR magnitude data (if available)
        if flag.condition(ncd).T2s.QR.Magn
            %--------------------------------------------------------------
            % Initialize variables for T2* echo data: 1:2: Magn, Phase
            %--------------------------------------------------------------
            vol_T2S_qr(1:nx,1:ny,1:nslices,1:nechoes,1:2) = 0;
            TE(1:nechoes) = 0;
            TR(1:nechoes) = 0;
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.QR.Magn, ...
                    cell2mat(filelist.condition(ncd).T2s.QR.Magn.json(i)))));
                necho = jsoninfo.EchoNumber;
                TE(necho) = jsoninfo.EchoTime * 1000; % [in ms]
                TR(necho) = jsoninfo.RepetitionTime * 1000;  % [in ms]
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                 
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.QR.Magn,...
                    cell2mat(filelist.condition(ncd).T2s.QR.Magn.nii(i)))); 
                vol_T2S_qr(:,:,:,necho,1) = niftiread( headerinfo_T2s);
            end % for i = 1:nechoes	
        end % if flag.condition(ncd).T2s.QR.Magn
        if flag.condition(ncd).T2s.QR.Ph
            % read QR real data
            for i = 1:nechoes % read json file for TE & TR info
                jsoninfo = jsondecode(...
                    fileread(fullfile(SubjectDir.condition(ncd).T2s.QR.Ph, ...
                    cell2mat(filelist.condition(ncd).T2s.QR.Ph.json(i)))));
                necho = jsoninfo.EchoNumber;
                % ---------------------------------------------------------
                % Use niftiinfo and niftiread (instead of spm_read_vols) 
                % because this reads data with proper scaling for moco  
                % and background field correction
                % ---------------------------------------------------------                
                headerinfo_T2s = ...
                    niftiinfo(fullfile(SubjectDir.condition(ncd).T2s.QR.Ph,...
                    cell2mat(filelist.condition(ncd).T2s.QR.Ph.nii(i)))); 
                vol_T2S_qr(:,:,:,necho,2) = niftiread( headerinfo_T2s);
            end % i = 1:nechoes              
        end %if flag.condition(ncd).T2s.QR.Ph            

    end % if strcmp(Manufacturer, 'Philips' // 'Siemens')
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % END READ DATA
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculate T2* maps
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmp(T2s_pars.correction, 'RD') &&... % works for Magn/Re/Im data
        (flag.condition(ncd).T2s.FR.Magn == 1) &&... % (acquired at 
      	(flag.condition(ncd).T2s.HR.Magn == 1) &&... % Philips scanner)
       	(flag.condition(ncd).T2s.QR.Magn == 1)     % AND Siemens Magn/Phase
        %------------------------------------------------------------------
        % 'RD': Most recent implementation of motion correction and magnetic
        % background fields by Ralf Deichmann ---> ralf_t2s_toolbox_24jan18
        % with improved motion correction according to:
        % N�th U, Volz S, Hattingen E, Deichmann R. An improved method for 
        %   retrospective motion correction in quantitative T2* mapping.
        %   Neuroimage. 2014 May 15;92:106-19. 
        %   doi: 10.1016/j.neuroimage.2014.01.050. Epub 2014 Feb 4.
        %------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % CP 15JUN18//05APR20
        % Needs to be adapted and checked for magn & phase Siemens data
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       	[SubjectDir, filelist] = calc_T2S_Moco_Gcorr_RD(SubjectDir, filelist, ...
        	subject_name, ncd, vol_T2S_fr, vol_T2S_hr, vol_T2S_qr, T2s_pars, ...
          	nechoes, TE, nslices, sliceThickness, voxel_size, V_save, Manufacturer);
    elseif strcmp(T2s_pars.correction, 'RD') &&... % works for Magn/Re/Im data
            (flag.condition(ncd).T2s.FR.Magn == 1) &&... % (acquired at 
            (flag.condition(ncd).T2s.HR.Magn == 1) &&... % Philips scanner)
            (flag.condition(ncd).T2s.QR.Magn == 0) % AND Siemens Magn/Phase 
            % easiest workaround for missing QR data:
            % duplicate HR input for QR, i.e. use vol_T2S_hr twice!
        [SubjectDir, filelist] = calc_T2S_Moco_Gcorr_RD(SubjectDir, filelist, ...
            subject_name, ncd, vol_T2S_fr, vol_T2S_hr, vol_T2S_hr, T2s_pars, ...
            nechoes, TE, nslices, sliceThickness, voxel_size, V_save, Manufacturer);        
	else    % if only full resolution data are available,
          	% ---> simple exponential fit of magnitude data
          	% TO DO: implement magnetic background field correction
     	[SubjectDir, filelist] = calc_T2s_uncorr(SubjectDir, filelist, ...
        	subject_name, ncd, squeeze(vol_T2S_fr(:,:,:,:,1)), TE, V_save);   
    end % if strcmp(T2s_pars.correction, 'RD' vs.'NH') // FR, HR, QR 
end % for nconditions (T2* processing)
end

