function filelist = coreg_T2s_to_TE1T2(SubjectDir, filelist, subject_name,...
                                                  nconditions, T2s_pars, flag)
	global defaults;
	spm_get_defaults;
	flags = defaults.coreg;

	% Flags to pass to routine (spm_reslice) to create resliced images
    resFlags = struct(...
    	'interp', 1,...%flags.write.interp,... % 1: trilinear interpolation
    	'wrap', flags.write.wrap,...           % wrapping info (ignore...)
      	'mask', flags.write.mask,...           % masking (see spm_reslice)
      	'which',1,...                          % write reslice time series for 
      	'mean',0);                             % later use do not write 
    %----------------------------------------------------------------------   
	% PG - Tar(G)et image, NEVER CHANGED
	% PF - Source image, transformed to match PG
	% PO - (O)ther images, already realigned to PF and transformed with PF
    %----------------------------------------------------------------------
	% T2 directory of 1st condition or single T2 contains reference (target)
    %----------------------------------------------------------------------
    if T2s_pars.T2_nconditions > 1 % coregister all T2 maps for different
                                   % conditions to TE1 of condition 1
        T2_reference = SubjectDir.condition(1).T2;
    else % only one T2 map exists in toplevel qBOLD folder that is used as 
         % as target for coregistration and R2' calculation
        T2_reference = SubjectDir.T2;
    end    
    
	% TARGET: T2, first echo
	PG = spm_select('FPList', T2_reference, '^TE1.*.nii$');
    if isempty(PG)
        error('No Target (T2, TE1) selected. Check files or file names!');
    end      
	PG = PG(1,:);
	VG = spm_vol(PG);
 
	%----------------------------------------------------------------------
	% T2s maps exist in each condition's T2S folders
	%----------------------------------------------------------------------
	fprintf('\n Now coregistering %s''s all conditions T2s maps to TE1 of reverence T2.\n %s\n',...
                                                       subject_name, PG); 
for ncd = 1:nconditions % for each subject
	if flag.condition(ncd).T2s.FR.Magn
        fprintf('\nCoregistering T2* condition %d/%d for subject %s\n',...
                                            ncd, nconditions, subject_name);
        % SOURCE: FLAIR data coregistered to nCBV         
        PF = spm_select('FPList', SubjectDir.condition(ncd).T2s.toplevel,...
                                                            '^TE1.*.nii$');  
        if isempty(PF)
            error('No Source (TE1) selected. Check files or file names!');
        end
        PF = PF(1,:);
        VF = spm_vol(PF);

        % select "other images" for coregistration (T2s maps)
        PO1 = spm_select( 'FPList', SubjectDir.condition(ncd).T2s.toplevel,...
            '^T2s.*.nii$');
        if isempty(PO1)
            error('No T2s maps selected. Check files or file names!');
        end

        % select absgrad (x, y, z) maps for coregistration
        PO2 = spm_select( 'FPList', SubjectDir.condition(ncd).T2s.toplevel,...
            '^absgrad.*.nii$');
        if isempty(PO2)
            error('No absgrad maps selected. Check files or file names!');
        end

        if isempty(PO1) && isempty(PO2) 
            PO = PF;
        else
            PO = char(PF,PO1,PO2);
        end

        % do coregistration, i.e. get coregistration parameters
        x  = spm_coreg( VG, VF, flags.estimate);

        % get the transformation to be applied with parameters 'x'
        M  = inv( spm_matrix(x));

        % in MM we put the transformations for the 'other' images
        MM = zeros( 4, 4, size( PO,1));

        for j=1:size( PO,1)
            % get the transformation matrix for every image
            MM(:,:,j) = spm_get_space( deblank( PO(j,:)));
        end

        for j=1:size( PO,1)
            % write the transformation applied to every image
            % filename: deblank(PO(j,:))
            spm_get_space( deblank( PO(j,:)), M*MM(:,:,j));
        end

        P = char( PG, PO);
        spm_reslice( P,resFlags);  
        
        %------------------------------------------------------------------
        % Copy coregistered parameter maps to toplevel condition directory
        %------------------------------------------------------------------
        source = fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
            sprintf('rT2s_%s.nii',subject_name));
        filelist.condition(ncd).T2s.map = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'T2s.nii');
        copyfile(source, filelist.condition(ncd).T2s.map);

        source = fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
            sprintf('rabsgradb0_%s.nii',subject_name));
        filelist.condition(ncd).T2s.absgradb0 = ...
            fullfile(SubjectDir.condition(ncd).toplevel, 'T2s_absgradb0.nii');
        copyfile(source, filelist.condition(ncd).T2s.absgradb0);
	end %if flag.condition(ncd).T2s.FR.Magn
end % for ncd = 1:nconditions                                  
end

