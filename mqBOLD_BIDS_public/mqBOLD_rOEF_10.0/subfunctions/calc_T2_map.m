% CP MAR/APR 2020: Clean data input and call T2 calculation
function filelist = calc_T2_map(SubjectDir, filelist, subject_name, ...
                                             nconditions, T2s_pars, flag)

    fprintf('\nCalculate T2 parameter map:\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Process T2 data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag.T2 % if T2 data exist
    
    %----------------------------------------------------------------------
    % Read data and extract necessary parameters from nii and json files
    %----------------------------------------------------------------------
    % assume that number of files in list equals numer of echoes
    nechoes = size(filelist.T2.nii,2);
    
    %Read first data set from list and determine parameters
    headerinfo_T2 = ...
        spm_vol(fullfile(SubjectDir.T2, cell2mat(filelist.T2.nii(1)))); 
    [vol, ~] = spm_read_vols( headerinfo_T2);
    [nx, ny, nslices] = size(vol);
    clear vol;

    % read T2 echo datat and parameters
    vol(1:nx,1:ny,1:nslices,1:nechoes) = 0;
    TE(1:nechoes) = 0;
    TR(1:nechoes) = 0;
    % read json file for TE & TR info
    for i = 1:nechoes
        jsoninfo = jsondecode(...
            fileread(fullfile(SubjectDir.T2, cell2mat(filelist.T2.json(i)))));
        necho = jsoninfo.EchoNumber;
        TE(necho) = jsoninfo.EchoTime * 1000; % [in ms]
        TR(necho) = jsoninfo.RepetitionTime * 1000;  % [in ms]
        headerinfo_T2 = ...
            spm_vol(fullfile(SubjectDir.T2, cell2mat(filelist.T2.nii(i)))); 
        [vol(:,:,:,necho), ~] = spm_read_vols( headerinfo_T2);
    end
	Manufacturer = jsoninfo.Manufacturer;    
    
%     voxel_size = [ceil(10.0*abs( headerinfo_T2.mat(1,1)))/10,...
%                   ceil(10.0*abs( headerinfo_T2.mat(2,2)))/10,...
%                   ceil(10.0*abs( headerinfo_T2.mat(3,3)))/10];
%                   jsoninfo.SpacingBetweenSlices]; % does not always exist

    % ---------------------------------------------------------------------
    % Save nifti Header as template for saving of Maps (TE1, T2, errors):
    % ---------------------------------------------------------------------
    V_save       = headerinfo_T2(1); % save image header info as structure
    V_save.dt    = [64 0];           % 64 stands for float data type 
    
    %----------------------------------------------------------------------
    % Calculate T2 maps
    %----------------------------------------------------------------------
    maske = squeeze(vol(:,:,:,1)) > mean(mean(mean(vol(:,:,:,1))));

    if strcmp(Manufacturer,'Philips') && strcmp(T2s_pars.T2_acqType, '3D')
        fprintf('\n3D T2 data: Fitting all echoes!\n');
        zeit = TE;
        data = vol;
    else
        % fitting only even echoes
        zeit = TE(2:2:end);
        data = vol(:,:,:,2:2:nechoes);
    end
    clear TE vol;
    
    fprintf('\n Starting T2 fit:\n slice# ');

    % slice wise fit of T2 and amp values
    for i = 1:nslices
        fprintf('%d ', i);
        serie = squeeze(data(:,:,i,:));
        % function 
        %[amap,cmap,devmap] = ...
        %       imagefit2param(zeit,serie,x,y,g1,g2,p,noisefakt,maxdev)
        % 2 parameter fit:  wert[i] = a*{x+y*exp(-zeit[i]/c)}
        % zeit: vector containing time points
        % serie: 3D set containing weighted images
        x = 0;          % x according to fit formula
        y = 1;          % y according to fit formula
        g1 = 1;         % lower limit for c
        g2 = 4000;      % upper limit for c
        p = 0.01;       % accuracy in relative values
        noisefakt = 10; % noise factor
        maxdev = 20;    % max. deviation of data from fitted values [in %]
        [~, cmap, devmap] = ...
            imagefit2param( zeit, serie,x,y,g1,g2,p,noisefakt,maxdev);
        t2map(:,:,i) = cmap;
        error(:,:,i) = devmap;
    end

    fprintf('\n');

    % mask T2 map and restrict maximum value to 150 ms to improve results
    % of coregistration / spatial normalisation
    t2map = t2map.*maske;
    t2map(t2map>150.0)=150.0;
    error = error.*maske;
    
    % save T2 parameter map
    filelist.T2.map = ...
        fullfile(SubjectDir.T2, sprintf('T2_%s.nii', subject_name));
    V_save.fname = filelist.T2.map;
    spm_write_vol(V_save, t2map);

    % save T2 fit error map
    filelist.T2.error = ...
        fullfile(SubjectDir.T2, sprintf('T2_error_%s.nii',subject_name));
    V_save.fname = filelist.T2.error;
    spm_write_vol(V_save, error);
    
    % save 1st echo image for coregistration
    filelist.T2.TE1 = ...
        fullfile(SubjectDir.T2, sprintf('TE1_%s.nii',subject_name)); 
    V_save.fname = filelist.T2.TE1;
    spm_write_vol(V_save, squeeze(data(:,:,:,1)));
    
    fprintf('\nOnly one T2 parameter map exists! Copied to all %d conditions.\nPath: %s\n',...
        nconditions, SubjectDir.T2);
    % copy parameter maps to toplevel condition directories
    
    if T2s_pars.T2_nconditions == 1
        for ncd = 1:nconditions
            filelist.condition(ncd).T2.map = ...
                fullfile(SubjectDir.condition(ncd).toplevel, 'T2.nii');
            copyfile(filelist.T2.map, filelist.condition(ncd).T2.map)
            filelist.condition(ncd).T2.error = ...
                fullfile(SubjectDir.condition(ncd).toplevel, 'T2_error.nii');
            copyfile(filelist.T2.error, filelist.condition(ncd).T2.error)
            filelist.condition(ncd).T2.TE1 = ...
                fullfile(SubjectDir.condition(ncd).toplevel, 'T2_TE1.nii');
            copyfile(filelist.T2.TE1, filelist.condition(ncd).T2.TE1)
        end % ncd = 1:nconditions
    end %if T2s_pars.T2_nconditions == 1
end % if flag.T2
end

