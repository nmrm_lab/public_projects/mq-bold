function filelist = calc_rOEF(SubjectDir, filelist, subject_name, ...
                                      nconditions, T2s_pars, flag)
% - SK11JUN15
%	+ SPM12 migration: Change segment filetype from .img to .nii
% - CP24JAN16
%   reduce rOEF calculation to essential steps
%   use coregistered GM and WM Segments for masking
% -CP 06JUN18
%   facilitate R2' calculation without DSC
% -CP 09JUN19
%   facilitate multiple conditons
% -CP 02APR20
%  adaptation to BIDS input and major revision

fprintf('\n%s',subject_name);
fprintf('\nin function calc_rOEF.m:\n');

if T2s_pars.GMWM_mask
    %----------------------------------------------------------------------
    % Load coregistered coregistered GM & WM sements to generate Brain mask
    % excluding outer CSF spaces
    %----------------------------------------------------------------------
    GM = niftiread(filelist.condition(1).GM);   
    WM = niftiread(filelist.condition(1).WM); 
    [nx, ny, nz] = size(GM);
    mask = fillholes(GM+WM);
    mask = reshape(mask, [nx, ny, nz]); 
else
    %----------------------------------------------------------------------
    % Load coregistered Brain mask
    %----------------------------------------------------------------------
    BrMsk = niftiread(filelist.condition(1).BrMsk_CSF);    
    mask = BrMsk>0.5;
end
    
% process all conditions
for ncd =1:nconditions
	%----------------------------------------------------------------------
	% load normalized CBV and eliminate nan
	%----------------------------------------------------------------------
	if flag.DSCfinished
        V_DSC = spm_vol(filelist.condition(ncd).CBV);
        [CBV_DSC, ~] = spm_read_vols( V_DSC);
        CBV_DSC( isnan( CBV_DSC)) = 0;
        CBV_DSC = CBV_DSC.*mask;
        % Save coregistered, cleaned and thresholded maps
        spm_write_vol( V_DSC, CBV_DSC);
	end % flag.DSCfinished   
    
	%----------------------------------------------------------------------
	% load T2, eliminate nan and limit range
	%----------------------------------------------------------------------        
    V = spm_vol(filelist.condition(ncd).T2.map);
    [T2, ~] = spm_read_vols(V);    
    T2( isnan( T2)) = 0;
    T2 = T2.*mask;
    T2( T2 < 0) = 0;
    T2( T2 > 150) = 150;
    % Save coregistered, cleaned and thresholded maps
    V.fname = filelist.condition(ncd).T2.map;
    spm_write_vol(V,T2);  
	%----------------------------------------------------------------------
	% Calculate R2
	%----------------------------------------------------------------------
	R2 = 1000 / (T2+eps); %  -> sec^-1
    % Upper boundary to avoid unreasonably bright hotspot
    mask = mask.*(T2>0)>0;
	R2max = 5*median( R2(mask));
	R2( R2>R2max) = R2max;
    R2 = R2.*mask;
	sR2 = smooth3( R2.*mask,'gaussian',3);
	% mask again after smoothing
	sR2 = sR2.*mask;
    %----------------------------------------------------------------------
    % Save coregistered, cleaned and thresholded maps 
    %----------------------------------------------------------------------
    % R2
    filelist.condition(ncd).R2 = ...
        fullfile(SubjectDir.condition(ncd).toplevel, 'R2.nii');
    V.fname = filelist.condition(ncd).R2;
    spm_write_vol(V,R2);  
    %sR2
    filelist.condition(ncd).sR2 = ...
        fullfile(SubjectDir.condition(ncd).toplevel, 'sR2.nii');
    V.fname = filelist.condition(ncd).sR2;
    spm_write_vol(V,sR2); 
    
	%----------------------------------------------------------------------
	% load coregistered T2* map
	%----------------------------------------------------------------------
    V = spm_vol(filelist.condition(ncd).T2s.map);
    [T2S, ~] = spm_read_vols(V);
    T2S( isnan( T2S)) = 0;
    mask = mask.*(T2S>0)>0;
    T2S = T2S.*mask;
	T2S( T2S < 0) = 0;
	T2S( T2S > 150) = 150;  
    % Save coregistered, cleaned and thresholded maps
    spm_write_vol(V,T2S); 
    
	%----------------------------------------------------------------------
	% Calculate R2*
	%----------------------------------------------------------------------
	R2S = 1000 / (T2S+eps); % -> sec^-1
    % Upper boundary to avoid unreasonably bright hotspot
	R2Smax = 5*median(R2S(mask));
	R2S( R2S > R2Smax) = R2Smax;
    R2S = R2S.*mask;
	sR2S = smooth3( R2S.*mask,'gaussian',3);
	% mask again after smoothing
	sR2S = sR2S.*mask;
    
    %---------------------------------------------------------------------
    % Save coregistered, cleaned and thresholded maps 
    %----------------------------------------------------------------------
    % R2S
    filelist.condition(ncd).R2S = ...
        fullfile(SubjectDir.condition(ncd).toplevel, 'R2s.nii');
    V.fname = filelist.condition(ncd).R2S;
    spm_write_vol(V,R2S); 
    %sR2S
    filelist.condition(ncd).sR2S = ...
        fullfile(SubjectDir.condition(ncd).toplevel, 'sR2s.nii');
    V.fname = filelist.condition(ncd).sR2S;
    spm_write_vol(V,sR2S); 
    
	%----------------------------------------------------------------------
	% Calculate smoothed R2'
	%----------------------------------------------------------------------
 	sR2strich = (sR2S - sR2);
  	sR2strich( sR2strich < 0) = 0;
    % Upper boundary to avoid unreasonably bright hotspot
	sR2strichmax = 3*median(sR2strich(mask));
    if sR2strichmax < 20
    	sR2strichmax = 20; % FIXED threshold
    end
    sR2strich( sR2strich > sR2strichmax) = sR2strichmax;
	sR2strich = sR2strich.*mask;
    %---------------------------------------------------------------------
    % Save coregistered, cleaned and thresholded maps 
    %----------------------------------------------------------------------
    %sR2strich
    filelist.condition(ncd).sR2strich = ...
        fullfile(SubjectDir.condition(ncd).toplevel, 'sR2strich.nii');
    V.fname = filelist.condition(ncd).sR2strich;
    spm_write_vol(V,sR2strich);   
 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculate & write relative apparent oxygen extraction fraction (rOEF)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if flag.DSCfinished
        % [Hz], physiologic constant derived from literature
      	C = 4/3*267.61918*pi*0.264*0.42*0.85*3; % 317 Hz

      	rCBV = CBV_DSC/100.0; %CBV fraction, e.g. CBV_WM = 0.015
      	rCBV( rCBV < 0) = 0;
       	rOEF = (sR2strich ./ (C*rCBV+eps)) .* mask;

      	% reasonable boundary to avoid unreasonably high hot spots
     	rOEFmax_DSC = 5*median( rOEF( mask));
        if rOEFmax_DSC < 1.5
            rOEFmax_DSC = 1.5; % FIXED Threshold
        end
     	rOEF( rOEF > rOEFmax_DSC) = rOEFmax_DSC;
        rOEF = rOEF .*mask;
    %---------------------------------------------------------------------
    % Save coregistered, cleaned and thresholded maps 
    %----------------------------------------------------------------------
    %sR2strich
    filelist.condition(ncd).rOEF = ...
        fullfile(SubjectDir.condition(ncd).toplevel, 'rOEF.nii');
    V.fname = filelist.condition(ncd).rOEF;
    spm_write_vol(V,rOEF);   
 
	end % if flag.DSCfinished
end%loop through all conditions
end