function [SubjectDir, filelist] = calc_T2s_uncorr(SubjectDir, filelist, ...
                                    subject_name, ncd, data, zeit, V_save)
% Simple exponential fit of T2s multiecho data
    [nx, ny, nslices, ~] = size(data);

	T2S_uncorr(1:nx,1:ny,1:nslices) = 0;
	T2S_error(1:nx,1:ny,1:nslices) = 0;
    % slice wise fit of T2 and amp values
    for i = 1:nslices
        fprintf('%d ', i);
        serie = squeeze(data(:,:,i,:));
        % function 
        %[amap,cmap,devmap] = ...
        %       imagefit2param(zeit,serie,x,y,g1,g2,p,noisefakt,maxdev)
        % 2 parameter fit:  wert[i] = a*{x+y*exp(-zeit[i]/c)}
        % zeit: vector containing time points
        % serie: 3D set containing weighted images
        x = 0;          % x according to fit formula
        y = 1;          % y according to fit formula
        g1 = 1;         % lower limit for c
        g2 = 4000;      % upper limit for c
        p = 0.01;       % accuracy in relative values
        noisefakt = 10; % noise factor
        maxdev = 20;    % max. deviation of data from fitted values [in %]
        [~, cmap, devmap] = ...
            imagefit2param( zeit, serie,x,y,g1,g2,p,noisefakt,maxdev);
        T2S_uncorr(:,:,i) = cmap;
        T2S_error(:,:,i) = devmap;
    end

    fprintf('\n');

    % mask T2 map and restrict maximum value to 150 ms to improve results
    % of coregistration / spatial normalisation
    maske = squeeze(data(:,:,:,1)) > mean(mean(mean(data(:,:,:,1))));
    T2S_uncorr = T2S_uncorr.*maske;
    T2S_uncorr(T2S_uncorr>150.0)=150.0;
    T2S_error = T2S_error.*maske;
    
    % save uncorrected T2* parameter map
    fprintf('T2*-Mapping: uncorrected T2* Map\n');

        filelist.condition(ncd).T2s.uncorr = ...
         	fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
            sprintf('T2s_uncorr_%s.nii',subject_name));
        V_save.fname = filelist.condition(ncd).T2s.uncorr;
        spm_write_vol(V_save, T2S_uncorr);

        filelist.condition(ncd).T2s.map = ...
         	fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
            sprintf('T2s_uncorr_%s.nii',subject_name));
        V_save.fname = filelist.condition(ncd).T2s.uncorr;
        spm_write_vol(V_save, T2S_uncorr);        
        
        filelist.condition(ncd).T2s.error = ...
         	fullfile(SubjectDir.condition(ncd).T2s.toplevel, ...
            sprintf('T2s_error_%s.nii',subject_name));
        V_save.fname = filelist.condition(ncd).T2s.error;
        spm_write_vol(V_save, T2S_error);       
end

