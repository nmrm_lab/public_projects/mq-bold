function coreg_allConditionsT2(SubjectDir, subject_name, conditions, ...
                                                         nconditions)
	global defaults;
	spm_defaults;
	flags = defaults.coreg;

	% Flags to pass to routine (spm_reslice) to create resliced images
    resFlags = struct(...
    	'interp', 1,...%flags.write.interp,... % 1: trilinear interpolation
    	'wrap', flags.write.wrap,...           % wrapping info (ignore...)
      	'mask', flags.write.mask,...           % masking (see spm_reslice)
      	'which',1,...                          % write reslice time series for 
      	'mean',0);                             % later use do not write 
        
	fprintf('\n Now coregistering %s''s all conditions T2 data to condition %s.\n\n',...
                                              subject_name, conditions(1));  
                                                        
	% PG - Tar(G)et image, NEVER CHANGED
	% PF - Source image, transformed to match PG
	% PO - (O)ther images, originally realigned to PF and transformed again to PF
    
	% take the T2 directory of the first condition as reference(e.g. target)
	% --> SubjectDir.condition(1).T2;

	% TARGET: T2, first echo
	PG = spm_select('FPList', SubjectDir.condition(1).T2, '^TE1.*.nii$');
    if isempty(PG)
        error('No Target (T2, TE1) selected. Check files or file names!');
    end      
	PG = PG(1,:);
	VG = spm_vol(PG);

	% SOURCE
	% other conditions T2, first echo        
    % Coregister T2 data of all condition to condition'1'
	for ncd = 2:nconditions % for each subject
        % Define Images to be coregistered (SOURCE)
        % PF - Source image, transformed to match PG
        % PO - (O)ther images, originally realigned to PF and transformed again to PF

        % SOURCE
        % T2, first echo images of other conditions 
        PF = spm_select('FPList', SubjectDir.condition(ncd).T2, '^TE1.*.nii$');  
        if isempty(PF)
            error('No Source (T2, TE1) selected. Check files or file names!');
        end
        PF = PF(1,:);
        VF = spm_vol(PF);


        % select "other images" for coregistration (T2*, other echoes)
        % get T2* files in this directory
        clear PO;
        PO = spm_select( 'FPList', SubjectDir.condition(ncd).T2, '^T2.*.nii$');
        if isempty(PO)
            error('No T2 maps etc. selected. Check files or file names!');
        end
            
        if isempty(PO) || PO == '/'
            PO = PF;
        else
            PO = char(PF,PO);
        end

        % do coregistration
        % this method from spm_coreg_ui.m
        % get coregistration parameters
        x  = spm_coreg( VG, VF, flags.estimate);

        % get the transformation to be applied with parameters 'x'
        M  = inv( spm_matrix(x));

        % in MM we put the transformations for the 'other' images
        MM = zeros( 4, 4, size( PO,1));

        for j=1:size( PO,1)
            % get the transformation matrix for every image
            MM(:,:,j) = spm_get_space( deblank( PO(j,:)));
        end

        for j=1:size( PO,1)
            % write the transformation applied to every image
            % filename: deblank(PO(j,:))
            spm_get_space( deblank( PO(j,:)), M*MM(:,:,j));
        end

        P = char( PG, PO);
        spm_reslice( P,resFlags);
    
    %----------------------------------------------------------------------
    % copy coregistered parameter maps to toplevel condition directories 
    %----------------------------------------------------------------------
    fprintf('CAVE: copying of T2 maps to condition toplevel folder nees to be implemented\n');
        
    end %end for all conditions
end

