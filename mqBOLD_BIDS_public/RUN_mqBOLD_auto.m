%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   %% Main function < RUN_mqBOLD_auto >
%   %% Automated mq-BOLD DSC & rOEF processing
%   %% reading data from BIDS structure 'input'
%   %% writing results to 'output' (see details below)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   - Copyright by PD Dr. Christine Preibisch (CHP) <preibisch@tum.de>
%   
%   Major contributions:
%   - Nuria Hirsch: 
%       - T2/T2* analysis
%       - implementation of T2* moco
%       ---> t2smap_NH (replaced by more recent Version from Ralf
%                       Deichmann in June 2020)
%
%   - Anne Kluge: 
%       - Implemented DSC processing according to Boxerman et al. 2006
%         (incl. automatic AIF detection & leakage correction)
%       ---> DSC_CBV_x.x
%
%   - Stephan Kaczmarz 07MAR17
%       -adapt to carotis studie
%
%% - CP 20JUL17
%       - Use most actual DSC processing 'DSC_automated_3.0_CP'
%       - Overall cleanup of outdated features
%       - Adapt for future neuroscientific use
%       - Can be run as stand alone program to calculate MR-rOEF maps
%
% - CP 05JUN2018
%       - adapt to qBOLD imaging in healthy volunteers
%
% - CP 15JUN2018
%       - incorporate improved MoCo using ralf_t2s_toolbox_24jan2018
%
% - CP 28AUG2018
%       - allow qBOLD measurements at multiple condition (e.g. eyes 'open',
%         'closed', 'checker' stimulation)
%       - current implementation does NOT include processing DSC (needs to
%         be adapted and tested)
%       - only tested for PAR/REC data (T2 & full, half & quarter resolution 
%         T2* data)
%  - CP 04JAN2019
%       - include DSC and test / repair processing of Dicom data from PETMR
%       - test multiple condition processing for Dicom data
%       - prepare for DMN study (baseline R2' + multiple condition DSC)
%
%   - CP FEB 2020: 
%       - Wrap up changes done by Subhadarshini Parhi in order to
%         modularise program performance and change to BIDS input,
%         i.e. all inputs are nifti files
%       - harmonize path settings across entire program via dedicated
%         structures containing directories (SubjectDir) and paths to files
%         (filelist)
%
%   - CP MAR-APR 2020: Major revision
%       - Introduce functions (dsc_settings & T2s_settings) that enable a
%         focused setting of important, in part sequence dependent,
%         parameter settings, especially for T2* correction and DSC
%         processing
%
%% Required collection of subscripts
%
% - SPM12 (https://www.fil.ion.ucl.ac.uk/spm/software/spm12)
%       --> mainly used for spatial transformations (motion correction, 
%           coregistration, normalisation to MNI)
% - CommonUtilities (Collection of commonly used functions)
% - DSC_CBV_10.0 (rCBV calculation from DSC data)
%       --> major contributor: Anne Kluge, Christine Preibisch
%       NOTE: Ensure proper settings of parameters in 
%             ---> dsc_settings()
% - mqBOLD_rOEF_10.0 (MR-rOEF calculation from T2, T2*, rCBV)
%   	--> major contributors: Nuria Hirsch, Christine Preibisch, Ralf
%                               Deichmann, Stephan Kaczmarz
%       NOTE: Ensure proper settings of parameters 
%              ---> T2s_settings_Philips() OR T2s_settings_Siemens()
%--------------------------------------------------------------------------
%  Required Input folder structure:
%--------------------------------------------------------------------------
%  + INPUT is required in BIDS structure
%       STUDY_folder        : can (probably) be named arbitrarly
%        + '/input'
%           + '/sub-p001'   : subjects need to be numbered
%                             consecutively with prefix 'sub-p';
%  - Standardized subfolders contain:
%               + anat      : FLAIR & T1w data (*.nii.gz, *.json)
%               + asl       : multiple condition ASL data (*.nii.gz, *.json) 
%               + dsc       : DSC data (*.nii.gz, *.json)
%               + fmap      : field map data (*.nii.gz, *.json)
%               + func      : BOLD fMRI (e.g. functional localizer)
%               + t2map     : multi-echo T2w data (*.nii.gz, *.json)
%               + t2star    : multiple conditions multi-echo T2*w data
%                           -> case Philips
%                                   + magnitude (no label), real, imaginary
%                                   + fullres, MoCoHR, MoCoQR
%           + .... 
%  CAVE: - Subfolders must contain the data in nifti & json format.
%        - Any data conversion must be performed before!
%        - file names must NOT contain the character "^" or " "
%--------------------------------------------------------------------------
% Output structure is generated automatically.
%--------------------------------------------------------------------------
%  + OUTPUT goes to ouput folder and widely keeps previous structure within
%       STUDY_folder        : can (probably) be named arbitrarly
%        + '/output'
%           + '/sub-p001'   : subjects need to be numbered
%                             consecutively with prefix 'sub-p';
%               + qBOLD     : contains previous structure
%                   + '/T1w/'    : contains (segmented) T1w data
%                   + '/FLAIR/'  : contains (segmented) FLAIR data
%                   + '/DSC/'    : contains DSC data & processings
%                                  (if only one measurement exists)
%                   + '/T2/'     : contains T2 multiecho data & processings
%                                  (if only one measurement exists)
%                   + '/conditionDir_1'   : condition subfolders can be
%                   + '/conditionDir_...' : named arbitrarly via setting
%                   + '/conditionDir_n'%  : the variable:
%                       for example, conditions could be named:
%                      -> conditions = ["rest","control","calc", "mem"];
%                         + DSC : contains DSC data & processings
%                                (if condition specific measurements
%                                 exists)
%                         + T2  : contains T2 multiecho data & processings
%                                (if condition specific measurements
%                                 exists)
%                         + T2S : contains T2s multiecho data & processings
%                                 (condition specific measurements should
%                                 exists)
%                         + final processed and coregistered (to T2-TE1)
%                           parameter maps of all included techniques
%
%                   NOTE:  - use condition subfolder also in case of 
%                            only one condition, to facilitate processing
%                                - conditions are processed according to 
%                                  their order in the variable 'conditions'
%                                --> 1st condition --> reference condition
%                                --> use identical names as contained in 
%                                    file names because they are used for
%                                    processing
%--------------------------------------------------------------------------  
% MAIN FUNCTION
%--------------------------------------------------------------------------
function RUN_mqBOLD_auto()
%--------------------------------------------------------------------------
% REMINDER: Considering sequence and vendor specific settings is important!
%--------------------------------------------------------------------------
T2s_pars.manufacturer = 'Philips';
% T2s_pars.manufacturer = 'Siemens';

% -------------------------------------------------------------------------
% SET CONDITION NAMES occuring in BIDS file names
%--------------------------------------------------------------------------
% CAVE: condition names for multiple conditions that are  used in
% condition specific data in the BIDS (xnat) input, i.e. file names
% ---> folder names in the output structure
% CAVE: wrong file names prevent prope sorting of data
% -------------------------------------------------------------------------
conditions = ["rest","control"];

% -------------------------------------------------------------------------
% GEOMETRY SETTING
% -------------------------------------------------------------------------
% Resetting the origin of nii data might improve results of coregistration 
% and spatial normalization if original origin is set unfavorably
% -------------------------------------------------------------------------
% Necessary shifts for individual subjects can be determined using SPM12
% display tool and entered in function reset_origin.m
% -------------------------------------------------------------------------
flag.reset_origin = 0; % 0: keep origin (default) 
                       % 1: reset origin

%--------------------------------------------------------------------------
% REMINDER to ensure correct settings, because programm takes some time ...
%--------------------------------------------------------------------------
answer = questdlg('Do conditions, DSC and T2s settings fit the data?', ...
	'Check program settings', ...
	'Yes','No','Yes');
% Handle response
switch answer
    case 'Yes'
        disp([answer '---> proceed ...'])
    case 'No'
        disp([answer '---> Stop processing and check!'])
        return
end
% ---------------------------------------------------------------------
% Initialize
% ---------------------------------------------------------------------
% Print status
fprintf('Initialization of automated mqBOLD processing started...\n');

% -------------------------------------------------------------------------
% Automatic detection of subject folders within top level study folder.
% Processed subjects are excluded if a file 'qBOLD_workspace.mat' exists
% in the subjects top level output folder, which indicates that proecessing
% was completed successfully.
% -------------------------------------------------------------------------
fprintf('Step 1: Getting subjects information...\n');
[StudyPath, nsubs] = get_subject_dirs_auto;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Iterate through all subjects
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for sb = 1 : nsubs
  	% ---------------------------------------------------------------------
   	% Get path to subject sb
   	% ---------------------------------------------------------------------
  	[~, subject_name, ~] = fileparts(StudyPath.input_dir_sb_process{sb});
  	SubjectDir.input = StudyPath.input_dir_sb_process{sb};
 	SubjectDir.output = StudyPath.output_dir_sb_process{sb};	
    
	if exist(fullfile(SubjectDir.output,'qBOLD_workspace.mat'),'file') == 0        
        %------------------------------------------------------------------
        % Save all text outputs in diary text file for documentation
        %------------------------------------------------------------------
        check_folder(SubjectDir.output);
        path_diary = fullfile(SubjectDir.output, 'diary.txt');
        diary(path_diary);
        
        % -----------------------------------------------------------------
        % Initialize conditions in Subject.Dir structure
        % -----------------------------------------------------------------
        nconditions = size(conditions,2);
        for ncd = 1:nconditions
            SubjectDir.condition(ncd).toplevel = ...
                fullfile(SubjectDir.output,'qBOLD',cell2mat(conditions(ncd)));
        end
        
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % ANATOMY (FLAIR & T1w-MPR) PROCESSING
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Copy anatomy from BIDS input to output folder
        %   -> establish previous data structure
        %   -> segment FLAIR & T1w (MPRAGE)
        % -----------------------------------------------------------------
        % STUDY_folder              : can (probably) be named arbitrarly
        %   + '/output'
        %       + '/sub-p001'       : subjects need to be numbered
        %                             consecutively with prefix 'sub-p';
        %           + qBOLD         : contains previous structure
        %               + '/T1w/'   : contains (segmented) T1w data
        %               + '/FLAIR/' : contains (segmented) FLAIR data
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fprintf('Anatomy processing of sb %i/%i: %s\n',...
                                              sb, nsubs, SubjectDir.input);
        % returns paths to anatomical data and flag indicating their presence                               
        [SubjectDir, filelist, flag] = ...
            run1_process_anatomy(subject_name, SubjectDir, flag);                          
                              
        
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % DSC PROCESSING
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % DSC processing ----> rCBV
        % (requires FLAIR and T1w MPR data)
        % NOTE: FLAIR and T1w MPR are copied to DSC in sortData_dsc
        % -----------------------------------------------------------------
        % Overal settings for DSC processing (common defaults  mostly OK)
        % (required for DSC-Processing in DSC_CBV_10.0)
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        DSC_pars = dsc_settings();                
        % -----------------------------------------------------------------
        % Automatic detection & sorting of DSC data in subject folder
        % CAVE: Always check correct coregistration & data orientation        
        % -----------------------------------------------------------------
        [SubjectDir, filelist, DSC_pars, flag] = sortData_dsc(subject_name, ...
        	conditions, SubjectDir, filelist, DSC_pars, flag, sb);
        if strcmp(flag.DSC,'singleDSC') % CAVE: only one DSC expected 
            %--------------------------------------------------------------
            % data are processed in qBOLD toplevel folder
            %--------------------------------------------------------------
        	% Print status            
         	fprintf('DSC processing of sb %i/%i\n', sb, nsubs);  
            % Perform processing of DSC data for rCBV etc.                        
            [SubjectDir, filelist, flag] = run2_dsc_cbv(SubjectDir, filelist, ...
                                                            DSC_pars, flag);          
        elseif strcmp(flag.DSC,'conditionDSC')
            %--------------------------------------------------------------
            % data are processed in in respective condition folders
            %--------------------------------------------------------------           
            for ncond = 1:nconditions 
                if flag.condition(ncond).DSC
                     fprintf('DSC processing of sb %i/%i, condition %s\n', ...
                         sb, nsubs,conditions(ncond));          
                    % Perform processing of DSC data for rCBV etc.
                    [SubjectDir.condition(ncond), filelist.condition(ncond),...
                        flag] = run2_dsc_cbv(SubjectDir.condition(ncond), ...
                        filelist.condition(ncond), DSC_pars, flag);  
                end % if flag.condition(ncond).DSC
            end % for ncond = 1:size(conditions,2)
        else
            fprintf('No DSC processing for sb %i/%i (%s)\n',...
                                        sb, nsubs,subject_name);
            fprintf('Check settings if data available!\n');                       
            flag.DSC = 0;
        end % strcmp(flag.DSC,'singleDSC') 

        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % mq-BOLD PROCESSING ---> T2, T2*, R2', rOEF
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Calculate mq-BOLD-based semiquantitative OEF (MR-rOEF)
        % - T2: exponential fit of multi-echo GraSE data 
        %       - 2D: fit even echoes only
        %       - 3D: fit all echoes
        %       ---> TE1 of single or 1st condition T2 data serves as
        %            reference for coregistration           
        % - T2* processing includes 
        %       - correction for motion if full, half and quarter  
        %         resolution multi-echo GE data are available
        %       - correction for magnetic background gradients if either
        %         magn/real/imaginary (Philips) or magn/phase (Siemens)
        %         data are available
        %       - T2s maps are coregistered to TE1 of reference T2
        % --> R2' = R2* - R2 = 1/T2* - 1/T2
        % - DSC-based CBV (either one or condition specific) 
        %   is coregistered to TE1 of reference T2
        % - coregistered T2, T2_error, T2s, T2s_absgradz,CBV, MPR, FLAIR
        %   are copied to toplevel conditions folders prior to rOEF
        %   calculation
        % -----------------------------------------------------------------
        % T2s_pars: Important sequence and vendor specific settings for T2* 
        %           processing,
        % CAVE:     Magnetic background field correction needs sequence 
        %           specific settings that are taken care of here.  
        %           Impementation of optimal RF pulse shape ('EXPO') and  
        %           retrieval of required parameter values requires expert 
        %           knowledge and access to pulse sequence programming 
        %           environment. 
        % WARNING:  INCORRECT settings produce weird results.
        % -----------------------------------------------------------------      
        T2s_pars = T2s_settings(T2s_pars);         
        % -----------------------------------------------------------------
        % Automatic detection & sorting of DSC data in subject folder
        % CAVE: Always check correct coregistration & data orientation        
        % ----------------------------------------------------------------- 
        
        [SubjectDir, filelist, flag] = sortData_T2_T2s(conditions, ...
                                    SubjectDir, filelist, T2s_pars, flag);  
        % -----------------------------------------------------------------
        % perform processing
        % -----------------------------------------------------------------
        if flag.T2       
            [SubjectDir, filelist] = run3_mqBOLD_rOEF(SubjectDir, filelist, ...
                subject_name, conditions, nconditions, T2s_pars, flag); 
        end %if flag.T2
        
        %------------------------------------------------------------------
        % Save parameter settings & paths 
        % ---> indicated sucessfulcompletion of prosessing
        % -----------------------------------------------------------------
        save(fullfile(SubjectDir.output, 'qBOLD_workspace.mat'), ...
            'SubjectDir', 'filelist', 'subject_name', 'conditions',  ...
            'nconditions','DSC_pars', 'T2s_pars','flag', 'sb');
        
        fprintf('rOEF processing of sb %s %i/%i finished\n', ...
                                            subject_name, sb, nsubs);   
        
        % close diary
        diary off;
        % clean up
        clear SubjectDir subject_name filelist
        close all
    else
        fprintf('\n\nSubject %s has been processed already!\n\n',...
            SubjectDir.output);
	end % if exist(fullfile(SubjectDir.output,'workspace.mat'),'file') == 0 
end % for nsubs    
end
